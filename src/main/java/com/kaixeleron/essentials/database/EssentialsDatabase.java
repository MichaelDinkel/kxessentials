package com.kaixeleron.essentials.database;

import com.kaixeleron.essentials.database.data.BalTopEntry;
import com.kaixeleron.essentials.database.converter.DatabaseConverter;
import org.bukkit.Location;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public interface EssentialsDatabase extends AutoCloseable {

    void saveDefault(UUID id) throws DatabaseException;

    void load(UUID id) throws DatabaseException;

    double getBalance(UUID id) throws DatabaseException;

    void setBalance(UUID id, double balance) throws DatabaseException;

    void addIgnored(UUID id, UUID ignored) throws DatabaseException;

    void removeIgnored(UUID id, UUID ignored) throws DatabaseException;

    boolean isIgnored(UUID id, UUID ignored) throws DatabaseException;

    void setMessagingDisabled(UUID id, boolean disabled) throws DatabaseException;

    boolean isMessagingDisabled(UUID id) throws DatabaseException;

    void setNickname(UUID id, String nickname) throws DatabaseException;

    String getNickname(UUID id) throws DatabaseException;

    void setPowertool(UUID id, String type, String command) throws DatabaseException;

    Map<String, String> getPowertools(UUID id) throws DatabaseException;

    void removePowertool(UUID id, String type) throws DatabaseException;

    void setSocialSpy(UUID id, boolean socialspy) throws DatabaseException;

    boolean isSocialSpyEnabled(UUID id) throws DatabaseException;

    void setAcceptingAllTeleports(UUID id, boolean enabled) throws DatabaseException;

    boolean isAcceptingAllTeleports(UUID id) throws DatabaseException;

    void setTeleportRequestsDisabled(UUID id, boolean enabled) throws DatabaseException;

    boolean areTeleportRequestsDisabled(UUID id) throws DatabaseException;

    void setHome(UUID id, String name, Location loc) throws DatabaseException;

    void removeHome(UUID id, String name) throws DatabaseException;

    Location getHome(UUID id, String name) throws DatabaseException;

    Set<String> listHomes(UUID id) throws DatabaseException;

    void setPayDisabled(UUID id, boolean disabled) throws DatabaseException;

    boolean isPayDisabled(UUID id) throws DatabaseException;

    void setKitCooldown(UUID id, String kit, long cooldown) throws DatabaseException;

    long getKitCooldown(UUID id, String kit) throws DatabaseException;

    void loadBalTop() throws DatabaseException;

    boolean isBalTopLoaded();

    List<BalTopEntry> getBalTop(int page) throws DatabaseException;

    int getBalTopPages() throws DatabaseException;

    double getBalanceTotal() throws DatabaseException;

    void setLoginRespawn(UUID id, boolean respawn) throws DatabaseException;

    boolean isRespawnOnLoginSet(UUID id) throws DatabaseException;

    DatabaseConverter getConverter() throws DatabaseException;

}
