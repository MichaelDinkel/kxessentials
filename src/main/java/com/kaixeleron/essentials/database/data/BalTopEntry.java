package com.kaixeleron.essentials.database.data;

import java.util.UUID;

public class BalTopEntry {

    private String name;

    private UUID id;

    private double amount;

    public BalTopEntry(String name, UUID id, double amount) {

        this.name = name;

        this.id = id;

        this.amount = amount;

    }

    public String getName() {

        return name;

    }

    public void setName(String name) {

        this.name = name;

    }

    public double getAmount() {

        return amount;

    }

    public UUID getId() {

        return id;

    }

    public void setAmount(double amount) {

        this.amount = amount;

    }
}
