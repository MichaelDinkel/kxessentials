package com.kaixeleron.essentials.database.converter;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.database.DatabaseException;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.*;
import java.util.UUID;

public class SQLConverter implements DatabaseConverter {

    private final EssentialsMain m;

    private final PlayerDataManager playerDataManager;

    private final String hostname, database, username, password;

    private final int port;

    private Connection c;

    private DatabasePlayerRecord[] records;

    private int recordHead = 0;

    public SQLConverter(EssentialsMain m, PlayerDataManager playerDataManager, String hostname, int port, String database, String username, String password) throws DatabaseException {

        this.m = m;

        this.playerDataManager = playerDataManager;

        this.hostname = hostname;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;

        this.records = new DatabasePlayerRecord[0];

        try {

            connect();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        }

    }

    private void connect() throws SQLException {

        if (c == null || c.isClosed()) c = DriverManager.getConnection(String.format("jdbc:mysql://%s:%d/%s?useSSL=false", hostname, port, database), username, password);

    }

    private void closeSilently(AutoCloseable... closeables) {

        for (AutoCloseable closeable : closeables) {

            if (closeable != null) {

                try {

                    closeable.close();

                } catch (Exception ignored) {}

            }

        }

    }

    @Override
    public void loadRecords() throws DatabaseException {

        int amount = getNumberOfRecords();

        System.out.println(String.format("Found %d records.", amount));
        System.out.println("Allocating record array...");

        records = new DatabasePlayerRecord[amount];

        System.out.println("Array allocated.");
        System.out.println("Loading player balances...");

        loadBalances();

        System.out.println("Balances loaded.");
        System.out.println("Loading ignored players...");

        loadIgnored();

        System.out.println("Ignored players loaded.");
        System.out.println("Loading homes...");

        loadHomes();

        System.out.println("Homes loaded.");
        System.out.println("Loading powertools...");

        loadPowertools();

        System.out.println("Powertools loaded.");
        System.out.println("Loading kit cooldowns...");

        loadKitCooldowns();

        System.out.println("Kit cooldowns loaded.");
        System.out.println("Loading user settings...");

        loadUserSettings();

        System.out.println("User settings loaded.");
        System.out.println("All SQL player records have been loaded into memory.");

    }

    private int getNumberOfRecords() throws DatabaseException {

        int records = 0;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            get = c.prepareStatement("SELECT COUNT(*) FROM `kxessentials_usersettings`;");

            rs = get.executeQuery();

            if (rs.next()) {

                records = rs.getInt(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return records;

    }

    @Override
    public DatabasePlayerRecord[] getRecords() {

        return records;

    }

    @Override
    public void importRecords(DatabasePlayerRecord[] records, boolean refresh) throws DatabaseException {

        System.out.println(String.format("Found %d records to import.", records.length));
        System.out.println("Importing player balances...");

        importBalances(records);

        System.out.println("Player balances imported.");
        System.out.println("Importing ignored players...");

        importIgnored(records);

        System.out.println("Ignored players imported.");
        System.out.println("Importing player homes...");

        importHomes(records);

        System.out.println("Player homes imported.");
        System.out.println("Importing powertools...");

        importPowertools(records);

        System.out.println("Powertools imported.");
        System.out.println("Importing kit cooldowns...");

        importKitCooldowns(records);

        System.out.println("Kit cooldowns imported.");
        System.out.println("Importing user settings...");

        importUserSettings(records);

        System.out.println("User settings imported.");
        System.out.println("MySQL import complete.");

        if (refresh) {

            new BukkitRunnable() {

                @Override
                public void run() {

                    System.out.println("Loading new data...");

                    playerDataManager.clearCache();

                    for (Player p : Bukkit.getOnlinePlayers()) {

                        playerDataManager.loadData(p);

                    }

                    System.out.println("Load complete.");

                }

            }.runTask(m);

        }

    }

    private int findRecordIndex(UUID id) {

        int index = -1;

        for (int i = 0; i < records.length; i++) {

            if (records[i] != null && records[i].getPlayerID().equals(id)) {

                index = i;

                break;

            }

        }

        return index;

    }

    private void loadBalances() throws DatabaseException {

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxessentials_balances`;");

            rs = get.executeQuery();

            while (rs.next()) {

                UUID id = UUID.fromString(rs.getString(1));

                DatabasePlayerRecord record;

                int index = findRecordIndex(id);

                if (index == -1) {

                    record = new DatabasePlayerRecord(id);

                    index = recordHead;
                    recordHead++;

                } else {

                    record = records[index];

                }

                record.setBalance(rs.getDouble(2));

                records[index] = record;

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

    }

    private void loadIgnored() throws DatabaseException {

        PreparedStatement getIds = null, getCount = null, get = null;
        ResultSet idRs = null, count = null, rs = null;

        try {

            connect();

            getIds = c.prepareStatement("SELECT DISTINCT `uuid` FROM `kxessentials_ignored`;");

            idRs = getIds.executeQuery();

            while (idRs.next()) {

                try {

                    getCount = c.prepareStatement("SELECT COUNT(*) FROM `kxessentials_ignored` WHERE `uuid` = ?;");
                    getCount.setString(1, idRs.getString(1));

                    count = getCount.executeQuery();

                    String[] ignored = new String[count.getInt(1)];

                    get = c.prepareStatement("SELECT `target` FROM `kxessentials_ignored` WHERE `uuid` = ?;");
                    get.setString(1, idRs.getString(1));

                    rs = get.executeQuery();

                    int current = 0;

                    while (rs.next()) {

                        ignored[current] = rs.getString(1);

                        current++;

                    }

                    UUID id = UUID.fromString(idRs.getString(1));

                    DatabasePlayerRecord record;

                    int index = findRecordIndex(id);

                    if (index == -1) {

                        record = new DatabasePlayerRecord(id);

                        index = recordHead;
                        recordHead++;

                    } else {

                        record = records[index];

                    }

                    record.setIgnored(ignored);

                    records[index] = record;

                } finally {

                    closeSilently(getCount, count, get, rs);

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(getIds, idRs);

        }

    }

    private void loadHomes() throws DatabaseException {

        PreparedStatement getIds = null, getCount = null, get = null;
        ResultSet idRs = null, count = null, rs = null;

        try {

            connect();

            getIds = c.prepareStatement("SELECT DISTINCT `uuid` FROM `kxessentials_homes`;");

            idRs = getIds.executeQuery();

            while (idRs.next()) {

                try {

                    getCount = c.prepareStatement("SELECT COUNT(*) FROM `kxessentials_homes` WHERE `uuid` = ?;");
                    getCount.setString(1, idRs.getString(1));

                    count = getCount.executeQuery();

                    String[] homeNames = new String[count.getInt(1)];
                    Location[] homes = new Location[homeNames.length];

                    get = c.prepareStatement("SELECT `name`, `world`, `x`, `y`, `z`, `yaw`, `pitch` FROM `kxessentials_homes` WHERE `uuid` = ?;");
                    get.setString(1, idRs.getString(1));

                    rs = get.executeQuery();

                    int current = 0;

                    while (rs.next()) {

                        homeNames[current] = rs.getString(1);
                        homes[current] = new Location(Bukkit.getWorld(rs.getString(2)), rs.getDouble(3), rs.getDouble(4), rs.getDouble(5), rs.getFloat(6), rs.getFloat(7));

                        current++;

                    }

                    UUID id = UUID.fromString(idRs.getString(1));

                    DatabasePlayerRecord record;

                    int index = findRecordIndex(id);

                    if (index == -1) {

                        record = new DatabasePlayerRecord(id);

                        index = recordHead;
                        recordHead++;

                    } else {

                        record = records[index];

                    }

                    record.setHomeNames(homeNames);
                    record.setHomes(homes);

                    records[index] = record;

                } finally {

                    closeSilently(getCount, count, get, rs);

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(getIds, idRs);

        }

    }

    private void loadPowertools() throws DatabaseException {

        PreparedStatement getIds = null, getCount = null, get = null;
        ResultSet idRs = null, count = null, rs = null;

        try {

            connect();

            getIds = c.prepareStatement("SELECT DISTINCT `uuid` FROM `kxessentials_powertools`;");

            idRs = getIds.executeQuery();

            while (idRs.next()) {

                try {

                    getCount = c.prepareStatement("SELECT COUNT(*) FROM `kxessentials_powertools` WHERE `uuid` = ?;");
                    getCount.setString(1, idRs.getString(1));

                    count = getCount.executeQuery();

                    String[] powertoolMaterials = new String[count.getInt(1)];
                    String[] powertoolCommands = new String[powertoolMaterials.length];

                    get = c.prepareStatement("SELECT `material`, `command` FROM `kxessentials_powertools` WHERE `uuid` = ?;");
                    get.setString(1, idRs.getString(1));

                    rs = get.executeQuery();

                    int current = 0;

                    while (rs.next()) {

                        powertoolMaterials[current] = rs.getString(1);
                        powertoolCommands[current] = rs.getString(2);

                        current++;

                    }

                    UUID id = UUID.fromString(idRs.getString(1));

                    DatabasePlayerRecord record;

                    int index = findRecordIndex(id);

                    if (index == -1) {

                        record = new DatabasePlayerRecord(id);

                        index = recordHead;
                        recordHead++;

                    } else {

                        record = records[index];

                    }

                    record.setPowertoolsMaterials(powertoolMaterials);
                    record.setPowertoolsCommands(powertoolCommands);

                    records[index] = record;

                } finally {

                    closeSilently(getCount, count, get, rs);

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(getIds, idRs);

        }

    }

    private void loadKitCooldowns() throws DatabaseException {

        PreparedStatement getIds = null, getCount = null, get = null;
        ResultSet idRs = null, count = null, rs = null;

        try {

            connect();

            getIds = c.prepareStatement("SELECT DISTINCT `uuid` FROM `kxessentials_kitcooldowns`;");

            idRs = getIds.executeQuery();

            while (idRs.next()) {

                try {

                    getCount = c.prepareStatement("SELECT COUNT(*) FROM `kxessentials_kitcooldowns` WHERE `uuid` = ?;");
                    getCount.setString(1, idRs.getString(1));

                    count = getCount.executeQuery();

                    String[] kitCooldownNames = new String[count.getInt(1)];
                    Long[] kitCooldowns = new Long[kitCooldownNames.length];

                    get = c.prepareStatement("SELECT `kit`, `cooldown` FROM `kxessentials_kitcooldowns` WHERE `uuid` = ?;");
                    get.setString(1, idRs.getString(1));

                    rs = get.executeQuery();

                    int current = 0;

                    while (rs.next()) {

                        kitCooldownNames[current] = rs.getString(1);
                        kitCooldowns[current] = rs.getLong(2);

                        current++;

                    }

                    UUID id = UUID.fromString(idRs.getString(1));

                    DatabasePlayerRecord record;

                    int index = findRecordIndex(id);

                    if (index == -1) {

                        record = new DatabasePlayerRecord(id);

                        index = recordHead;
                        recordHead++;

                    } else {

                        record = records[index];

                    }

                    record.setKitCooldownNames(kitCooldownNames);
                    record.setKitCooldowns(kitCooldowns);

                    records[index] = record;

                } finally {

                    closeSilently(getCount, count, get, rs);

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(getIds, idRs);

        }

    }

    private void loadUserSettings() throws DatabaseException {

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxessentials_usersettings`;");

            rs = get.executeQuery();

            while (rs.next()) {

                UUID id = UUID.fromString(rs.getString(1));

                DatabasePlayerRecord record;

                int index = findRecordIndex(id);

                if (index == -1) {

                    record = new DatabasePlayerRecord(id);

                    index = recordHead;
                    recordHead++;

                } else {

                    record = records[index];

                }

                record.setNickname(rs.getString(2));
                record.setMessagingDisabled(rs.getBoolean(3));
                record.setSocialSpyEnabled(rs.getBoolean(4));
                record.setAcceptingAllTeleports(rs.getBoolean(5));
                record.setTeleportRequestsDisabled(rs.getBoolean(6));
                record.setPayDisabled(rs.getBoolean(7));

                records[index] = record;

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

    }

    private void importBalances(DatabasePlayerRecord[] records) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_balances` (`uuid`, `balance`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `balance` = VALUES(`balance`);");

            int i = 0;

            for (DatabasePlayerRecord record : records) {

                insert.setString(1, record.getPlayerID().toString());
                insert.setDouble(2, record.getBalance());

                insert.addBatch();

                i++;

                if (i % 1000 == 0 || i == records.length) {

                    insert.executeBatch();

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    private void importIgnored(DatabasePlayerRecord[] records) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_ignored` (`uuid`, `target`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `uuid` = `uuid`;");

            int i = 0;

            for (DatabasePlayerRecord record : records) {

                String id = record.getPlayerID().toString();

                for (String s : record.getIgnored()) {

                    insert.setString(1, id);
                    insert.setString(2, s);

                    insert.addBatch();

                    i++;

                    if (i % 1000 == 0 || i == records.length) {

                        insert.executeBatch();

                    }

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    private void importHomes(DatabasePlayerRecord[] records) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_homes` (`uuid`, `name`, `world`, `x`, `y`, `z`, `yaw`, `pitch`) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `world` = VALUES(`world`), `x` = VALUES(`x`), `y` = VALUES(`y`), `z` = VALUES(`z`), `yaw` = VALUES(`yaw`), `pitch` = VALUES(`pitch`);");

            int i = 0;

            for (DatabasePlayerRecord record : records) {

                String id = record.getPlayerID().toString();

                for (int home = 0; home < record.getHomeNames().length; home++) {

                    Location l = record.getHomes()[home];

                    insert.setString(1, id);
                    insert.setString(2, record.getHomeNames()[home]);
                    insert.setString(3, l.getWorld().getName());
                    insert.setDouble(4, l.getX());
                    insert.setDouble(5, l.getY());
                    insert.setDouble(6, l.getZ());
                    insert.setFloat(7, l.getYaw());
                    insert.setFloat(8, l.getPitch());

                    insert.addBatch();

                    i++;

                    if (i % 1000 == 0 || i == records.length) {

                        insert.executeBatch();

                    }

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    private void importPowertools(DatabasePlayerRecord[] records) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_powertools` (`uuid`, `material`, `command`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `command` = VALUES(`command`);");

            int i = 0;

            for (DatabasePlayerRecord record : records) {

                String id = record.getPlayerID().toString();

                for (int powertool = 0; powertool < record.getPowertoolMaterials().length; powertool++) {

                    insert.setString(1, id);
                    insert.setString(2, record.getPowertoolMaterials()[powertool]);
                    insert.setString(3, record.getPowertoolCommands()[powertool]);

                    insert.addBatch();

                    i++;

                    if (i % 1000 == 0 || i == records.length) {

                        insert.executeBatch();

                    }

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    private void importKitCooldowns(DatabasePlayerRecord[] records) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_powertools` (`uuid`, `kit`, `cooldown`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `cooldown` = VALUES(`cooldown`);");

            int i = 0;

            for (DatabasePlayerRecord record : records) {

                String id = record.getPlayerID().toString();

                for (int kit = 0; kit < record.getKitCooldownNames().length; kit++) {

                    insert.setString(1, id);
                    insert.setString(2, record.getKitCooldownNames()[kit]);
                    insert.setLong(3, record.getKitCooldowns()[kit]);

                    insert.addBatch();

                    i++;

                    if (i % 1000 == 0 || i == records.length) {

                        insert.executeBatch();

                    }

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    private void importUserSettings(DatabasePlayerRecord[] records) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_usersettings` (`uuid`, `nickname`, `messagingdisabled`, `socialspy`, `acceptingallteleports`, `teleportsdisabled`, `paydisabled`) VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `nickname` = VALUES(`nickname`), `messagingdisabled` = VALUES(`messagingdisabled`), `socialspy` = VALUES(`socialspy`), `acceptingallteleports` = VALUES(`acceptingallteleports`), `teleportsdisabled` = VALUES(`teleportsdisabled`), `paydisabled` = VALUES(`paydisabled`);");

            int i = 0;

            for (DatabasePlayerRecord record : records) {

                insert.setString(1, record.getPlayerID().toString());
                insert.setString(2, record.getNickname());
                insert.setBoolean(3, record.isMessagingDisabled());
                insert.setBoolean(4, record.isSocialSpyEnabled());
                insert.setBoolean(5, record.isAcceptingAllTeleports());
                insert.setBoolean(6, record.areTeleportRequestsDisabled());
                insert.setBoolean(7, record.isPayDisabled());

                insert.addBatch();

                i++;

                if (i % 1000 == 0 || i == records.length) {

                    insert.executeBatch();

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

}
