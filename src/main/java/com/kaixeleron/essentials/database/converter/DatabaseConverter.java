package com.kaixeleron.essentials.database.converter;

import com.kaixeleron.essentials.database.DatabaseException;

public interface DatabaseConverter {

    void loadRecords() throws DatabaseException;

    DatabasePlayerRecord[] getRecords();

    void importRecords(DatabasePlayerRecord[] records, boolean refresh) throws DatabaseException;

}
