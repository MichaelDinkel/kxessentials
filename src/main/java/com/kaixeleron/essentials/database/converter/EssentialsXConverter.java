package com.kaixeleron.essentials.database.converter;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.database.DatabaseException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class EssentialsXConverter implements DatabaseConverter {

    private final EssentialsMain m;

    private final Set<DatabasePlayerRecord> records;

    private final Set<EssentialsXNameBasedPlayerRecord> toConvert;

    private final Map<String, UUID> uuidCache;

    private final Gson gson;

    public EssentialsXConverter(EssentialsMain m) {

        this.m = m;

        records = new HashSet<>();
        toConvert = new HashSet<>();

        uuidCache = new HashMap<>();

        gson = new Gson();

    }

    @Override
    public void loadRecords() {

        File userDataFolder = new File(m.getDataFolder().getParentFile(), "Essentials" + File.separatorChar + "userdata");

        if (userDataFolder.exists()) {

            File[] files = userDataFolder.listFiles();

            if (files != null) {

                System.out.println(String.format("Found %d records.", files.length));
                System.out.println("Importing records...");

                for (File f : files) {

                    FileConfiguration data = YamlConfiguration.loadConfiguration(f);

                    double balance = Double.parseDouble(data.getString("money", "0"));

                    String[] homeNames;
                    Location[] homes;

                    ConfigurationSection homeSection = data.getConfigurationSection("homes");

                    if (homeSection != null) {

                        homeNames = homeSection.getKeys(false).toArray(new String[homeSection.getKeys(false).size()]);
                        homes = new Location[homeNames.length];

                        for (int i = 0; i < homeNames.length; i++) {

                            homes[i] = new Location(Bukkit.getWorld(homeSection.getString(String.format("%s.world", homeNames[i]))), homeSection.getDouble(String.format("%s.x", homeNames[i])), homeSection.getDouble(String.format("%s.y", homeNames[i])), homeSection.getDouble(String.format("%s.z", homeNames[i])), (float) homeSection.getDouble(String.format("%s.yaw", homeNames[i])), (float) homeSection.getDouble(String.format("%s.pitch", homeNames[i])));

                        }

                    } else {

                        homeNames = new String[0];
                        homes = new Location[0];

                    }

                    List<String> ignored = data.getStringList("ignore");

                    String[] powertoolMaterials, powertoolCommands;

                    ConfigurationSection powertools = data.getConfigurationSection("powertools");

                    if (powertools != null) {

                        powertoolMaterials = powertools.getKeys(false).toArray(new String[powertools.getKeys(false).size()]);
                        powertoolCommands = new String[powertoolMaterials.length];

                        for (int i = 0; i < powertoolMaterials.length; i++) {

                            powertoolCommands[i] = powertools.getString(powertoolMaterials[i]);

                        }

                    } else {

                        powertoolMaterials = new String[0];
                        powertoolCommands = new String[0];

                    }

                    //Name-based record
                    if (f.getName().length() < 40) {

                        toConvert.add(new EssentialsXNameBasedPlayerRecord(f.getName().replace(".yml", ""), balance, homeNames, homes, ignored.toArray(new String[ignored.size()]), powertoolMaterials, powertoolCommands, null, null, data.getString("nickname"), data.getBoolean("teleportauto"), false, !data.getBoolean("acceptingpay"), data.getBoolean("socialspy"), !data.getBoolean("teleportenabled")));

                    } else {

                        records.add(new DatabasePlayerRecord(UUID.fromString(f.getName().replace(".yml", "")), balance, homeNames, homes, ignored.toArray(new String[ignored.size()]), powertoolMaterials, powertoolCommands, null, null, data.getString("nickname"), data.getBoolean("teleportauto"), false, !data.getBoolean("acceptingpay"), data.getBoolean("socialspy"), !data.getBoolean("teleportenabled")));

                    }

                }

                System.out.println("Records imported.");

                if (toConvert.size() > 0) {

                    System.out.println(String.format("Detected %d records requiring UUID conversion.", toConvert.size()));

                    long started = System.currentTimeMillis();

                    List<EssentialsXNameBasedPlayerRecord> buffer = new ArrayList<>(100);

                    int current = 0;

                    for (EssentialsXNameBasedPlayerRecord record : toConvert) {

                        buffer.add(record);

                        current++;

                        if (current % 100 == 0 || current == toConvert.size()) {

                            try {

                                String[] names = new String[buffer.size()];

                                for (int name = 0; name < buffer.size(); name++) {

                                    names[name] = buffer.get(name).getName();

                                }

                                UUID[] ids = getUUIDs(names);

                                for (int i = 0; i < buffer.size(); i++) {

                                    records.add(buffer.get(i).convertToDatabasePlayerRecord(ids[i]));

                                }

                            } catch (IOException e) {

                                //TODO handle the error better
                                System.err.println("Could not fetch UUIDs from Mojang.");
                                e.printStackTrace();

                            }

                            buffer.clear();

                            if ((current / 100) % 600 == 0) {

                                long limit = started + TimeUnit.MINUTES.toMillis(10L);

                                long difference = limit - System.currentTimeMillis();

                                if (difference < 0) {

                                    try {

                                        System.out.println(String.format("Sleeping for %d seconds due to rate limiting.", TimeUnit.MILLISECONDS.toSeconds(difference)));

                                        Thread.sleep(difference);

                                    } catch (InterruptedException ignored) {}

                                }

                            }

                        }

                    }

                    System.out.println("Converted all records to UUID.");

                }

                //TODO convert powertool materials
                //TODO convert ignored names

            }

        } else {

            System.err.println("Could not import EssentialsX data as there is no userdata folder.");

        }

    }

    @Override
    public DatabasePlayerRecord[] getRecords() {

        return records.toArray(new DatabasePlayerRecord[records.size()]);

    }

    @Override
    public void importRecords(DatabasePlayerRecord[] records, boolean refresh) throws DatabaseException {

    }

    private UUID[] getUUIDs(String[] names) throws IOException {

        UUID[] out = new UUID[names.length];

        BufferedWriter writer = null;
        BufferedReader reader = null;

        try {

            HttpsURLConnection connection = (HttpsURLConnection) new URL("https://api.mojang.com/profiles/minecraft").openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
            writer.write(gson.toJson(names));
            writer.flush();

            StringBuilder response = new StringBuilder();

            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String data = reader.readLine();

            while (data != null) {

                response.append(data);
                data = reader.readLine();

            }

            JsonArray responseJson = gson.fromJson(response.toString(), JsonArray.class);

            for (JsonElement e : responseJson) {

                JsonObject obj = e.getAsJsonObject();

                StringBuilder uuid = new StringBuilder(obj.get("id").getAsString());

                uuid.insert(8, "-").insert(13, "-").insert(18, "-").insert(23, "-");

                String name = obj.get("name").getAsString();
                UUID id = UUID.fromString(uuid.toString());

                for (int i = 0; i < names.length; i++) {

                    if (names[i].equals(name)) {

                        out[i] = id;

                        break;

                    }

                }

                uuidCache.put(name, id);

            }

        } finally {

            try {

                if (writer != null) writer.close();
                if (reader != null) reader.close();

            } catch (Exception ignored) {}

        }

        return out;

    }

}
