package com.kaixeleron.essentials.database.converter;

import org.bukkit.Location;

import java.util.UUID;

public class DatabasePlayerRecord {

    private final UUID player;

    private double balance;

    private String[] homeNames;

    private Location[] homes;

    private String[] ignored, powertoolMaterials, powertoolCommands, kitCooldownNames;

    private Long[] kitCooldowns;

    private String nickname;

    private boolean acceptingAllTeleports, messagingDisabled, payDisabled, socialSpyEnabled, teleportRequestsDisabled;

    public DatabasePlayerRecord(UUID player, double balance, String[] homeNames, Location[] homes, String[] ignored, String[] powertoolMaterials, String[] powertoolCommands, String[] kitCooldownNames, Long[] kitCooldowns, String nickname, boolean acceptingAllTeleports, boolean messagingDisabled, boolean payDisabled, boolean socialSpyEnabled, boolean teleportRequestsDisabled) {

        this.player = player;

        this.balance = balance;
        this.homeNames = homeNames;
        this.homes = homes;
        this.ignored = ignored;
        this.powertoolMaterials = powertoolMaterials;
        this.powertoolCommands = powertoolCommands;
        this.kitCooldownNames = kitCooldownNames;
        this.kitCooldowns = kitCooldowns;
        this.nickname = nickname;
        this.acceptingAllTeleports = acceptingAllTeleports;
        this.messagingDisabled = messagingDisabled;
        this.payDisabled = payDisabled;
        this.socialSpyEnabled = socialSpyEnabled;
        this.teleportRequestsDisabled = teleportRequestsDisabled;

    }

    public DatabasePlayerRecord(UUID player) {

        this.player = player;

        this.balance = 0.0D;
        this.homeNames = new String[0];
        this.homes = new Location[0];
        this.ignored = new String[0];
        this.powertoolMaterials = new String[0];
        this.powertoolCommands = new String[0];
        this.kitCooldownNames = new String[0];
        this.kitCooldowns = new Long[0];
        this.nickname = "";
        this.acceptingAllTeleports = false;
        this.messagingDisabled = false;
        this.payDisabled = false;
        this.socialSpyEnabled = false;
        this.teleportRequestsDisabled = false;

    }

    public UUID getPlayerID() {

        return player;

    }

    public double getBalance() {

        return balance;

    }

    public void setBalance(double balance) {

        this.balance = balance;

    }

    public String[] getHomeNames() {

        return homeNames;

    }

    public void setHomeNames(String[] homeNames) {

        this.homeNames = homeNames;

    }

    public Location[] getHomes() {

        return homes;

    }

    public void setHomes(Location[] homes) {

        this.homes = homes;

    }

    public String[] getIgnored() {

        return ignored;

    }

    public void setIgnored(String[] ignored) {

        this.ignored = ignored;

    }

    public String[] getPowertoolMaterials() {

        return powertoolMaterials;

    }

    public void setPowertoolsMaterials(String[] powertoolMaterials) {

        this.powertoolMaterials = powertoolMaterials;

    }

    public String[] getPowertoolCommands() {

        return powertoolCommands;

    }

    public void setPowertoolsCommands(String[] powertoolCommands) {

        this.powertoolCommands = powertoolCommands;

    }

    public String[] getKitCooldownNames() {

        return kitCooldownNames;

    }

    public void setKitCooldownNames(String[] kitCooldownNames) {

        this.kitCooldownNames = kitCooldownNames;

    }

    public Long[] getKitCooldowns() {

        return kitCooldowns;

    }

    public void setKitCooldowns(Long[] kitCooldowns) {

        this.kitCooldowns = kitCooldowns;

    }

    public String getNickname() {

        return nickname;

    }

    public void setNickname(String nickname) {

        this.nickname = nickname;

    }

    public boolean isAcceptingAllTeleports() {

        return acceptingAllTeleports;

    }

    public void setAcceptingAllTeleports(boolean acceptingAllTeleports) {

        this.acceptingAllTeleports = acceptingAllTeleports;

    }

    public boolean isMessagingDisabled() {

        return messagingDisabled;

    }

    public void setMessagingDisabled(boolean messagingDisabled) {

        this.messagingDisabled = messagingDisabled;

    }

    public boolean isPayDisabled() {
        return payDisabled;
    }

    public void setPayDisabled(boolean payDisabled) {

        this.payDisabled = payDisabled;

    }

    public boolean isSocialSpyEnabled() {

        return socialSpyEnabled;

    }

    public void setSocialSpyEnabled(boolean socialSpyEnabled) {

        this.socialSpyEnabled = socialSpyEnabled;

    }

    public boolean areTeleportRequestsDisabled() {

        return teleportRequestsDisabled;

    }

    public void setTeleportRequestsDisabled(boolean teleportRequestsDisabled) {

        this.teleportRequestsDisabled = teleportRequestsDisabled;

    }

}
