package com.kaixeleron.essentials.database.converter;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.database.DatabaseException;
import com.kaixeleron.essentials.database.YAMLDatabase;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class YAMLConverter implements DatabaseConverter {

    private final EssentialsMain m;

    private final File playerDataFolder;

    private final YAMLDatabase yamlDatabase;

    private final PlayerDataManager playerDataManager;

    private DatabasePlayerRecord[] records;

    public YAMLConverter(EssentialsMain m, File playerDataFolder, YAMLDatabase yamlDatabase, PlayerDataManager playerDataManager) {

        this.m = m;

        this.playerDataFolder = playerDataFolder;

        this.yamlDatabase = yamlDatabase;

        this.playerDataManager = playerDataManager;
        
        records = new DatabasePlayerRecord[0];

    }

    @Override
    public void loadRecords() {

        File[] files = playerDataFolder.listFiles();
        
        if (files != null) {

            System.out.println(String.format("Found %d records.", files.length));
            System.out.println("Allocating record array...");

            records = new DatabasePlayerRecord[files.length];

            System.out.println("Array allocated.");
            System.out.println("Importing records...");
            
            for (int i = 0; i < files.length; i++) {

                FileConfiguration playerData = YamlConfiguration.loadConfiguration(files[i]);

                Map<String, Location> homes = new HashMap<>();

                ConfigurationSection homeSection = playerData.getConfigurationSection("homes");
                
                if (homeSection != null) for (String s : homeSection.getKeys(false)) {

                    homes.put(s, new Location(Bukkit.getWorld(homeSection.getString(String.format("%s.world", s))), homeSection.getDouble(String.format("%s.x", s)), homeSection.getDouble(String.format("%s.y", s)), homeSection.getDouble(String.format("%s.z", s)), (float) homeSection.getDouble(String.format("%s.yaw", s)), (float) homeSection.getDouble(String.format("%s.pitch", s))));

                }

                Map<String, String> powertools = new HashMap<>();

                ConfigurationSection powertoolSection = playerData.getConfigurationSection("powerTools");

                if (powertoolSection != null) for (String s : powertoolSection.getKeys(false)) {

                    powertools.put(s, powertoolSection.getString(s));

                }

                Map<String, Long> cooldowns = new HashMap<>();

                ConfigurationSection cooldownSection = playerData.getConfigurationSection("kitCooldowns");

                if (cooldownSection != null) {

                    for (String s : cooldownSection.getKeys(false)) {

                        cooldowns.put(s, cooldownSection.getLong(s, 0L));

                    }

                }
                
                DatabasePlayerRecord record = new DatabasePlayerRecord(UUID.fromString(files[i].getName().replace(".yml", "")), playerData.getDouble("economy.balance", 0.0D), homes.keySet().toArray(new String[homes.size()]), homes.values().toArray(new Location[homes.size()]), playerData.getStringList("ignored").toArray(new String[playerData.getStringList("ignored").size()]), powertools.keySet().toArray(new String[powertools.size()]), powertools.values().toArray(new String[powertools.size()]), cooldowns.keySet().toArray(new String[cooldowns.size()]), cooldowns.values().toArray(new Long[cooldowns.size()]), playerData.getString("nickname", ""), playerData.getBoolean("teleportation.acceptingAll", false), playerData.getBoolean("messaging.disabled", false), playerData.getBoolean("economy.payDisabled", false), playerData.getBoolean("messaging.socialSpy", false), playerData.getBoolean("teleportation.disabled", false));
                
                records[i] = record;
                
            }
            
        }
        
    }

    @Override
    public DatabasePlayerRecord[] getRecords() {
        
        return records;
        
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void importRecords(DatabasePlayerRecord[] records, boolean refresh) throws DatabaseException {

        System.out.println(String.format("Found %d records to import.", records.length));
        System.out.println("Importing records...");

        for (DatabasePlayerRecord record : records) {

            File f = new File(playerDataFolder, String.format("%s.yml", record.getPlayerID().toString()));

            if (!f.exists()) {

                try {

                    f.createNewFile();

                } catch (IOException e) {

                    System.err.println("Could not create player data file. Import cancelled.");

                    throw new DatabaseException(e);

                }

            }

            FileConfiguration playerData = YamlConfiguration.loadConfiguration(f);

            playerData.set("economy.balance", record.getBalance());

            for (int i = 0; i < record.getHomeNames().length; i++) {

                Location l = record.getHomes()[i];

                ConfigurationSection homes = playerData.getConfigurationSection("homes");

                if (homes == null) {

                    homes = playerData.createSection("homes");

                }

                homes.set(String.format("%s.world", record.getHomeNames()[i]), l.getWorld().getName());
                homes.set(String.format("%s.x", record.getHomeNames()[i]), l.getX());
                homes.set(String.format("%s.y", record.getHomeNames()[i]), l.getY());
                homes.set(String.format("%s.z", record.getHomeNames()[i]), l.getZ());
                homes.set(String.format("%s.yaw", record.getHomeNames()[i]), l.getYaw());
                homes.set(String.format("%s.pitch", record.getHomeNames()[i]), l.getPitch());

            }

            if (record.getIgnored().length > 0) playerData.set("ignored", Arrays.asList(record.getIgnored()));

            for (int i = 0; i < record.getPowertoolMaterials().length; i++) {

                playerData.set(String.format("powerTools.%s", record.getPowertoolMaterials()[i]), record.getPowertoolCommands()[i]);

            }

            for (int i = 0; i < record.getKitCooldownNames().length; i++) {

                playerData.set(String.format("kitCooldowns.%s", record.getKitCooldownNames()[i]), record.getKitCooldowns()[i]);

            }

            if (record.getNickname() != null && record.getNickname().length() > 0) playerData.set("nickname", record.getNickname());

            if (record.isAcceptingAllTeleports()) playerData.set("teleportation.acceptingAll", true);
            if (record.isMessagingDisabled()) playerData.set("messaging.disabled", true);
            if (record.isPayDisabled()) playerData.set("economy.payDisabled", true);
            if (record.isSocialSpyEnabled()) playerData.set("messaging.socialSpy", true);
            if (record.areTeleportRequestsDisabled()) playerData.set("teleportation.disabled", true);

            try {

                playerData.save(f);

            } catch (IOException e) {

                System.err.println("Could not save player data file. Import cancelled.");

                throw new DatabaseException(e);

            }

        }

        System.out.println("YAML import complete.");

        if (refresh) {

            new BukkitRunnable() {

                @Override
                public void run() {

                    System.out.println("Loading new data...");

                    yamlDatabase.close();
                    playerDataManager.clearCache();

                    for (Player p : Bukkit.getOnlinePlayers()) {

                        yamlDatabase.load(p.getUniqueId());
                        playerDataManager.loadData(p);

                    }

                    yamlDatabase.loadBalTop();

                    System.out.println("Load complete.");

                }

            }.runTask(m);

        }

    }

}
