package com.kaixeleron.essentials.database.converter;

import org.bukkit.Location;

import java.util.UUID;

public class EssentialsXNameBasedPlayerRecord {

    private final double balance;

    private final String[] homeNames;

    private final Location[] homes;

    private final String[] ignored, powertoolMaterials, powertoolCommands, kitCooldownNames;

    private final Long[] kitCooldowns;

    private final String name, nickname;

    private final boolean acceptingAllTeleports, messagingDisabled, payDisabled, socialSpyEnabled, teleportRequestsDisabled;

    public EssentialsXNameBasedPlayerRecord(String name, double balance, String[] homeNames, Location[] homes, String[] ignored, String[] powertoolMaterials, String[] powertoolCommands, String[] kitCooldownNames, Long[] kitCooldowns, String nickname, boolean acceptingAllTeleports, boolean messagingDisabled, boolean payDisabled, boolean socialSpyEnabled, boolean teleportRequestsDisabled) {

        this.name = name;

        this.balance = balance;
        this.homeNames = homeNames;
        this.homes = homes;
        this.ignored = ignored;
        this.powertoolMaterials = powertoolMaterials;
        this.powertoolCommands = powertoolCommands;
        this.kitCooldownNames = kitCooldownNames;
        this.kitCooldowns = kitCooldowns;
        this.nickname = nickname;
        this.acceptingAllTeleports = acceptingAllTeleports;
        this.messagingDisabled = messagingDisabled;
        this.payDisabled = payDisabled;
        this.socialSpyEnabled = socialSpyEnabled;
        this.teleportRequestsDisabled = teleportRequestsDisabled;

    }

    public String getName() {

        return name;

    }

    public DatabasePlayerRecord convertToDatabasePlayerRecord(UUID id) {

        return new DatabasePlayerRecord(id, balance, homeNames, homes, ignored, powertoolMaterials, powertoolCommands, kitCooldownNames, kitCooldowns, nickname, acceptingAllTeleports, messagingDisabled, payDisabled, socialSpyEnabled, teleportRequestsDisabled);

    }

}
