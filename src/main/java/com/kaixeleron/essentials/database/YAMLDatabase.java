package com.kaixeleron.essentials.database;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.database.converter.DatabaseConverter;
import com.kaixeleron.essentials.database.converter.YAMLConverter;
import com.kaixeleron.essentials.database.data.BalTopEntry;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class YAMLDatabase implements EssentialsDatabase {

    private final EssentialsMain m;

    private final PlayerDataManager playerDataManager;

    private final File dataFolder;

    private final Map<UUID, FileConfiguration> dataFiles;

    private final List<BalTopEntry> baltop;

    private boolean baltopLoaded, baltopSorting;

    private double balanceTotal;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public YAMLDatabase(EssentialsMain m, PlayerDataManager playerDataManager, File dataFolder) {

        this.m = m;

        this.playerDataManager = playerDataManager;

        this.dataFolder = new File(dataFolder, "playerData");
        this.dataFolder.mkdir();

        dataFiles = new HashMap<>();

        baltop = Collections.synchronizedList(new ArrayList<>());

        baltopLoaded = false;

        baltopSorting = false;

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void saveDefault(UUID id) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            File f = new File(dataFolder, String.format("%s.yml", id.toString()));

            if (!f.exists()) {

                try {

                    f.createNewFile();

                } catch (IOException e) {

                    throw new DatabaseException(e);

                }

            }

        }

    }

    @Override
    public void load(UUID id) {

        dataFiles.put(id, YamlConfiguration.loadConfiguration(new File(dataFolder, String.format("%s.yml", id.toString()))));

    }

    @Override
    public double getBalance(UUID id) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        return dataFiles.get(id).getDouble("economy.balance", -1.0D);

    }

    @Override
    public void setBalance(UUID id, double balance) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        balanceTotal = balanceTotal + (balance - getBalance(id));

        synchronized (baltop) {

            for (BalTopEntry entry : baltop) {

                if (entry.getId().equals(id)) {

                    entry.setAmount(balance);

                    new BukkitRunnable() {

                        @Override
                        public void run() {

                            sortBalTop();

                        }

                    }.runTaskAsynchronously(m);

                    break;

                }

            }

        }

        dataFiles.get(id).set("economy.balance", balance);
        save(id);

    }

    @Override
    public void addIgnored(UUID id, UUID ignored) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        List<String> list = dataFiles.get(id).getStringList("ignored");

        if (!list.contains(ignored.toString())) {

            list.add(ignored.toString());

        }

        dataFiles.get(id).set("ignored", list);
        save(id);

    }

    @Override
    public void removeIgnored(UUID id, UUID ignored) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        List<String> list = dataFiles.get(id).getStringList("ignored");

        if (list.contains(ignored.toString())) {

            list.remove(ignored.toString());

            if (list.isEmpty()) list = null;

            dataFiles.get(id).set("ignored", list);

            save(id);

        }

    }

    @Override
    public boolean isIgnored(UUID id, UUID ignored) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        return dataFiles.containsKey(id) && dataFiles.get(id).getStringList("ignored").contains(ignored.toString());

    }

    @Override
    public void setMessagingDisabled(UUID id, boolean disabled) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        dataFiles.get(id).set("messaging.disabled", disabled ? true : null);
        save(id);

    }

    @Override
    public boolean isMessagingDisabled(UUID id) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        return dataFiles.get(id).getBoolean("messaging.disabled", false);

    }

    @Override
    public void setNickname(UUID id, String nickname) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        dataFiles.get(id).set("nickname", nickname);
        save(id);

    }

    @Override
    public String getNickname(UUID id) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        return dataFiles.get(id).getString("nickname", "");

    }

    @Override
    public void setPowertool(UUID id, String type, String command) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        dataFiles.get(id).set(String.format("powerTools.%s", type), command);
        save(id);

    }

    @Override
    public Map<String, String> getPowertools(UUID id) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        Map<String, String> out = new HashMap<>();

        ConfigurationSection section = dataFiles.get(id).getConfigurationSection("powertools");

        if (section != null) {

            for (String s : section.getKeys(false)) {

                out.put(s, section.getString(s));

            }

        }

        return out;

    }

    @Override
    public void removePowertool(UUID id, String type) throws DatabaseException {

        if (dataFiles.containsKey(id)) {

            FileConfiguration file = dataFiles.get(id);

            file.set(String.format("powerTools.%s", type), null);

            if (file.getConfigurationSection("powerTools").getKeys(false).size() == 0) {

                file.set("powerTools", null);

            }

            save(id);

        }

    }

    @Override
    public void setSocialSpy(UUID id, boolean socialspy) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        dataFiles.get(id).set("messaging.socialSpy", socialspy ? true : null);
        save(id);

    }

    @Override
    public boolean isSocialSpyEnabled(UUID id) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        return dataFiles.get(id).getBoolean("messaging.socialSpy", false);

    }

    @Override
    public void setAcceptingAllTeleports(UUID id, boolean enabled) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        dataFiles.get(id).set("teleportation.acceptingAll", enabled ? true : null);
        save(id);

    }

    @Override
    public boolean isAcceptingAllTeleports(UUID id) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        return dataFiles.get(id).getBoolean("teleportation.acceptingAll", false);

    }

    @Override
    public void setTeleportRequestsDisabled(UUID id, boolean enabled) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        dataFiles.get(id).set("teleportation.disabled", enabled ? true : null);
        save(id);

    }

    @Override
    public boolean areTeleportRequestsDisabled(UUID id) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        return dataFiles.get(id).getBoolean("teleportation.disabled", false);

    }

    @Override
    public void setHome(UUID id, String name, Location loc) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        ConfigurationSection homes = dataFiles.get(id).getConfigurationSection("homes");

        if (homes == null) {

            homes = dataFiles.get(id).createSection("homes");

        }

        homes.set(String.format("%s.world", name), loc.getWorld().getName());
        homes.set(String.format("%s.x", name), loc.getX());
        homes.set(String.format("%s.y", name), loc.getY());
        homes.set(String.format("%s.z", name), loc.getZ());
        homes.set(String.format("%s.yaw", name), loc.getYaw());
        homes.set(String.format("%s.pitch", name), loc.getPitch());

        save(id);

    }

    @Override
    public void removeHome(UUID id, String name) throws DatabaseException {

        if (dataFiles.containsKey(id)) {

            ConfigurationSection homes = dataFiles.get(id).getConfigurationSection("homes");

            if (homes != null) {

                homes.set(name, null);

                save(id);

            }

        }

    }

    @Override
    public Location getHome(UUID id, String name) throws DatabaseException {

        Location out = null;

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        ConfigurationSection homes = dataFiles.get(id).getConfigurationSection(String.format("homes.%s", name));

        if (homes != null && homes.getString("world") != null) {

            out = new Location(Bukkit.getWorld(homes.getString("world")), homes.getDouble("x"), homes.getDouble("y"), homes.getDouble("z"), (float) homes.getDouble("yaw"), (float) homes.getDouble("pitch"));

        }

        return out;

    }

    @Override
    public Set<String> listHomes(UUID id) throws DatabaseException {

        Set<String> out = new HashSet<>();

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        ConfigurationSection homes = dataFiles.get(id).getConfigurationSection("homes");

        if (homes != null) {

            out.addAll(homes.getKeys(false));

        }

        return out;

    }

    @Override
    public void setPayDisabled(UUID id, boolean disabled) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        dataFiles.get(id).set("economy.payDisabled", disabled ? true : null);

        save(id);

    }

    @Override
    public boolean isPayDisabled(UUID id) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        return dataFiles.get(id).getBoolean("economy.payDisabled", false);

    }

    @Override
    public void setKitCooldown(UUID id, String kit, long cooldown) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        if (cooldown == 0L) {

            dataFiles.get(id).set(String.format("kitCooldowns.%s", kit), null);

        } else {

            dataFiles.get(id).set(String.format("kitCooldowns.%s", kit), cooldown);

        }

        save(id);

    }

    @Override
    public long getKitCooldown(UUID id, String kit) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        return dataFiles.get(id).getLong(String.format("kitCooldowns.%s", kit), 0L);

    }

    @Override
    public void loadBalTop() {

        new BukkitRunnable() {

            @Override
            public void run() {

                if (dataFolder != null && dataFolder.listFiles() != null) {

                    for (File f : dataFolder.listFiles()) {

                        UUID id = UUID.fromString(f.getName().replace(".yml", ""));

                        OfflinePlayer p = Bukkit.getOfflinePlayer(id);

                        String name = p.getName();

                        if (name == null) name = p.getUniqueId().toString();

                        double balance = YamlConfiguration.loadConfiguration(f).getDouble("economy.balance");

                        synchronized (baltop) {

                            baltop.add(new BalTopEntry(name, id, balance));

                        }

                        balanceTotal = balanceTotal + balance;

                    }

                    sortBalTop();

                    baltopLoaded = true;

                }

            }

        }.runTaskAsynchronously(m);

    }

    @Override
    public boolean isBalTopLoaded() {

        return baltopLoaded;

    }

    @Override
    public List<BalTopEntry> getBalTop(int page) {

        List<BalTopEntry> out = new ArrayList<>();

        if (baltopLoaded) {

            synchronized (baltop) {

                for (int i = (page - 1) * 10; i < page * 10; i++) {

                    if (i >= baltop.size()) {

                        break;

                    } else {

                        out.add(baltop.get(i));

                    }

                }

            }

        }

        return out;

    }

    @Override
    public int getBalTopPages() {

        if (baltopLoaded) {

            synchronized (baltop) {

                return (int) Math.ceil((double) baltop.size() / 10.0D);

            }

        } else {

            return 0;

        }

    }

    @Override
    public double getBalanceTotal() {

        return balanceTotal;

    }

    @Override
    public void setLoginRespawn(UUID id, boolean respawn) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        dataFiles.get(id).set("teleportation.spawnonlogin", respawn ? true : null);
        save(id);

    }

    @Override
    public boolean isRespawnOnLoginSet(UUID id) throws DatabaseException {

        if (!dataFiles.containsKey(id)) {

            saveDefault(id);
            load(id);

        }

        return dataFiles.get(id).getBoolean("teleportation.spawnonlogin", false);

    }

    @Override
    public void close() {

        synchronized (baltop) {

            baltop.clear();

        }

        baltopLoaded = false;

        dataFiles.clear();

    }

    private void save(UUID id) throws DatabaseException {

        try {

            dataFiles.get(id).save(new File(dataFolder, String.format("%s.yml", id.toString())));

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    private void sortBalTop() {

        if (!baltopSorting) {

            baltopSorting = true;

            BalTopEntry[] sort;

            synchronized (baltop) {

                sort = baltop.toArray(new BalTopEntry[baltop.size()]);

            }

            sort(sort, 0, sort.length - 1);

            synchronized (baltop) {

                baltop.clear();

                baltop.addAll(Arrays.asList(sort));

            }

            baltopSorting = false;

        }

    }

    private void sort(BalTopEntry[] array, int low, int high) {

        if (low < high) {

            int index = partition(array, low, high);

            sort(array, low, index - 1);
            sort(array, index + 1, high);

        }

    }

    private int partition(BalTopEntry[] array, int low, int high) {

        BalTopEntry pivot = array[high];

        int i = low - 1;

        for (int j = low; j < high; j++) {

            if (array[j].getAmount() >= pivot.getAmount()) {

                i++;

                BalTopEntry temp = array[i];

                array[i] = array[j];
                array[j] = temp;

            }

        }

        BalTopEntry temp = array[i + 1];
        array[i + 1] = array[high];
        array[high] = temp;

        return i + 1;

    }

    @Override
    public DatabaseConverter getConverter() {

        return new YAMLConverter(m, dataFolder, this, playerDataManager);

    }

}
