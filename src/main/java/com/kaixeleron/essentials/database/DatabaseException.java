package com.kaixeleron.essentials.database;

public class DatabaseException extends Exception {

    public DatabaseException(Exception parent) {

        super(parent);

    }

}
