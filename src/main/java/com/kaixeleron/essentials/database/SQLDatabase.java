package com.kaixeleron.essentials.database;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.database.converter.DatabaseConverter;
import com.kaixeleron.essentials.database.converter.SQLConverter;
import com.kaixeleron.essentials.database.data.BalTopEntry;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;

import java.sql.*;
import java.util.*;

public class SQLDatabase implements EssentialsDatabase {

    private final EssentialsMain m;

    private Connection c = null;

    private final PlayerDataManager playerDataManager;

    private final String hostname, database, username, password;

    private final int port;

    public SQLDatabase(EssentialsMain m, PlayerDataManager playerDataManager, String hostname, int port, String database, String username, String password) throws DatabaseException {

        this.m = m;

        this.playerDataManager = playerDataManager;

        this.hostname = hostname;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;

        try {

            connect();

            c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxessentials_balances` (`uuid` CHAR(36), `balance` DOUBLE, PRIMARY KEY(`uuid`));").executeUpdate();
            c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxessentials_ignored` (`uuid` CHAR(36), `target` CHAR(36), PRIMARY KEY(`uuid`, `target`));").executeUpdate();
            c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxessentials_homes` (`uuid` CHAR(36), `name` VARCHAR(256), `world` VARCHAR(64), `x` DOUBLE, `y` DOUBLE, `z` DOUBLE, `yaw` FLOAT, `pitch` FLOAT, PRIMARY KEY(`uuid`, `name`));").executeUpdate();
            c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxessentials_powertools` (`uuid` CHAR(36), `material` VARCHAR(64), `command` VARCHAR(256), PRIMARY KEY(`uuid`, `material`));").executeUpdate();
            c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxessentials_kitcooldowns` (`uuid` CHAR(36), `kit` VARCHAR(256), `cooldown` BIGINT, PRIMARY KEY(`uuid`, `kit`));").executeUpdate();
            c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxessentials_usersettings` (`uuid` CHAR(36), `nickname` VARCHAR(16), `messagingdisabled` BIT, `socialspy` BIT, `acceptingallteleports` BIT, `teleportsdisabled` BIT, `paydisabled` BIT, `spawnonlogin` BIT, PRIMARY KEY(`uuid`));").executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        }

    }

    private void connect() throws SQLException {

        if (c == null || c.isClosed()) c = DriverManager.getConnection(String.format("jdbc:mysql://%s:%d/%s?useSSL=false", hostname, port, database), username, password);

    }

    private void closeSilently(AutoCloseable... closeables) {

        for (AutoCloseable closeable : closeables) {

            if (closeable != null) {

                try {

                    closeable.close();

                } catch (Exception ignored) {}

            }

        }

    }

    @Override
    public void saveDefault(UUID id) throws DatabaseException {

        PreparedStatement check = null, insert = null;
        ResultSet rs = null;

        try {

            connect();

            check = c.prepareStatement("SELECT * FROM `kxessentials_usersettings` WHERE `uuid` = ?;");
            check.setString(1, id.toString());

            rs = check.executeQuery();

            if (!rs.next()) {

                insert = c.prepareStatement("INSERT INTO `kxessentials_usersettings` (`uuid`, `nickname`, `messagingdisabled`, `socialspy`, `acceptingallteleports`, `teleportsdisabled`, `paydisabled`) VALUES (?, ?, ?, ?, ?, ?, ?);");
                insert.setString(1, id.toString());
                insert.setString(2, "");
                insert.setBoolean(3, false);
                insert.setBoolean(4, false);
                insert.setBoolean(5, false);
                insert.setBoolean(6, false);
                insert.setBoolean(7, false);

                insert.executeUpdate();

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(check, insert, rs);

        }

    }

    @Override
    public void load(UUID id) {}

    @Override
    public double getBalance(UUID id) throws DatabaseException {

        double balance = -1.0D;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `balance` FROM `kxessentials_balances` WHERE `uuid` = ?;");
            get.setString(1, id.toString());

            rs = get.executeQuery();

            if (rs.next()) {

                balance = rs.getDouble(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return balance;

    }

    @Override
    public void setBalance(UUID id, double balance) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_balances` (`uuid`, `balance`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `balance` = VALUES(`balance`);");
            insert.setString(1, id.toString());
            insert.setDouble(2, balance);

            insert.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    @Override
    public void addIgnored(UUID id, UUID ignored) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_ignored` (`uuid`, `target`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `uuid`=`uuid`;");
            insert.setString(1, id.toString());
            insert.setString(2, ignored.toString());

            insert.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    @Override
    public void removeIgnored(UUID id, UUID ignored) throws DatabaseException {

        PreparedStatement delete = null;

        try {

            connect();

            delete = c.prepareStatement("DELETE FROM `kxessentials_ignored` WHERE `uuid` = ? AND `target` = ?;");
            delete.setString(1, id.toString());
            delete.setString(2, ignored.toString());

            delete.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(delete);

        }

    }

    @Override
    public boolean isIgnored(UUID id, UUID ignored) throws DatabaseException {

        boolean out;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxessentials_ignored` WHERE `uuid` = ? AND `target` = ?;");
            get.setString(1, id.toString());
            get.setString(2, ignored.toString());

            rs = get.executeQuery();

            out = rs.next();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void setMessagingDisabled(UUID id, boolean disabled) throws DatabaseException {

        PreparedStatement update = null;

        try {

            connect();

            update = c.prepareStatement("UPDATE `kxessentials_usersettings` SET `messagingdisabled` = ? WHERE `uuid` = ?;");
            update.setBoolean(1, disabled);
            update.setString(2, id.toString());

            update.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(update);

        }

    }

    @Override
    public boolean isMessagingDisabled(UUID id) throws DatabaseException {

        boolean out = false;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `messagingdisabled` FROM `kxessentials_usersettings` WHERE `uuid` = ?;");
            get.setString(1, id.toString());

            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getBoolean(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void setNickname(UUID id, String nickname) throws DatabaseException {

        if (nickname != null && nickname.length() > 16) nickname = nickname.substring(0, 16);

        PreparedStatement update = null;

        try {

            connect();

            update = c.prepareStatement("UPDATE `kxessentials_usersettings` SET `nickname` = ? WHERE `uuid` = ?;");
            update.setString(1, nickname == null ? "" : nickname);
            update.setString(2, id.toString());

            update.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(update);

        }

    }

    @Override
    public String getNickname(UUID id) throws DatabaseException {

        String out = "";

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `nickname` FROM `kxessentials_usersettings` WHERE `uuid` = ?;");
            get.setString(1, id.toString());

            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getString(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void setPowertool(UUID id, String type, String command) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_powertools` (`uuid`, `material`, `command`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `command` = VALUES(`command`);");
            insert.setString(1, id.toString());
            insert.setString(2, type);
            insert.setString(3, command);

            insert.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    @Override
    public Map<String, String> getPowertools(UUID id) throws DatabaseException {

        Map<String, String> out = new HashMap<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `material`, `command` FROM `kxessentials_powertools` WHERE `uuid` = ?;");
            get.setString(1, id.toString());

            rs = get.executeQuery();

            while (rs.next()) {

                out.put(rs.getString(1), rs.getString(2));

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;
    }

    @Override
    public void removePowertool(UUID id, String type) throws DatabaseException {

        PreparedStatement delete = null;

        try {

            connect();

            delete = c.prepareStatement("DELETE FROM `kxessentials_powertools` WHERE `uuid` = ? AND `material` = ?;");
            delete.setString(1, id.toString());
            delete.setString(2, type);

            delete.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(delete);

        }

    }

    @Override
    public void setSocialSpy(UUID id, boolean socialspy) throws DatabaseException {

        PreparedStatement update = null;

        try {

            connect();

            update = c.prepareStatement("UPDATE `kxessentials_usersettings` SET `socialspy` = ? WHERE `uuid` = ?;");
            update.setBoolean(1, socialspy);
            update.setString(2, id.toString());

            update.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(update);

        }

    }

    @Override
    public boolean isSocialSpyEnabled(UUID id) throws DatabaseException {

        boolean out = false;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `socialspy` FROM `kxessentials_usersettings` WHERE `uuid` = ?;");
            get.setString(1, id.toString());

            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getBoolean(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void setAcceptingAllTeleports(UUID id, boolean enabled) throws DatabaseException {

        PreparedStatement update = null;

        try {

            connect();

            update = c.prepareStatement("UPDATE `kxessentials_usersettings` SET `acceptingallteleports` = ? WHERE `uuid` = ?;");
            update.setBoolean(1, enabled);
            update.setString(2, id.toString());

            update.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(update);

        }

    }

    @Override
    public boolean isAcceptingAllTeleports(UUID id) throws DatabaseException {

        boolean out = false;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `acceptingallteleports` FROM `kxessentials_usersettings` WHERE `uuid` = ?;");
            get.setString(1, id.toString());

            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getBoolean(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void setTeleportRequestsDisabled(UUID id, boolean enabled) throws DatabaseException {

        PreparedStatement update = null;

        try {

            connect();

            update = c.prepareStatement("UPDATE `kxessentials_usersettings` SET `teleportsdisabled` = ? WHERE `uuid` = ?;");
            update.setBoolean(1, enabled);
            update.setString(2, id.toString());

            update.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(update);

        }

    }

    @Override
    public boolean areTeleportRequestsDisabled(UUID id) throws DatabaseException {

        boolean out = false;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `teleportsdisabled` FROM `kxessentials_usersettings` WHERE `uuid` = ?;");
            get.setString(1, id.toString());

            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getBoolean(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void setHome(UUID id, String name, Location loc) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_homes` (`uuid`, `name`, `world`, `x`, `y`, `z`, `yaw`, `pitch`) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `world` = VALUES(`world`), `x` = VALUES(`x`), `y` = VALUES(`y`), `z` = VALUES(`z`), `yaw` = VALUES(`yaw`), `pitch` = VALUES(`pitch`);");
            insert.setString(1, id.toString());
            insert.setString(2, name);
            insert.setString(3, loc.getWorld().getName());
            insert.setDouble(4, loc.getX());
            insert.setDouble(5, loc.getY());
            insert.setDouble(6, loc.getZ());
            insert.setFloat(7, loc.getYaw());
            insert.setFloat(8, loc.getPitch());

            insert.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    @Override
    public void removeHome(UUID id, String name) throws DatabaseException {

        PreparedStatement delete = null;

        try {

            connect();

            delete = c.prepareStatement("DELETE FROM `kxessentials_homes` WHERE `uuid` = ? AND `name` = ?;");
            delete.setString(1, id.toString());
            delete.setString(2, name);

            delete.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(delete);

        }

    }

    @Override
    public Location getHome(UUID id, String name) throws DatabaseException {

        Location out = null;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `world`, `x`, `y`, `z`, `yaw`, `pitch` FROM `kxessentials_homes` WHERE `uuid` = ? AND `name` = ?;");
            get.setString(1, id.toString());
            get.setString(2, name);

            rs = get.executeQuery();

            if (rs.next() && rs.getString(1) != null) {

                out = new Location(Bukkit.getWorld(rs.getString(1)), rs.getDouble(2), rs.getDouble(3), rs.getDouble(4), rs.getFloat(5), rs.getFloat(6));

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public Set<String> listHomes(UUID id) throws DatabaseException {

        Set<String> out = new HashSet<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `name` FROM `kxessentials_homes` WHERE `uuid` = ?;");
            get.setString(1, id.toString());

            rs = get.executeQuery();

            while (rs.next()) {

                out.add(rs.getString(1));

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void setPayDisabled(UUID id, boolean disabled) throws DatabaseException {

        PreparedStatement update = null;

        try {

            connect();

            update = c.prepareStatement("UPDATE `kxessentials_usersettings` SET `paydisabled` = ? WHERE `uuid` = ?;");
            update.setBoolean(1, disabled);
            update.setString(2, id.toString());

            update.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(update);

        }

    }

    @Override
    public boolean isPayDisabled(UUID id) throws DatabaseException {

        boolean out = false;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `paydisabled` FROM `kxessentials_usersettings` WHERE `uuid` = ?;");
            get.setString(1, id.toString());

            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getBoolean(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void setKitCooldown(UUID id, String kit, long cooldown) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxessentials_kitcooldowns` (`uuid`, `kit`, `cooldown`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `cooldown` = VALUES(`cooldown`);");
            insert.setString(1, id.toString());
            insert.setString(2, kit);
            insert.setLong(3, cooldown);

            insert.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    @Override
    public long getKitCooldown(UUID id, String kit) throws DatabaseException {

        long out = 0L;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `cooldown` FROM `kxessentials_kitcooldowns` WHERE `uuid` = ? AND `kit` = ?;");
            get.setString(1, id.toString());
            get.setString(2, kit);

            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getLong(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void loadBalTop() {}

    @Override
    public boolean isBalTopLoaded() {

        return true;

    }

    @Override
    public List<BalTopEntry> getBalTop(int page) throws DatabaseException {

        List<BalTopEntry> out = new ArrayList<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxessentials_balances` ORDER BY `balance` DESC LIMIT 10 OFFSET ?;");
            get.setInt(1, (page - 1) * 10);

            rs = get.executeQuery();

            while (rs.next()) {

                OfflinePlayer o = Bukkit.getOfflinePlayer(UUID.fromString(rs.getString(1)));

                out.add(new BalTopEntry(o.getName() == null ? rs.getString(1) : o.getName(), o.getUniqueId(), rs.getDouble(2)));

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public int getBalTopPages() throws DatabaseException {

        int pages = 0;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT COUNT(*) FROM `kxessentials_balances`;");

            rs = get.executeQuery();

            if (rs.next()) {

                pages = (int) Math.ceil((double) rs.getInt(1) / 10.0D);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return pages;

    }

    @Override
    public double getBalanceTotal() throws DatabaseException {

        double out = 0.0D;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT SUM(`balance`) FROM `kxessentials_balances`;");

            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getDouble(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;
    }

    @Override
    public void setLoginRespawn(UUID id, boolean respawn) throws DatabaseException {

        PreparedStatement update = null;

        try {

            connect();

            update = c.prepareStatement("UPDATE `kxessentials_usersettings` SET `spawnonlogin` = ? WHERE `uuid` = ?;");
            update.setBoolean(1, respawn);
            update.setString(2, id.toString());

            update.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(update);

        }

    }

    @Override
    public boolean isRespawnOnLoginSet(UUID id) throws DatabaseException {

        boolean out = false;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `spawnonlogin` FROM `kxessentials_usersettings` WHERE `uuid` = ?;");
            get.setString(1, id.toString());

            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getBoolean(1);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void close() throws Exception {

        if (c != null) c.close();

    }

    @Override
    public DatabaseConverter getConverter() throws DatabaseException {

        return new SQLConverter(m, playerDataManager, hostname, port, database, username, password);

    }

}
