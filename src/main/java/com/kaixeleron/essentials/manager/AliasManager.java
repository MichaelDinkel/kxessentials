package com.kaixeleron.essentials.manager;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import javax.annotation.Nullable;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AliasManager {

    private final File aliasFile;

    private FileConfiguration aliases;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public AliasManager(File dataFolder) {

        aliasFile = new File(dataFolder, "aliases.yml");

        if (!aliasFile.exists()) {

            try {

                aliasFile.createNewFile();
                writeDefault(aliasFile);

            } catch (IOException e) {

                System.err.println("Could not create aliases.yml file.");
                e.printStackTrace();

            }

        }

        reload();

    }

    public @Nullable Material getByAlias(String alias) {

        Material out = null;

        for (String material : aliases.getKeys(false)) {

            if (aliases.getStringList(material).contains(alias)) {

                out = Material.valueOf(material);

                break;

            }

        }

        return out;

    }

    public void reload() {

        aliases = YamlConfiguration.loadConfiguration(aliasFile);

    }

    private void writeDefault(File file) throws IOException {

        BufferedWriter writer = null;

        try {

            writer = new BufferedWriter(new FileWriter(file));

            writer.write("EXPERIENCE_BOTTLE:");
            writer.newLine();
            writer.write("    - \"xpbottle\"");
            writer.newLine();

            writer.flush();

        } finally {

            if (writer != null) writer.close();

        }

    }

}
