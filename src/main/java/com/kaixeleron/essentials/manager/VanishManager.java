package com.kaixeleron.essentials.manager;

import com.kaixeleron.essentials.EssentialsMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class VanishManager {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final Set<Player> vanished;

    public VanishManager(EssentialsMain m, TextManager textManager) {

        this.m = m;

        this.textManager = textManager;

        vanished = new HashSet<>();

    }

    public void vanish(Player p) {

        vanished.add(p);

        for (Player player : Bukkit.getOnlinePlayers()) {

            if (!p.equals(player) && !player.hasPermission("kxessentials.vanish.see")) {

                player.hidePlayer(m, p);

            }

        }

    }

    public void visible(Player p) {

        vanished.remove(p);

        for (Player player : Bukkit.getOnlinePlayers()) {

            if (!p.equals(player) && !player.canSee(p)) {

                player.showPlayer(m, p);

            }

        }

    }

    public boolean isVanished(Player p) {

        return vanished.contains(p);

    }

    public void clearVanished() {

        for (Player p : vanished) {

            for (Player player : Bukkit.getOnlinePlayers()) {

                if (!p.equals(player) && !player.canSee(p)) {

                    player.showPlayer(m, p);

                }

            }

            p.sendMessage(textManager.getMainColor() + "You have been made visible.");

        }

        vanished.clear();

    }

}
