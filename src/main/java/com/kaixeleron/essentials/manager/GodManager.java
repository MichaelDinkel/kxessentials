package com.kaixeleron.essentials.manager;

import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class GodManager {

    private final Set<Player> gods;

    public GodManager() {

        gods = new HashSet<>();

    }

    public void addGod(Player p) {

        gods.add(p);

    }

    public boolean isGod(Player p) {

        return gods.contains(p);

    }

    public void removeGod(Player p) {

        gods.remove(p);

    }

    public void clearGods() {

        gods.clear();

    }

}
