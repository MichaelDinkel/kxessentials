package com.kaixeleron.essentials.manager;

import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SpawnerManager {

    private final EntityType[] blacklisted;

    public SpawnerManager(List<String> blacklist) {

        Set<EntityType> blacklisted = new HashSet<>();

        for (String s : blacklist) {

            try {

                blacklisted.add(EntityType.valueOf(s));

            } catch (IllegalArgumentException e) {

                System.err.println(String.format("Invalid spawner blacklist entity type: %s", s));

            }

        }

        this.blacklisted = blacklisted.toArray(new EntityType[blacklisted.size()]);

    }

    public void registerPermissions() {

        Permission star = new Permission("kxessentials.spawner.*", PermissionDefault.FALSE);
        Bukkit.getPluginManager().addPermission(star);

        outer: for (EntityType e : EntityType.values()) {

            for (EntityType type : blacklisted) {

                if (e.equals(type)) continue outer;

            }

            Permission child = new Permission(String.format("kxessentials.spawner.%s", e.toString().toLowerCase().replace("_", "")), PermissionDefault.OP);
            child.addParent(star, true);
            Bukkit.getPluginManager().addPermission(child);

        }

    }

    public EntityType[] getValidEntities() {

        EntityType[] out = new EntityType[EntityType.values().length - blacklisted.length];

        EntityType[] values = EntityType.values();

        int offset = 0;

        for (int i = 0; i < out.length; i++) {

            boolean blacklisted = false;

            do {

                for (EntityType bad : this.blacklisted) {

                    if (values[i + offset].equals(bad)) {

                        blacklisted = true;
                        offset++;

                        break;

                    } else {

                        blacklisted = false;

                    }

                }

            } while (blacklisted);

            out[i] = values[i + offset];

        }

        return out;

    }

}
