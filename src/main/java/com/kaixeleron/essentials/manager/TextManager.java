package com.kaixeleron.essentials.manager;

import org.bukkit.ChatColor;

public class TextManager {

    private ChatColor main, alt, message, usage, error, socialspy, sign;

    private String currencySymbol;

    public TextManager() {

        main = ChatColor.RESET;
        alt = ChatColor.RESET;
        message = ChatColor.RESET;
        usage = ChatColor.RESET;
        error = ChatColor.RESET;
        socialspy = ChatColor.RESET;
        sign = ChatColor.RESET;

        currencySymbol = "";

    }

    public ChatColor getMainColor() {

        return main;

    }

    public void setMainColor(ChatColor main) {

        this.main = main;

    }

    public ChatColor getAltColor() {

        return alt;

    }

    public void setAltColor(ChatColor alt) {

        this.alt = alt;

    }

    public ChatColor getMessageColor() {

        return message;

    }

    public void setMessageColor(ChatColor message) {

        this.message = message;

    }

    public ChatColor getUsageColor() {

        return usage;

    }

    public void setUsageColor(ChatColor usage) {

        this.usage = usage;

    }

    public ChatColor getErrorColor() {

        return error;

    }

    public void setErrorColor(ChatColor error) {

        this.error = error;

    }

    public String getCurrencySymbol() {

        return currencySymbol;

    }

    public void setCurrencySymbol(String currencySymbol) {

        this.currencySymbol = currencySymbol;

    }

    public ChatColor getSocialSpyColor() {

        return socialspy;

    }

    public void setSocialSpyColor(ChatColor socialspy) {

        this.socialspy = socialspy;

    }

    public ChatColor getSignColor() {

        return sign;

    }

    public void setSignColor(ChatColor sign) {

        this.sign = sign;

    }

}
