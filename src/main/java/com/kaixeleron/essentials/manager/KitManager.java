package com.kaixeleron.essentials.manager;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class KitManager {

    private final TextManager textManager;

    private final Set<Player> viewing;

    private final File kitFile;
    private FileConfiguration kitData;

    private final Map<String, ItemStack[]> kits;
    private final Map<String, Long> kitCooldowns;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public KitManager(File dataFolder, TextManager textManager) {

        this.textManager = textManager;

        viewing = new HashSet<>();

        kitFile = new File(dataFolder, "kits.yml");

        if (!kitFile.exists()) {

            try {

                kitFile.createNewFile();

            } catch (IOException e) {

                System.err.println("Could not create a kits.yml file.");
                e.printStackTrace();

            }

        }

        kits = new HashMap<>();
        kitCooldowns = new HashMap<>();

    }

    @SuppressWarnings("deprecation")
    public void reload() {

        kitData = YamlConfiguration.loadConfiguration(kitFile);

        Permission grandStar = Bukkit.getPluginManager().getPermission("kxessentials.*");

        Permission star = Bukkit.getPluginManager().getPermission("kxessentials.kit.*");

        if (star == null) {

            star = new Permission("kxessentials.kit.*", PermissionDefault.FALSE);
            Bukkit.getPluginManager().addPermission(star);

        }

        star.addParent(grandStar, true);

        for (String kit : kitData.getKeys(false)) {

            Set<String> keys = kitData.getConfigurationSection(String.format("%s.items", kit)).getKeys(false);

            ItemStack[] items = new ItemStack[keys.size()];

            for (String s : keys) {

                try {

                    int position = Integer.parseInt(s);

                    ItemStack item = new ItemStack(Material.valueOf(kitData.getString(String.format("%s.items.%s.material", kit, s))));
                    item.setAmount(kitData.getInt(String.format("%s.items.%s.amount", kit, s)));
                    item.setDurability((short) kitData.getInt(String.format("%s.items.%s.durability", kit, s)));

                    ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName(kitData.getString(String.format("%s.items.%s.name", kit, s)));
                    meta.setLore(kitData.getStringList(String.format("%s.items.%s.name", kit, s)));

                    ConfigurationSection enchantSection = kitData.getConfigurationSection(String.format("%s.items.%s.enchantments", kit, s));

                    if (enchantSection != null) {

                        for (String enchant : enchantSection.getKeys(false)) {

                            item.addUnsafeEnchantment(Enchantment.getByName(enchant), kitData.getInt(String.format("%s.items.%s.enchantments.%s", kit, s, enchant)));

                        }

                    }

                    if (item.getType().equals(Material.POTION) || item.getType().equals(Material.SPLASH_POTION) || item.getType().equals(Material.LINGERING_POTION)) {

                        if (kitData.getConfigurationSection(String.format("%s.items.%s.potion", kit, s)) != null) {

                            ((PotionMeta) meta).setBasePotionData(new PotionData(PotionType.valueOf(kitData.getString(String.format("%s.items.%s.potion.effect", kit, s))), kitData.getBoolean(String.format("%s.items.%s.potion.extended", kit, s)), kitData.getBoolean(String.format("%s.items.%s.potion.upgraded", kit, s))));

                        }

                    }

                    item.setItemMeta(meta);

                    items[position] = item;

                } catch (NumberFormatException e) {

                    System.err.println(String.format("Invalid kit item position in kit %s: %s", kit, s));

                }

            }

            kits.put(kit, items);

            kitCooldowns.put(kit, kitData.getLong(String.format("%s.cooldown", kit), 0L));

            if (Bukkit.getPluginManager().getPermission(String.format("kxessentials.kit.%s", kit)) != null) {

                Permission perm = new Permission(String.format("kxessentials.kit.%s", kit), PermissionDefault.OP);
                perm.addParent(star, true);

                Bukkit.getPluginManager().addPermission(perm);

            }

        }

    }

    private void save() {

        try {

            kitData.save(kitFile);

        } catch (IOException e) {

            System.err.println("Could not save the kits.yml file.");
            e.printStackTrace();

        }

    }

    @SuppressWarnings("deprecation")
    public boolean createKit(String name, ItemStack[] contents, long cooldown) {

        if (kitData.getConfigurationSection(name) == null) {

            kitData.set(String.format("%s.cooldown", name), cooldown);

            ConfigurationSection items = kitData.createSection(String.format("%s.items", name));

            for (int i = 0; i < contents.length; i++) {

                if (contents[i] != null) {

                    items.set(String.format("%d.material", i), contents[i].getType().toString());
                    items.set(String.format("%d.amount", i), contents[i].getAmount());
                    items.set(String.format("%d.durability", i), contents[i].getDurability());
                    items.set(String.format("%d.name", i), contents[i].getItemMeta().getDisplayName());
                    items.set(String.format("%d.lore", i), contents[i].getItemMeta().getLore());

                    Map<Enchantment, Integer> enchants = contents[i].getEnchantments();

                    for (Enchantment e : enchants.keySet()) {

                        items.set(String.format("%d.enchantments.%s", i, e.getName()), enchants.get(e));

                    }

                    if (contents[i].getType().equals(Material.POTION) || contents[i].getType().equals(Material.SPLASH_POTION) || contents[i].getType().equals(Material.LINGERING_POTION)) {

                        PotionMeta potion = (PotionMeta) contents[i].getItemMeta();

                        items.set(String.format("%d.potion.effect", i), potion.getBasePotionData().getType().toString());
                        items.set(String.format("%d.potion.extended", i), potion.getBasePotionData().isExtended());
                        items.set(String.format("%d.potion.upgraded", i), potion.getBasePotionData().isUpgraded());

                    }

                }

            }

            kits.put(name, contents);

            kitCooldowns.put(name, cooldown);

            save();

            return true;

        }

        return false;

    }

    public boolean deleteKit(String name) {

        if (kitData.getConfigurationSection(name) != null) {

            kitData.set(name, null);

            kits.remove(name);

            kitCooldowns.remove(name);

            save();

            return true;

        }

        return false;

    }

    public boolean kitExists(String name) {

        return kits.containsKey(name) || kitData.getConfigurationSection(name) != null;

    }

    public ItemStack[] getKit(String name) {

        return kits.get(name);

    }

    public long getKitCooldown(String name) {

        return kitCooldowns.get(name);

    }

    public void clearKitCache() {

        kits.clear();
        kitCooldowns.clear();

    }

    public void addViewing(Player p) {

        viewing.add(p);

    }

    public void removeViewing(Player p) {

        viewing.remove(p);

    }

    public boolean isViewing(Player p) {

        return viewing.contains(p);

    }

    public void clearViewing() {

        viewing.clear();

    }

    public String generateCooldownString(long cooldown) {

        System.out.println(cooldown);

        long days = TimeUnit.MILLISECONDS.toDays(cooldown);
        long hours = TimeUnit.MILLISECONDS.toHours(cooldown) - (days * 24);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(cooldown) - (TimeUnit.MILLISECONDS.toHours(cooldown) * 60);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(cooldown) - (TimeUnit.MILLISECONDS.toMinutes(cooldown) * 60);

        StringBuilder message = new StringBuilder();

        message.append(textManager.getErrorColor()).append("You may use this kit again in ");

        if (days > 0) {

            message.append(textManager.getMainColor()).append(days).append(" days").append(textManager.getErrorColor()).append(", ");

        }

        if (hours > 0) {

            message.append(textManager.getMainColor()).append(hours).append(" hours").append(textManager.getErrorColor()).append(", ");

        }

        if (minutes > 0) {

            message.append(textManager.getMainColor()).append(minutes).append(" minutes").append(textManager.getErrorColor()).append(", ");

        }

        if (seconds > 0) {

            message.append(textManager.getMainColor()).append(seconds).append(" seconds").append(textManager.getErrorColor()).append(", ");

        }

        if (message.length() > 0) {

            return message.substring(0, message.length() - 4);

        } else {

            return textManager.getMainColor() + "0 seconds";

        }

    }

}
