package com.kaixeleron.essentials.manager;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class NicknameManager {

    private final Map<Player, String> nicknames;

    private String prefix;

    public NicknameManager(String prefix) {

        nicknames = new HashMap<>();

        this.prefix = prefix;

    }

    public void setNickname(Player p, String nickname) {

        nicknames.put(p, nickname);

        if (nickname != null && nickname.length() > 0) p.setDisplayName(prefix + nickname);

    }

    public String getNickname(Player p) {

        return nicknames.get(p);

    }

    public String getRealName(String nickname) {

        for (Player p : nicknames.keySet()) {

            if (nicknames.get(p).equalsIgnoreCase(nickname)) {

                return p.getName();

            }

        }

        return null;

    }

    public void removeNickname(Player p) {

        nicknames.remove(p);

        p.setDisplayName(p.getName());

    }

    public void clearNicknames() {

        nicknames.clear();

    }

    public String getPrefix() {

        return prefix;

    }

    public void setPrefix(String prefix) {

        this.prefix = prefix;

    }

}
