package com.kaixeleron.essentials.manager;

import com.kaixeleron.essentials.EssentialsMain;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class TeleportManager {

    private final EssentialsMain m;

    private final Set<Player> teleporting, acceptAll, disabled;

    private final Map<Player, BukkitRunnable> timers;
    private final Map<Player, Player> teleportRequests, teleportHereRequests;

    private int waitTime;

    private ConfigurationSection spawnSection;

    private Location spawn = null;

    private final BackManager backManager;

    private final TextManager textManager;

    public TeleportManager(EssentialsMain m, int waitTime, BackManager backManager, TextManager textManager) {

        this.m = m;

        teleporting = new HashSet<>();
        acceptAll = new HashSet<>();
        disabled = new HashSet<>();

        timers = new HashMap<>();
        teleportRequests = new HashMap<>();
        teleportHereRequests = new HashMap<>();

        this.waitTime = waitTime;

        this.backManager = backManager;
        this.textManager = textManager;

        this.spawnSection = m.getConfig().getConfigurationSection("spawn");

        if (spawnSection != null && spawnSection.getString("world") != null) {

            spawn = new Location(Bukkit.getWorld(spawnSection.getString("world")), spawnSection.getDouble("x"), spawnSection.getDouble("y"), spawnSection.getDouble("z"), (float) spawnSection.getDouble("yaw"), (float) spawnSection.getDouble("pitch"));

        }

    }

    public void teleport(final Player p, final Location l) {

        if (waitTime == 0 || p.hasPermission("kxessentials.teleport.instant")) {

            backManager.setBackLocation(p, p.getLocation());

            p.teleport(l);

        } else {

            teleporting.add(p);

            p.sendMessage(textManager.getMainColor() + "Teleporting in " + textManager.getAltColor() + waitTime + textManager.getMainColor() + " seconds. Don't move.");

            BukkitRunnable timer = new BukkitRunnable() {

                @Override
                public void run() {

                    backManager.setBackLocation(p, p.getLocation());

                    p.teleport(l);
                    p.sendMessage(textManager.getMainColor() + "Teleporting...");

                    teleporting.remove(p);
                    timers.remove(p);

                }

            };

            timer.runTaskLater(m, waitTime * 20L);

            timers.put(p, timer);

        }

    }

    public boolean isTeleporting(Player p) {

        return teleporting.contains(p);

    }

    public void cancelTeleport(Player p) {

        if (teleporting.contains(p)) {

            p.sendMessage(textManager.getMainColor() + "Pending teleport cancelled.");

            teleporting.remove(p);
            timers.get(p).cancel();
            timers.remove(p);

        }

    }

    public void cancelAllTeleports() {

        for (Player p : teleporting) {

            p.sendMessage("Pending teleport cancelled.");

            timers.get(p).cancel();

        }

        teleporting.clear();
        timers.clear();

    }

    public void addTeleportRequest(Player sender, Player recipient) {

        teleportRequests.put(recipient, sender);
        teleportHereRequests.remove(recipient);

    }

    public void removeTeleportRequest(Player p) {

        teleportRequests.remove(p);

    }

    public Player getTeleportRequest(Player p) {

        return teleportRequests.get(p);

    }

    public void addTeleportHereRequest(Player sender, Player recipient) {

        teleportHereRequests.put(recipient, sender);
        teleportRequests.remove(recipient);

    }

    public void removeTeleportHereRequest(Player p) {

        teleportHereRequests.remove(p);

    }

    public Player getTeleportHereRequest(Player p) {

        return teleportHereRequests.get(p);

    }

    public void removeAllRequests(Player player) {

        for (Iterator<Player> iterator = teleportRequests.keySet().iterator(); iterator.hasNext();) {

            Player p = iterator.next();

            if (p.equals(player)) {

                iterator.remove();

            } else if (teleportRequests.get(p).equals(player)) {

                iterator.remove();

            }

        }

        for (Iterator<Player> iterator = teleportHereRequests.keySet().iterator(); iterator.hasNext();) {

            Player p = iterator.next();

            if (p.equals(player)) {

                iterator.remove();

            } else if (teleportHereRequests.get(p).equals(player)) {

                iterator.remove();

            }

        }

    }

    public void clearRequests() {

        teleportRequests.clear();
        teleportHereRequests.clear();

    }

    public void addAcceptingAll(Player p) {

        acceptAll.add(p);

    }

    public void removeAcceptingAll(Player p) {

        acceptAll.remove(p);

    }

    public boolean isAcceptingAll(Player p) {

        return acceptAll.contains(p);

    }

    public void clearAcceptingAll() {

        acceptAll.clear();

    }

    public void addDisabled(Player p) {

        disabled.add(p);

    }

    public void removeDisabled(Player p) {

        disabled.remove(p);

    }

    public boolean isDisabled(Player p) {

        return disabled.contains(p);

    }

    public void clearDisabled() {

        disabled.clear();

    }

    public void setSpawn(Location spawn) {

        this.spawn = spawn;

        if (spawnSection == null) {

            spawnSection = m.getConfig().createSection("spawn");

            spawnSection.set("world", spawn.getWorld().getName());
            spawnSection.set("x", spawn.getX());
            spawnSection.set("y", spawn.getY());
            spawnSection.set("z", spawn.getZ());
            spawnSection.set("yaw", spawn.getYaw());
            spawnSection.set("pitch", spawn.getPitch());

            m.saveConfig();

        }

    }

    public Location getSpawn() {

        return spawn;

    }

}
