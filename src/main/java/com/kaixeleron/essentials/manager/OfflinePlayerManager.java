package com.kaixeleron.essentials.manager;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.authlib.GameProfile;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class OfflinePlayerManager {

    private final Map<String, UUID> cache;

    private final Gson gson;

    public OfflinePlayerManager() {

        cache = Collections.synchronizedMap(new HashMap<>());

        StringBuilder serverCache = new StringBuilder();

        BufferedReader cacheReader = null;

        try {

            cacheReader = new BufferedReader(new FileReader(new File(new File(".").getAbsoluteFile(), "usercache.json")));

            String input = cacheReader.readLine();

            while (input != null) {

                serverCache.append(input);
                input = cacheReader.readLine();

            }

        } catch (IOException e) {

            System.err.println("Could not read server usercache.json file.");
            e.printStackTrace();

        } finally {

            try {

                if (cacheReader != null) cacheReader.close();

            } catch (Exception ignored) {}

        }

        gson = new Gson();

        JsonArray array = gson.fromJson(serverCache.toString(), JsonArray.class);

        for (JsonElement e : array) {

            JsonObject entry = e.getAsJsonObject();

            synchronized (cache) {

                cache.put(entry.get("name").getAsString(), UUID.fromString(entry.get("uuid").getAsString()));

            }

        }

    }

    public GameProfile getOfflineProfile(String name) {

        UUID id;

        synchronized (cache) {

            id = cache.get(name);

        }

        if (id == null) {

            BufferedReader reader = null;

            try {

                HttpsURLConnection connection = (HttpsURLConnection) new URL(String.format("https://api.mojang.com/users/profiles/minecraft/%s", name)).openConnection();

                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                StringBuilder builder = new StringBuilder();

                String data = reader.readLine();

                while (data != null) {

                    builder.append(data);
                    data = reader.readLine();

                }

                JsonObject profile = gson.fromJson(builder.toString(), JsonObject.class);

                JsonElement idElement = profile.get("id");

                if (idElement != null) {

                    StringBuilder uuid = new StringBuilder(idElement.getAsString());

                    uuid.insert(8, "-").insert(13, "-").insert(18, "-").insert(23, "-");

                    String profileName = profile.get("name").getAsString();

                    UUID profileUUID = UUID.fromString(uuid.toString());

                    synchronized (cache) {

                        cache.put(profileName, profileUUID);

                    }

                    return new GameProfile(profileUUID, profileName);

                }

            } catch (IOException e) {

                System.err.println("Could not fetch player UUID from Mojang.");
                e.printStackTrace();
                return null;

            } finally {

                if (reader != null) try {

                    reader.close();

                } catch (Exception ignored) {}

            }

        } else {

            for (String s : cache.keySet()) {

                if (s.equalsIgnoreCase(name)) {

                    return new GameProfile(id, s);

                }

            }

            return new GameProfile(id, name);

        }

        return null;

    }

    public void addCache(String name, UUID id) {

        synchronized (cache) {

            cache.put(name, id);

        }

    }

    public void clearCache() {

        synchronized (cache) {

            cache.clear();

        }

    }

}
