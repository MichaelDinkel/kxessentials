package com.kaixeleron.essentials.manager;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PriceManager {

    private final Map<Material, Double> prices;

    private final File priceFile;
    private FileConfiguration priceData;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public PriceManager(File dataFolder) {

        prices = new HashMap<>();

        priceFile = new File(dataFolder, "prices.yml");

        if (!priceFile.exists()) {

            try {

                priceFile.createNewFile();

            } catch (IOException e) {

                System.err.println("Could not create a prices.yml file.");
                e.printStackTrace();

            }

        }

    }

    public void reload() {

        priceData = YamlConfiguration.loadConfiguration(priceFile);

        prices.clear();

        for (String s : priceData.getKeys(false)) {

            try {

                prices.put(Material.valueOf(s), priceData.getDouble(s));

            } catch (IllegalArgumentException e) {

                System.err.println(String.format("Invalid material type: %s", s));

            }

        }

    }

    private void save() {

        try {

            priceData.save(priceFile);

        } catch (IOException e) {

            System.err.println("Could not save the prices.yml file.");
            e.printStackTrace();

        }

    }

    public void setPrice(Material m, double price) {

        prices.put(m, price);

        priceData.set(m.toString(), price);

        save();

    }

    public Double getPrice(Material m) {

        return prices.get(m);

    }

    public void removePrice(Material m) {

        prices.remove(m);

        priceData.set(m.toString(), null);

        save();

    }

    public void clearCache() {

        prices.clear();

    }

}
