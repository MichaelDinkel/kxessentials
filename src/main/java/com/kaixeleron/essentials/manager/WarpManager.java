package com.kaixeleron.essentials.manager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WarpManager {

    private final Map<String, Location> warps;

    private final File warpFile;
    private FileConfiguration warpData;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public WarpManager(File dataFolder) {

        warps = new HashMap<>();

        warpFile = new File(dataFolder, "warps.yml");

        if (!warpFile.exists()) {

            try {

                warpFile.createNewFile();

            } catch (IOException e) {

                System.err.println("Could not create a warps.yml file.");
                e.printStackTrace();

            }

        }

    }

    public void reload() {

        warpData = YamlConfiguration.loadConfiguration(warpFile);

        warps.clear();

        for (String s : warpData.getKeys(false)) {

            String world = warpData.getString(String.format("%s.world", s), null);
            double x = warpData.getDouble(String.format("%s.x", s), Double.MAX_VALUE);
            double y = warpData.getDouble(String.format("%s.y", s), Double.MAX_VALUE);
            double z = warpData.getDouble(String.format("%s.z", s), Double.MAX_VALUE);
            float yaw = (float) warpData.getDouble(String.format("%s.yaw", s), Float.MAX_VALUE);
            float pitch = (float) warpData.getDouble(String.format("%s.pitch", s), Float.MAX_VALUE);

            if (world == null || x == Double.MAX_VALUE || y == Double.MAX_VALUE || z == Double.MAX_VALUE || yaw == Float.MAX_VALUE || pitch == Float.MAX_VALUE) {

                System.err.println(String.format("Invalid kit: %s", s));

            } else {

                World w = Bukkit.getWorld(world);

                if (w == null) {

                    System.err.println(String.format("Kit %s refers to a nonexistent world.", s));

                } else {

                    warps.put(s, new Location(w, x, y, z, yaw, pitch));

                }

            }

        }

    }

    public void registerPerms() {

        Permission star = Bukkit.getPluginManager().getPermission("kxessentials.warp.*");

        for (String warp : listWarps()) {

            Permission permission = new Permission(String.format("kxessentials.warp.%s", warp), PermissionDefault.OP);
            permission.addParent(star, true);

            Bukkit.getPluginManager().addPermission(permission);

        }

    }

    private void save() {

        try {

            warpData.save(warpFile);

        } catch (IOException e) {

            System.err.println("Could not save the warps.yml file.");
            e.printStackTrace();

        }

    }

    public void addWarp(String name, Location warp) {

        if (!warps.containsKey(name)) {

            warps.put(name, warp);

            ConfigurationSection warpSection = warpData.createSection(name);

            warpSection.set("world", warp.getWorld().getName());
            warpSection.set("x", warp.getX());
            warpSection.set("y", warp.getY());
            warpSection.set("z", warp.getZ());
            warpSection.set("yaw", warp.getYaw());
            warpSection.set("pitch", warp.getPitch());

            save();

        }

    }

    public Location getWarp(String name) {

        return warps.get(name);

    }

    public void deleteWarp(String name) {

        warps.remove(name);

        warpData.set(name, null);

        save();

    }

    public Set<String> listWarps() {

        return warps.keySet();

    }

    public void clearCache() {

        warps.clear();

    }

}
