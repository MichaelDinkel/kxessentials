package com.kaixeleron.essentials.manager;

import com.kaixeleron.essentials.EssentialsMain;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BackManager {

    private final EssentialsMain main;

    private final List<Player> debounce;

    private final Map<Player, Location> backLocations;

    public BackManager(EssentialsMain main) {

        this.main = main;

        debounce = new ArrayList<>();
        backLocations = new HashMap<>();

    }

    public void setBackLocation(final Player p, Location l) {

        if (!debounce.contains(p)) {

            backLocations.put(p, l);

            debounce.add(p);

            new BukkitRunnable() {

                @Override
                public void run() {

                    debounce.remove(p);

                }

            }.runTaskLater(main, 1L);

        }

    }

    public Location getBackLocation(Player p) {

        return backLocations.get(p);

    }

    public void removeBackLocation(Player p) {

        backLocations.remove(p);

    }

    public void clearBackLocations() {

        backLocations.clear();

    }

}
