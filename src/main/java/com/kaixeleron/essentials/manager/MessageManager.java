package com.kaixeleron.essentials.manager;

import org.bukkit.entity.Player;

import java.util.*;

public class MessageManager {

    private final TextManager textManager;

    private PlayerDataManager playerDataManager;

    private final Map<Player, Player> replies;
    private final Map<Player, Set<Player>> ignored;

    private final Set<Player> disabled, socialspy;

    public MessageManager(TextManager textManager) {

        this.textManager = textManager;

        replies = new HashMap<>();
        ignored = new HashMap<>();

        disabled = new HashSet<>();
        socialspy = new HashSet<>();

    }

    public void setReply(Player sender, Player recipient) {

        replies.put(sender, recipient);
        replies.put(recipient, sender);

    }

    public void removeReply(Player p) {

        replies.remove(p);

        Player other = null;

        for (Player recipient : replies.keySet()) {

            if (replies.get(recipient).equals(p)) {

                other = recipient;

                break;

            }

        }

        if (other != null) replies.remove(other);

    }

    public Player getReply(Player p) {

        return replies.get(p);

    }

    public void addIgnored(Player p, Player ignored) {

        Set<Player> list = this.ignored.get(p);

        if (list == null) list = new HashSet<>();

        if (!list.contains(ignored)) list.add(ignored);

        this.ignored.put(p, list);

    }

    public void removeIgnored(Player p, Player ignored) {

        Set<Player> list = this.ignored.get(p);

        if (list != null) {

            list.remove(ignored);

            if (list.isEmpty()) {

                this.ignored.remove(p);

            } else {

                this.ignored.put(p, list);

            }

        }

    }

    public void clearIgnored(Player p) {

        ignored.remove(p);

    }

    public boolean isIgnored(Player p, Player ignored) {

        Set<Player> list = this.ignored.get(p);

        return list != null && list.contains(ignored);

    }

    public void clearAllIgnored() {

        ignored.clear();

    }

    public void setDisabled(Player p, boolean disabled) {

        if (disabled) {

            if (!this.disabled.contains(p)) {

                this.disabled.add(p);

            }

        } else {

            this.disabled.remove(p);

        }

    }

    public boolean isDisabled(Player p) {

        return disabled.contains(p);

    }

    public void clearDisabled() {

        disabled.clear();

    }

    public void addSocialSpy(Player p) {

        socialspy.add(p);

    }

    public void removeSocialSpy(Player p) {

        socialspy.remove(p);

    }

    public boolean isSpying(Player p) {

        return socialspy.contains(p);

    }

    public void clearSocialSpy() {

        socialspy.clear();

    }

    public void dispatchSocialSpyMessage(Player sender, Player recipient, String message) {

        for (Iterator<Player> iterator = socialspy.iterator(); iterator.hasNext();) {

            Player p = iterator.next();

            if (!p.hasPermission("kxessentials.command.socialspy")) {

                iterator.remove();
                playerDataManager.setSocialSpy(p, false);

            } else {

                if (!p.equals(sender) && !p.equals(recipient)) {

                    p.sendMessage(textManager.getSocialSpyColor() + String.format("[%s -> %s] %s", sender.getName(), recipient.getName(), message));

                }

            }

        }

    }

    public void setPlayerDataManager(PlayerDataManager playerDataManager) {

        this.playerDataManager = playerDataManager;

    }

}
