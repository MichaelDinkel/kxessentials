package com.kaixeleron.essentials.manager;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.database.DatabaseException;
import com.kaixeleron.essentials.database.EssentialsDatabase;
import com.kaixeleron.essentials.database.SQLDatabase;
import com.kaixeleron.essentials.database.YAMLDatabase;
import com.kaixeleron.essentials.database.converter.DatabaseConverter;
import com.kaixeleron.essentials.database.converter.EssentialsXConverter;
import com.kaixeleron.essentials.database.data.BalTopEntry;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class PlayerDataManager {

    private final EssentialsMain m;

    private EssentialsDatabase database;

    private final MessageManager messageManager;

    private final NicknameManager nicknameManager;

    private final TeleportManager teleportManager;

    private final Table<Player, Material, String> powertools;

    private final Table<Player, String, Location> homes;

    private final Set<Player> payDisabled;

    private double defaultBalance;

    private final String hostname, db, username, password;

    private final int port;

    public PlayerDataManager(EssentialsMain m, MessageManager messageManager, NicknameManager nicknameManager, TeleportManager teleportManager, double defaultBalance, String hostname, int port, String db, String username, String password) {

        this.m = m;

        this.messageManager = messageManager;

        this.nicknameManager = nicknameManager;

        this.teleportManager = teleportManager;

        powertools = HashBasedTable.create();

        homes = HashBasedTable.create();

        payDisabled = new HashSet<>();

        this.defaultBalance = defaultBalance;

        this.hostname = hostname;
        this.port = port;
        this.db = db;
        this.username = username;
        this.password = password;

    }

    public void loadData(Player p) {

        try {

            database.saveDefault(p.getUniqueId());
            database.load(p.getUniqueId());

            if (database.getBalance(p.getUniqueId()) < 0) {

                database.setBalance(p.getUniqueId(), defaultBalance);

            }

            messageManager.setDisabled(p, database.isMessagingDisabled(p.getUniqueId()));

            if (database.isSocialSpyEnabled(p.getUniqueId())) {

                messageManager.addSocialSpy(p);

            }

            nicknameManager.setNickname(p, database.getNickname(p.getUniqueId()));

            if (database.areTeleportRequestsDisabled(p.getUniqueId())) {

                teleportManager.addDisabled(p);

            }

            if (database.isAcceptingAllTeleports(p.getUniqueId())) {

                teleportManager.addAcceptingAll(p);

            }

            if (database.isPayDisabled(p.getUniqueId())) {

                payDisabled.add(p);

            }

            Map<String, String> powertools = database.getPowertools(p.getUniqueId());

            if (powertools != null) for (String s : powertools.keySet()) {

                try {

                    this.powertools.put(p, Material.valueOf(s), powertools.get(s));

                } catch (IllegalArgumentException e) {

                    System.err.println(String.format("Invalid powertool material: %s", s));

                }

            }

        } catch (DatabaseException e) {

            e.printStackTrace();

        }

    }

    public double getBalance(OfflinePlayer p) {

        try {

            return database.getBalance(p.getUniqueId());

        } catch (DatabaseException e) {

            System.err.println("Could not get balance for player.");
            e.printStackTrace();
            return 0.0D;

        }

    }

    public void setBalance(OfflinePlayer p, double balance) {

        try {

            database.setBalance(p.getUniqueId(), balance);

        } catch (DatabaseException e) {

            System.err.println("Could not update player balance.");
            e.printStackTrace();

        }

    }

    public void resetBalance(OfflinePlayer p) {

        setBalance(p, defaultBalance);

    }

    public void addIgnored(Player p, OfflinePlayer ignored) {

        try {

            database.addIgnored(p.getUniqueId(), ignored.getUniqueId());

        } catch (DatabaseException e) {

            System.err.println("Could not add ignored player.");
            e.printStackTrace();

        }

    }

    public void removeIgnored(Player p, OfflinePlayer ignored) {

        try {

            database.removeIgnored(p.getUniqueId(), ignored.getUniqueId());

        } catch (DatabaseException e) {

            System.err.println("Could not remove ignored player.");
            e.printStackTrace();

        }

    }

    public boolean isIgnored(Player p, OfflinePlayer ignored) {

        try {

            return database.isIgnored(p.getUniqueId(), ignored.getUniqueId());

        } catch (DatabaseException e) {

            System.err.println("Could not get ignored status.");
            e.printStackTrace();
            return false;

        }

    }

    public void setMessagingDisabled(Player p, boolean disabled) {

        try {

            database.setMessagingDisabled(p.getUniqueId(), disabled);

        } catch (DatabaseException e) {

            System.err.println("Could not set messaging disabled status.");
            e.printStackTrace();

        }

    }

    public void setNickname(Player p, String nickname) {

        try {

            database.setNickname(p.getUniqueId(), nickname);

        } catch (DatabaseException e) {

            System.err.println("Could not set nickname.");
            e.printStackTrace();

        }

    }

    public void setPowertool(Player p, Material m, String command) {

        powertools.put(p, m, command);

        try {

            database.setPowertool(p.getUniqueId(), m.toString(), command);

        } catch (DatabaseException e) {

            System.err.println("Could not set powertool.");
            e.printStackTrace();

        }

    }

    public String getPowertool(Player p, Material m) {

        return powertools.get(p, m);

    }

    public void removePowertool(Player p, Material m) {

        powertools.remove(p, m);

        try {

            database.removePowertool(p.getUniqueId(), m.toString());

        } catch (DatabaseException e) {

            System.err.println("Could not remove powertool.");
            e.printStackTrace();

        }

    }

    public void setSocialSpy(Player p, boolean socialspy) {

        try {

            database.setSocialSpy(p.getUniqueId(), socialspy);

        } catch (DatabaseException e) {

            System.err.println("Could not set social spy status.");
            e.printStackTrace();

        }

    }

    public void setAcceptingAllTeleports(Player p, boolean enabled) {

        try {

            database.setAcceptingAllTeleports(p.getUniqueId(), enabled);

        } catch (DatabaseException e) {

            System.err.println("Could not set accepting all teleports status.");
            e.printStackTrace();

        }

    }

    public void setTeleportRequestsDisabled(Player p, boolean enabled) {

        try {

            database.setTeleportRequestsDisabled(p.getUniqueId(), enabled);

        } catch (DatabaseException e) {

            System.err.println("Could not set teleportation disabled status.");
            e.printStackTrace();

        }

    }

    public void setHome(OfflinePlayer p, String name, Location loc) {

        if (p.isOnline()) {

            homes.put(p.getPlayer(), name, loc);

        }

        try {

            database.setHome(p.getUniqueId(), name, loc);

        } catch (DatabaseException e) {

            System.err.println("Could not add home to player.");
            e.printStackTrace();

        }

    }

    public void removeHome(OfflinePlayer p, String name) {

        if (p.isOnline()) {

            homes.remove(p.getPlayer(), name);

        }

        try {

            database.removeHome(p.getUniqueId(), name);

        } catch (DatabaseException e) {

            System.err.println("Could not add home from player.");
            e.printStackTrace();

        }

    }

    public Location getHome(OfflinePlayer p, String name) {

        if (p.isOnline() && homes.contains(p.getPlayer(), name)) {

            return homes.get(p.getPlayer(), name);

        } else {

            try {

                Location home = database.getHome(p.getUniqueId(), name);

                if (home != null && p.isOnline()) {

                    homes.put(p.getPlayer(), name, home);

                }

                return home;

            } catch (DatabaseException e) {

                System.err.println("Could not get home data from database.");
                e.printStackTrace();

                return null;

            }

        }

    }

    public Set<String> listHomes(OfflinePlayer p) {

        /*if (p.isOnline() && homes.containsRow(p.getPlayer())) {

            return homes.row(p.getPlayer()).keySet();

        } else {*/

            try {

                return database.listHomes(p.getUniqueId());

            } catch (DatabaseException e) {

                System.err.println("Could not get home data from database.");
                e.printStackTrace();

                return null;

            }

        //}

    }

    public void setPayDisabled(Player p, boolean disabled) {

        if (disabled) {

            if (!payDisabled.contains(p)) {

                payDisabled.add(p);

            }

        } else {

            payDisabled.remove(p);

        }

        try {

            database.setPayDisabled(p.getUniqueId(), disabled);

        } catch (DatabaseException e) {

            System.err.println("Could not toggle pay disabled status.");
            e.printStackTrace();

        }

    }

    public void setKitCooldown(Player p, String kit, long cooldown) {

        try {

            database.setKitCooldown(p.getUniqueId(), kit, cooldown);

        } catch (DatabaseException e) {

            System.err.println("Could not set kit cooldown on player.");
            e.printStackTrace();

        }

    }

    public long getKitCooldown(Player p, String kit) {

        try {

            return database.getKitCooldown(p.getUniqueId(), kit);

        } catch (DatabaseException e) {

            System.err.println("Could not get kit cooldown on player.");
            e.printStackTrace();
            return Long.MAX_VALUE;

        }

    }

    public boolean isPayDisabled(Player p) {

        return payDisabled.contains(p);

    }

    public boolean isBalTopReady() {

        return database.isBalTopLoaded();

    }

    public List<BalTopEntry> getBalTop(int page) {

        try {

            return database.getBalTop(page);

        } catch (DatabaseException e) {

            System.err.println("Could not get top balance data.");
            e.printStackTrace();
            return new ArrayList<>();

        }

    }

    public int getBalTopPages() {

        try {

            return database.getBalTopPages();

        } catch (DatabaseException e) {

            System.err.println("Could not get top balance page count.");
            e.printStackTrace();
            return 0;

        }

    }

    public double getBalanceTotal() {

        try {

            return database.getBalanceTotal();

        } catch (DatabaseException e) {

            System.err.println("Could not get balance total.");
            e.printStackTrace();
            return 0.0D;

        }

    }

    public void clearCachedData(Player p) {

        payDisabled.remove(p);
        powertools.rowKeySet().remove(p);
        homes.rowKeySet().remove(p);

    }

    public void clearCache() {

        payDisabled.clear();
        powertools.clear();
        homes.clear();

    }

    public void setDatabase(EssentialsDatabase database) {

        this.database = database;

    }

    public boolean exportToDatabase(String type) {

        switch (type.toLowerCase()) {

            case "yaml":

                if (database instanceof YAMLDatabase) {

                    return false;

                } else {

                    new BukkitRunnable() {

                        @Override
                        public void run() {

                            try {

                                DatabaseConverter converter = database.getConverter();

                                converter.loadRecords();

                                new YAMLDatabase(m, PlayerDataManager.this, m.getDataFolder()).getConverter().importRecords(converter.getRecords(), false);

                            } catch (DatabaseException e) {

                                System.err.println("A database error occurred while exporting player data.");
                                e.printStackTrace();

                            }

                        }

                    }.runTaskAsynchronously(m);

                }

                return true;

            case "sql":

                if (database instanceof SQLDatabase) {

                    return false;

                } else {

                    new BukkitRunnable() {

                        @Override
                        public void run() {

                            try {

                                DatabaseConverter converter = database.getConverter();

                                converter.loadRecords();

                                new SQLDatabase(m, PlayerDataManager.this, hostname, port, db, username, password).getConverter().importRecords(converter.getRecords(), false);

                            } catch (DatabaseException e) {

                                System.err.println("A database error occurred while exporting player data.");
                                e.printStackTrace();

                            }

                        }

                    }.runTaskAsynchronously(m);

                }

                return true;

            case "essentialsx":

                new BukkitRunnable() {

                    @Override
                    public void run() {

                        try {

                            DatabaseConverter converter = database.getConverter();

                            converter.loadRecords();

                            new EssentialsXConverter(m).importRecords(converter.getRecords(), false);

                        } catch (DatabaseException e) {

                            System.err.println("A database error occurred while exporting player data.");
                            e.printStackTrace();

                        }

                    }

                }.runTaskAsynchronously(m);

                return true;

            default:

                try {

                    final Class<?> targetDbClass = Class.forName(type);

                    if (EssentialsDatabase.class.isAssignableFrom(targetDbClass) && targetDbClass.isInstance(database)) {

                        return false;

                    } else {

                        new BukkitRunnable() {

                            @Override
                            public void run() {

                                try {

                                    DatabaseConverter converter = database.getConverter();

                                    converter.loadRecords();

                                    ((EssentialsDatabase) targetDbClass.getMethod("getCustomEssentialsDatabase").invoke(null)).getConverter().importRecords(converter.getRecords(), false);

                                } catch (DatabaseException e) {

                                    System.err.println("A database error occurred while exporting player data.");
                                    e.printStackTrace();

                                } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {

                                    System.err.println("Unable to load custom database.");

                                }

                            }

                        }.runTaskAsynchronously(m);

                    }

                } catch (ClassNotFoundException e) {

                    System.err.println("Unable to load custom database.");

                }

                break;

        }

        return false;

    }

    public boolean importDatabase(String type) {

        switch (type.toLowerCase()) {

            case "yaml":

                if (database instanceof YAMLDatabase) {

                    return false;

                } else {

                    new BukkitRunnable() {

                        @Override
                        public void run() {

                            try {

                                DatabaseConverter converter = new YAMLDatabase(m, PlayerDataManager.this, m.getDataFolder()).getConverter();

                                converter.loadRecords();

                                database.getConverter().importRecords(converter.getRecords(), true);

                            } catch (DatabaseException e) {

                                System.err.println("A database error occurred while importing player data.");
                                e.printStackTrace();

                            }

                        }

                    }.runTaskAsynchronously(m);

                }

                return true;

            case "sql":

                if (database instanceof SQLDatabase) {

                    return false;

                } else {

                    new BukkitRunnable() {

                        @Override
                        public void run() {

                            try {

                                DatabaseConverter converter = new SQLDatabase(m, PlayerDataManager.this, hostname, port, db, username, password).getConverter();

                                converter.loadRecords();

                                database.getConverter().importRecords(converter.getRecords(), true);

                            } catch (DatabaseException e) {

                                System.err.println("A database error occurred while importing player data.");
                                e.printStackTrace();

                            }

                        }

                    }.runTaskAsynchronously(m);

                }

                return true;

            case "essentialsx":

                new BukkitRunnable() {

                    @Override
                    public void run() {

                        try {

                            DatabaseConverter converter = new EssentialsXConverter(m);

                            converter.loadRecords();

                            database.getConverter().importRecords(converter.getRecords(), true);

                        } catch (DatabaseException e) {

                            System.err.println("A database error occurred while importing player data.");
                            e.printStackTrace();

                        }

                    }

                }.runTaskAsynchronously(m);

                return true;

            default:

                try {

                    final Class<?> targetDbClass = Class.forName(type);

                    if (EssentialsDatabase.class.isAssignableFrom(targetDbClass) && targetDbClass.isInstance(database)) {

                        return false;

                    } else {

                        new BukkitRunnable() {

                            @Override
                            public void run() {

                                try {

                                    DatabaseConverter converter = ((EssentialsDatabase) targetDbClass.getMethod("getCustomEssentialsDatabase").invoke(null)).getConverter();

                                    converter.loadRecords();

                                    database.getConverter().importRecords(converter.getRecords(), true);

                                } catch (DatabaseException e) {

                                    System.err.println("A database error occurred while importing player data.");
                                    e.printStackTrace();

                                } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {

                                    System.err.println("Unable to load custom database.");

                                }

                            }

                        }.runTaskAsynchronously(m);

                    }

                } catch (ClassNotFoundException e) {

                    System.err.println("Unable to load custom database.");

                }

                break;

        }

        return false;

    }

    public void setLoginRespawn(UUID id, boolean respawn) {

        try {

            database.setLoginRespawn(id, respawn);

        } catch (DatabaseException e) {

            System.err.println(String.format("Unable to set login respawn status for %s.", id.toString()));
            e.printStackTrace();

        }

    }

    public boolean isRespawnOnLoginSet(UUID id) {

        try {

            return database.isRespawnOnLoginSet(id);

        } catch (DatabaseException e) {

            System.err.println(String.format("Unable to get login respawn status for %s.", id.toString()));
            e.printStackTrace();
            return false;

        }

    }

}
