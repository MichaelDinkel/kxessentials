package com.kaixeleron.essentials.manager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class AfkManager {

    private final Set<Player> afk;

    private final TextManager manager;

    public AfkManager(TextManager manager) {

        afk = new HashSet<>();

        this.manager = manager;

    }

    public void setAfk(Player p, boolean afk) {

        if (afk) {

            if (!this.afk.contains(p)) this.afk.add(p);
            Bukkit.broadcastMessage(manager.getAltColor() + p.getName() + manager.getMainColor() + " is now AFK.");

        } else {

            if (this.afk.contains(p)) this.afk.remove(p);
            Bukkit.broadcastMessage(manager.getAltColor() + p.getName() + manager.getMainColor() + " is no longer AFK.");

        }

    }

    public boolean isAfk(Player p) {

        return afk.contains(p);

    }

    public void clearAfk() {

        afk.clear();

    }

}
