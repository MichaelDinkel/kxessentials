package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class CommandClearInventory implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final Set<Player> clearing;

    private final Map<CommandSender, UUID> clearingOther;

    public CommandClearInventory(EssentialsMain m, TextManager textManager) {

        this.m = m;

        this.textManager = textManager;

        clearing = new HashSet<>();

        clearingOther = new HashMap<>();

    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.clearinventory.other")) {

            if (sender instanceof Player) {

                Player p = (Player) sender;

                if (clearing.contains(p)) {

                    p.getInventory().clear();
                    sender.sendMessage(textManager.getMainColor() + "Inventory cleared.");

                    clearing.remove(p);

                } else {

                    clearing.add(p);

                    sender.sendMessage(textManager.getMainColor() + "Are you sure you want to clear your inventory? Type " + textManager.getAltColor() + "/" + label + textManager.getMainColor() + " again to confirm.");

                    new BukkitRunnable() {

                        @Override
                        public void run() {

                            clearing.remove(p);

                        }

                    }.runTaskLater(m, 200L);

                }

            } else {

                if (sender.hasPermission("kxessentials.command.clearinventory.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                if (clearingOther.containsKey(sender) && clearingOther.get(sender).equals(p.getUniqueId())) {

                    p.getInventory().clear();
                    sender.sendMessage(textManager.getMainColor() + "Inventory of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " cleared.");

                    clearingOther.remove(sender);

                } else {

                    clearingOther.put(sender, p.getUniqueId());

                    sender.sendMessage(textManager.getMainColor() + "Are you sure you want to clear the inventory of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + "? Type " + textManager.getAltColor() + "/" + label + " " + p.getName() + textManager.getMainColor() + " again to confirm.");

                    new BukkitRunnable() {

                        @Override
                        public void run() {

                            clearingOther.remove(sender);

                        }

                    }.runTaskLater(m, 200L);

                }

            }

        }

        return true;

    }

}
