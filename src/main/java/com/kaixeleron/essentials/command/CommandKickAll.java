package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandKickAll implements CommandExecutor {

    private final TextManager textManager;

    public CommandKickAll(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        String reason = "Kicked by an operator.";

        if (args.length > 0) {

            StringBuilder builder = new StringBuilder();

            for (String s : args) {

                builder.append(s).append(" ");

            }

            reason = builder.substring(0, builder.length() - 1);

        }

        Player kicker = null;

        if (sender instanceof Player) kicker = (Player) sender;

        for (Player p : Bukkit.getOnlinePlayers()) {

            if (!p.equals(kicker) && !p.hasPermission("kxessentials.command.kickall.immune")) p.kickPlayer(reason);

        }

        sender.sendMessage(textManager.getMainColor() + "Kicked all online players" + (args.length > 0 ? " for reason: " + textManager.getAltColor() + reason + textManager.getMainColor() + ".": "."));

        return true;

    }

}
