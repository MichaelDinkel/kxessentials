package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.KitManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class CommandShowKit implements CommandExecutor {

    private final TextManager textManager;

    private final KitManager kitManager;

    public CommandShowKit(TextManager textManager, KitManager kitManager) {

        this.textManager = textManager;

        this.kitManager = kitManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                if (kitManager.kitExists(args[0])) {

                    kitManager.addViewing((Player) sender);

                    Inventory inv = Bukkit.createInventory(null, 45, String.format("Kit %s", args[0]));

                    inv.addItem(kitManager.getKit(args[0]));

                    ((Player) sender).openInventory(inv);

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "Kit " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + " does not exist.");

                }

            }

        } else sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        return true;

    }

}
