package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpPos implements CommandExecutor {

    private final TeleportManager teleportManager;

    public CommandTpPos(TeleportManager teleportManager) {

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length < 3) {

                sender.sendMessage(cmd.getDescription());
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + cmd.getUsage());

            } else {

                boolean success = false;

                double x = 0.0D, y = 0.0D, z = 0.0D;
                float yaw = 0.0F, pitch = 0.0F;

                Location location = ((Player) sender).getLocation();

                try {

                    x = Double.parseDouble(args[0]);
                    y = Double.parseDouble(args[1]);
                    z = Double.parseDouble(args[2]);

                    if (args.length >= 4) {

                        yaw = Float.parseFloat(args[3]);

                    } else {

                        yaw = location.getYaw();

                    }

                    if (args.length >= 5) {

                        pitch = Float.parseFloat(args[4]);

                    } else {

                        pitch = location.getPitch();

                    }

                    success = true;

                } catch (NumberFormatException e) {

                    sender.sendMessage(ChatColor.RED + "Invalid position. Must be valid numbers.");

                }

                if (success) {

                    teleportManager.teleport((Player) sender, new Location(((Player) sender).getWorld(), x, y, z, yaw, pitch));

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

}
