package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class CommandTree implements CommandExecutor {

    private final TextManager textManager;

    private int maxDist;

    public CommandTree(TextManager textManager, int maxDist) {

        this.textManager = textManager;
        this.maxDist = maxDist;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                TreeType type = null;

                switch (args[0].toLowerCase()) {

                    case "oak":

                        if (args.length > 1 && args[1].equalsIgnoreCase("big")) {

                            type = TreeType.BIG_TREE;

                        } else {

                            type = TreeType.TREE;

                        }

                        break;

                    case "birch":

                        if (args.length > 1 && args[1].equalsIgnoreCase("big")) {

                            type = TreeType.TALL_BIRCH;

                        } else {

                            type = TreeType.BIRCH;

                        }

                        break;

                    case "spruce":

                        if (args.length > 1 && args[1].equalsIgnoreCase("big")) {

                            type = TreeType.TALL_REDWOOD;

                        } else {

                            type = TreeType.REDWOOD;

                        }

                        break;

                    case "redmushroom":

                        type = TreeType.RED_MUSHROOM;

                        break;

                    case "brownmushroom":

                        type = TreeType.BROWN_MUSHROOM;

                        break;

                    case "jungle":

                        if (args.length > 1 && args[1].equalsIgnoreCase("big")) {

                            type = TreeType.JUNGLE;

                        } else {

                            type = TreeType.SMALL_JUNGLE;

                        }

                        break;

                    case "junglebush":

                        type = TreeType.JUNGLE_BUSH;

                        break;

                    case "swamp":

                        type = TreeType.SWAMP;

                        break;

                    case "acacia":

                        type = TreeType.ACACIA;

                        break;

                    case "darkoak":

                        type = TreeType.DARK_OAK;

                        break;

                    case "chorus":

                        type = TreeType.CHORUS_PLANT;

                        break;

                }

                if (type == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid tree type.");

                } else {

                    Location eyeLoc = ((Player) sender).getEyeLocation().clone();

                    Vector direction = eyeLoc.getDirection();
                    direction.normalize();

                    for (int i = 0; i < maxDist; i++) {

                        eyeLoc.add(direction);

                        Block b = eyeLoc.getBlock();

                        if (!b.getType().equals(Material.AIR)) {

                            eyeLoc.add(0.0D, 1.0D, 0.0D);
                            break;

                        }

                    }

                    if (eyeLoc.getWorld().generateTree(eyeLoc, type)) {

                        sender.sendMessage(textManager.getMainColor() + "Tree spawned.");

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "Unable to spawn the tree at the target location.");

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
