package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

public class CommandEnchant implements CommandExecutor {

    private final TextManager textManager;

    public CommandEnchant(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else if (args.length == 2 || !sender.hasPermission("kxessentials.command.enchant.other")) {

            if (sender instanceof Player) {

                @SuppressWarnings("deprecation") Enchantment e = Enchantment.getByKey(new NamespacedKey("minecraft", args[0].toLowerCase()));

                int level = Integer.MIN_VALUE;

                try {

                    level = Integer.parseInt(args[1]);

                } catch (NumberFormatException ignored) {}

                if (e == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid enchantment: " + textManager.getMainColor() + args[0]);

                } else if (level == Integer.MIN_VALUE) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid level " + textManager.getMainColor() + level + textManager.getErrorColor() + ". Must be am integer.");

                } else {

                    if (sender.hasPermission("kxessentials.command.enchant.unsafe")) {

                        ((Player) sender).getInventory().getItemInMainHand().addUnsafeEnchantment(e, level);

                        sender.sendMessage(textManager.getMainColor() + "Enchantment applied.");

                    } else {

                        try {

                            ((Player) sender).getInventory().getItemInMainHand().addEnchantment(e, level);

                            sender.sendMessage(textManager.getMainColor() + "Enchantment applied.");

                        } catch (IllegalArgumentException ignored) {

                            sender.sendMessage(textManager.getErrorColor() + "The specified enchantment or level cannot be applied to this item.");

                        }

                    }

                }

            } else {

                if (sender.hasPermission("kxessentials.command.enchant.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[2]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                @SuppressWarnings("deprecation") Enchantment e = Enchantment.getByKey(new NamespacedKey("minecraft", args[0]));

                int level = Integer.MIN_VALUE;

                try {

                    level = Integer.parseInt(args[1]);

                } catch (NumberFormatException ignored) {}

                if (e == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid enchantment: " + textManager.getMainColor() + args[0]);

                } else if (level == Integer.MIN_VALUE) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid level " + textManager.getMainColor() + level + textManager.getErrorColor() + ". Must be an integer.");

                } else {

                    if (sender.hasPermission("kxessentials.command.enchant.unsafe")) {

                        p.getInventory().getItemInMainHand().addUnsafeEnchantment(e, level);

                        sender.sendMessage(textManager.getMainColor() + "Enchantment applied to " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + "'s item.");

                    } else {

                        try {

                            p.getInventory().getItemInMainHand().addEnchantment(e, level);

                            sender.sendMessage(textManager.getMainColor() + "Enchantment applied to " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + "'s item.");

                        } catch (IllegalArgumentException ignored) {

                            sender.sendMessage(textManager.getErrorColor() + "The specified enchantment or level cannot be applied to this item.");

                        }

                    }

                }

            }

        }

        return true;

    }

}
