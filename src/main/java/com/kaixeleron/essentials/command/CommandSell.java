package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.PriceManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.math.BigDecimal;

public class CommandSell implements CommandExecutor {

    private final TextManager textManager;

    private final PriceManager priceManager;

    private final PlayerDataManager playerDataManager;

    public CommandSell(TextManager textManager, PriceManager priceManager, PlayerDataManager playerDataManager) {

        this.textManager = textManager;

        this.priceManager = priceManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length > 0 && args[0].equalsIgnoreCase("all") && sender.hasPermission("kxessentials.command.sell.all")) {

                double total = 0.0D;
                int numSold = 0;

                for (ItemStack i : ((Player) sender).getInventory().getContents()) {

                    if (i != null) {

                        Double price = priceManager.getPrice(i.getType());

                        if (price != null) {

                            int amount = i.getAmount();

                            total = total + price * amount;

                            numSold = numSold + amount;

                            i.setAmount(0);
                            i.setType(Material.AIR);

                        }

                    }

                }

                if (numSold == 0) {

                    sender.sendMessage(textManager.getErrorColor() + "You have no sellable items/");

                } else {

                    playerDataManager.setBalance((Player) sender, playerDataManager.getBalance((Player) sender) + total);

                    sender.sendMessage(textManager.getMainColor() + "Sold " + textManager.getAltColor() + numSold + textManager.getMainColor() + " items for " + textManager.getAltColor() + textManager.getCurrencySymbol() + total + textManager.getMainColor() + ".");

                }

            } else {

                ItemStack item = ((Player) sender).getInventory().getItemInMainHand();

                int amount = item.getAmount();

                Double price = priceManager.getPrice(item.getType());

                if (price == null) {

                    sender.sendMessage(textManager.getErrorColor() + "That item is not priced.");

                } else {

                    price = price * amount;

                    playerDataManager.setBalance((Player) sender, playerDataManager.getBalance((Player) sender) + price);

                    sender.sendMessage(textManager.getMainColor() + "Sold " + textManager.getAltColor() + amount + " " + item.getType().toString().toLowerCase().replace('_', ' ') + (amount == 1 ? "" : "s") + textManager.getMainColor() + " for " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(price).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                    item.setAmount(0);
                    item.setType(Material.AIR);

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
