package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class CommandLightning implements CommandExecutor {

    private final TextManager textManager;

    private int maxDist;

    public CommandLightning(TextManager textManager, int maxDist) {

        this.textManager = textManager;

        this.maxDist = maxDist;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.lightning.other")) {

            if (sender instanceof Player) {

                Location eyeLoc = ((Player) sender).getEyeLocation().clone();

                Vector direction = eyeLoc.getDirection();
                direction.normalize();

                for (int i = 0; i < maxDist; i++) {

                    eyeLoc.add(direction);

                    Block b = eyeLoc.getBlock();

                    if (!b.getType().equals(Material.AIR)) {

                        break;

                    }

                }

                //noinspection ConstantConditions
                eyeLoc.getWorld().strikeLightning(eyeLoc);

            } else {

                if (sender.hasPermission("kxessentials.command.lightning.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                p.getWorld().strikeLightning(p.getLocation());
                if (!(sender instanceof Player) || !p.equals((Player) sender)) sender.sendMessage(textManager.getMainColor() + "Smote " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

            }

        }

        return true;

    }

}
