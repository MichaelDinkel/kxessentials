package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class CommandJump implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    private int maxDist;

    public CommandJump(TextManager textManager, TeleportManager teleportManager, int maxDist) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

        this.maxDist = maxDist;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Location eyeLoc = ((Player) sender).getEyeLocation().clone();

            Vector direction = eyeLoc.getDirection();
            direction.normalize();

            Block target = null;

            for (int i = 0; i < maxDist; i++) {

                eyeLoc.add(direction);

                Block b = eyeLoc.getBlock();

                if (!b.getType().equals(Material.AIR)) {

                    target = b;
                    break;

                }

            }

            if (target == null) {

                sender.sendMessage(textManager.getErrorColor() + "No block found within the maximum jump range.");

            } else {

                teleportManager.teleport((Player) sender, eyeLoc);

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
