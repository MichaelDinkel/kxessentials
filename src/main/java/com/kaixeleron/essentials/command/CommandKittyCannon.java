package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Cat;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class CommandKittyCannon implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final Random r;

    public CommandKittyCannon(EssentialsMain m, TextManager textManager) {

        this.m = m;

        this.textManager = textManager;

        r = new Random();

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Location l = ((Player) sender).getEyeLocation();

            final Cat cat = (Cat) ((Player) sender).getWorld().spawnEntity(l, EntityType.CAT);

            Cat.Type[] types = Cat.Type.values();

            cat.setCatType(types[r.nextInt(types.length)]);
            cat.setVelocity(l.getDirection().multiply(2));

            new BukkitRunnable() {

                @Override
                public void run() {

                    cat.getWorld().createExplosion(cat.getLocation(), 0.0F);
                    cat.remove();

                }

            }.runTaskLater(m, 20L);

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
