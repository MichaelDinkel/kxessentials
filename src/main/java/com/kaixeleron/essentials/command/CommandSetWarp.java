package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import com.kaixeleron.essentials.manager.WarpManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

public class CommandSetWarp implements CommandExecutor {

    private final TextManager textManager;

    private final WarpManager warpManager;

    public CommandSetWarp(TextManager textManager, WarpManager warpManager) {

        this.textManager = textManager;

        this.warpManager = warpManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                if (warpManager.listWarps().contains(args[0])) {

                    sender.sendMessage(textManager.getErrorColor() + "Warp " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + " already exists.");

                } else {

                    if (args[0].matches("[a-zA-Z0-9]+")) {

                        Permission star = Bukkit.getPluginManager().getPermission("kxessentials.warp.*");

                        if (star == null) {

                            star = new Permission("kxessentials.warp.*", PermissionDefault.TRUE);

                            Bukkit.getPluginManager().addPermission(star);

                        }

                        Permission perm = new Permission(String.format("kxessentials.warp.%s", args[0]), PermissionDefault.OP);
                        perm.addParent(star, true);
                        Bukkit.getPluginManager().addPermission(perm);

                        warpManager.addWarp(args[0], ((Player) sender).getLocation());
                        sender.sendMessage(textManager.getMainColor() + "Warp " + textManager.getAltColor() + args[0] + textManager.getMainColor() + " set to your current location.");

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "Invalid warp name. Must be alphanumeric.");

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
