package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.OfflinePlayerManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class CommandDelHome implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final OfflinePlayerManager offlinePlayerManager;

    private final PlayerDataManager playerDataManager;

    public CommandDelHome(EssentialsMain m, TextManager textManager, OfflinePlayerManager offlinePlayerManager, PlayerDataManager playerDataManager) {

        this.m = m;

        this.textManager = textManager;

        this.offlinePlayerManager = offlinePlayerManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            if (sender instanceof Player || sender.hasPermission("kxessentials.command.delhome.other")) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

            }

        } else {

            if (args[0].contains(":")) {

                if (sender.hasPermission("kxessentials.command.delhome.other")) {

                    final String[] split = args[0].split(":");

                    if (split.length > 1) {

                        new BukkitRunnable() {

                            @Override
                            public void run() {

                                final GameProfile profile = offlinePlayerManager.getOfflineProfile(split[0]);

                                new BukkitRunnable() {

                                    @Override
                                    public void run() {

                                        if (profile == null) {

                                            sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                                        } else {

                                            OfflinePlayer p = Bukkit.getOfflinePlayer(profile.getId());

                                            if (playerDataManager.getHome(p, split[1]) == null) {

                                                sender.sendMessage(textManager.getErrorColor() + "Home " + textManager.getMainColor() + split[1] + textManager.getErrorColor() + " does not exist.");

                                            } else {

                                                playerDataManager.removeHome(p, split[1]);

                                                sender.sendMessage(textManager.getMainColor() + "Home " + textManager.getAltColor() + split[1] + textManager.getMainColor() + " deleted for " + textManager.getAltColor() + (profile.getName() == null ? split[0] : profile.getName()) + textManager.getMainColor() + ".");

                                            }

                                        }

                                    }

                                }.runTask(m);

                            }

                        }.runTaskAsynchronously(m);

                    } else {

                        sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                        sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                    }

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "You do not have permission to delete homes of other players.");

                }

            } else {

                if (sender instanceof Player) {

                    if (playerDataManager.getHome((Player) sender, args[0]) == null) {

                        sender.sendMessage(textManager.getErrorColor() + "Home " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + " does not exist.");

                    } else {

                        playerDataManager.removeHome((Player) sender, args[0]);

                        sender.sendMessage(textManager.getMainColor() + "Home " + textManager.getAltColor() + args[0] + textManager.getMainColor() + " deleted.");

                    }

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        }

        return true;

    }

}
