package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpAll implements CommandExecutor {

    private final TextManager textManager;

    public CommandTpAll(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Location l = ((Player) sender).getLocation();

            for (Player p : Bukkit.getOnlinePlayers()) {

                if (!p.equals(sender)) p.teleport(l);

            }

            sender.sendMessage(textManager.getMainColor() + "Teleported all players.");

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
