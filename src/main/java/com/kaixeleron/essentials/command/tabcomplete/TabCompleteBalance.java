package com.kaixeleron.essentials.command.tabcomplete;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class TabCompleteBalance implements TabCompleter {

    @SuppressWarnings("NullableProblems")
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

        List<String> suggestions = new ArrayList<>();

        if (args.length == 1) {

            if (sender.hasPermission("kxessentials.command.balance.other")) {

                for (Player player : Bukkit.getOnlinePlayers()) {

                    if (player.getName().toLowerCase().startsWith(args[0].toLowerCase())) {

                        suggestions.add(player.getName());

                    }

                }

            }

        }

        return suggestions;

    }

}
