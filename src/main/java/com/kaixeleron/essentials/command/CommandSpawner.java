package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.SpawnerManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class CommandSpawner implements CommandExecutor {

    private final TextManager textManager;

    private final SpawnerManager spawnerManager;

    private int maxDist;

    public CommandSpawner(TextManager textManager, SpawnerManager spawnerManager, int maxDist) {

        this.textManager = textManager;

        this.spawnerManager = spawnerManager;

        this.maxDist = maxDist;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                boolean any = false;

                StringBuilder list = new StringBuilder();
                list.append(textManager.getMainColor()).append("Available types: ");

                for (EntityType e : spawnerManager.getValidEntities()) {

                    String formatted = e.toString().toLowerCase().replace("_", "");

                    if (sender.hasPermission(String.format("kxessentials.spawner.%s", formatted))) {

                        list.append(textManager.getAltColor()).append(formatted).append(textManager.getMainColor()).append(", ");

                        any = true;

                    }

                }

                if (any) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());
                    sender.sendMessage(list.substring(0, list.length() - 4));

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "You do not have permission to use any entities when changing a spawner.");

                }

            } else {

                EntityType type = null;

                String formatted = "";

                for (EntityType e : spawnerManager.getValidEntities()) {

                    formatted = e.toString().replace("_", "");

                    if (formatted.equalsIgnoreCase(args[0])) {

                        type = e;
                        break;

                    }

                }

                if (type == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid entity type " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + ".");

                } else if (!sender.hasPermission(String.format("kxessentials.spawner.%s", formatted.toLowerCase()))) {

                    sender.sendMessage(textManager.getErrorColor() + "You do not have permission to change a spawner to that entity.");

                } else {

                    Block spawner = null;

                    Location eyeLoc = ((Player) sender).getEyeLocation();

                    Vector direction = eyeLoc.getDirection();
                    direction.normalize();

                    for (int i = 0; i < maxDist; i++) {

                        eyeLoc.add(direction);

                        Block b = eyeLoc.getBlock();

                        if (b.getType().equals(Material.SPAWNER)) {

                            spawner = b;

                            break;

                        }

                    }

                    if (spawner == null) {

                        sender.sendMessage(textManager.getErrorColor() + "No spawner found in your line of sight.");

                    } else {

                        BlockState state = spawner.getState();

                        ((CreatureSpawner) state).setSpawnedType(type);

                        state.update();

                        sender.sendMessage(textManager.getMainColor() + "Spawner type changed to " + textManager.getAltColor() + formatted.toLowerCase() + textManager.getMainColor() + ".");

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
