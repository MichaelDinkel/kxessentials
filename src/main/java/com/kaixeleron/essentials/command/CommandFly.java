package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandFly implements CommandExecutor {

    private final TextManager textManager;

    public CommandFly(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.fly.other")) {

            if (sender instanceof Player) {

                boolean flying = ((Player) sender).getAllowFlight();

                ((Player) sender).setAllowFlight(!flying);

                sender.sendMessage(textManager.getMainColor() + "Flight " + textManager.getAltColor() + (!flying ? "enabled" : "disabled") + textManager.getMainColor() + ".");

            } else {

                if (sender.hasPermission("kxessentials.command.fly.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                boolean flying = p.getAllowFlight();

                p.setAllowFlight(!flying);

                sender.sendMessage(textManager.getMainColor() + "Flight " + textManager.getAltColor() + (!flying ? "enabled" : "disabled") + textManager.getMainColor() + " on " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

            }

        }

        return true;

    }

}
