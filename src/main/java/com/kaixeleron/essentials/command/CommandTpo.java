package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.BackManager;
import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpo implements CommandExecutor {

    private final BackManager backManager;

    private final TextManager textManager;

    public CommandTpo(BackManager backManager, TextManager textManager) {

        this.backManager = backManager;
        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                Player p = Bukkit.getPlayerExact(args[0]);

                if (p == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                } else if (p.equals(sender)) {

                    sender.sendMessage(textManager.getErrorColor() + "You cannot teleport to yourself.");

                } else {

                    backManager.setBackLocation(p, p.getLocation());

                    ((Player) sender).teleport(p.getLocation());
                    sender.sendMessage("Teleporting...");

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
