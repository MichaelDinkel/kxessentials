package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpToggle implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    private final PlayerDataManager playerDataManager;

    public CommandTpToggle(TextManager textManager, TeleportManager teleportManager, PlayerDataManager playerDataManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            boolean disabled = teleportManager.isDisabled((Player) sender);

            playerDataManager.setTeleportRequestsDisabled((Player) sender, !disabled);

            if (!disabled) {

                teleportManager.addDisabled((Player) sender);

                sender.sendMessage(textManager.getMainColor() + "Teleport requests disabled.");

            } else {

                teleportManager.removeDisabled((Player) sender);

                sender.sendMessage(textManager.getMainColor() + "Teleport requests enabled.");

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

}
