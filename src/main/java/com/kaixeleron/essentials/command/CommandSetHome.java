package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.OfflinePlayerManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class CommandSetHome implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final OfflinePlayerManager offlinePlayerManager;

    private final PlayerDataManager playerDataManager;

    public CommandSetHome(EssentialsMain m, TextManager textManager, OfflinePlayerManager offlinePlayerManager, PlayerDataManager playerDataManager) {

        this.m = m;

        this.textManager = textManager;

        this.offlinePlayerManager = offlinePlayerManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                if (args[0].contains(":")) {

                    if (sender.hasPermission("kxessentials.command.sethome.other")) {

                        final String[] split = args[0].split(":");

                        if (split.length > 1) {

                            if (split[1].matches("[a-zA-Z0-9]+")) {

                                new BukkitRunnable() {

                                    @Override
                                    public void run() {

                                        final GameProfile profile = offlinePlayerManager.getOfflineProfile(split[0]);

                                        new BukkitRunnable() {

                                            @Override
                                            public void run() {

                                                if (profile == null) {

                                                    sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                                                } else {

                                                    playerDataManager.setHome(Bukkit.getOfflinePlayer(profile.getId()), split[1], ((Player) sender).getLocation());

                                                    sender.sendMessage(textManager.getMainColor() + "Home " + textManager.getAltColor() + split[1] + textManager.getMainColor() + " set for " + textManager.getAltColor() + (profile.getName() == null ? split[0] : profile.getName()) + textManager.getMainColor() + ".");

                                                }

                                            }

                                        }.runTask(m);

                                    }

                                }.runTaskAsynchronously(m);

                            } else {

                                sender.sendMessage(textManager.getErrorColor() + "Invalid home name. Must be alphanumeric.");

                            }

                        } else {

                            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                        }

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "You do not have permission to set homes for other players.");

                    }

                } else {

                    if (args[0].matches("[a-zA-Z0-9]+")) {

                        playerDataManager.setHome((Player) sender, args[0], ((Player) sender).getLocation());

                        sender.sendMessage(textManager.getMainColor() + "Home " + textManager.getAltColor() + args[0] + textManager.getMainColor() + " set.");

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "Invalid home name. Must be alphanumeric.");

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
