package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class CommandNear implements CommandExecutor {

    private final TextManager textManager;

    private final int maxDist;

    private final DecimalFormat format;

    public CommandNear(TextManager textManager, int maxDist) {

        this.textManager = textManager;

        this.maxDist = maxDist;

        format = new DecimalFormat("0.0");

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.near.other")) {

            if (sender instanceof Player) {

                StringBuilder builder = new StringBuilder();

                for (Player p : Bukkit.getOnlinePlayers()) {

                    if (!p.equals((Player) sender) && ((Player) sender).getWorld().equals(p.getWorld())) {

                        double distance = p.getLocation().distance(((Player) sender).getLocation());

                        if (distance <= maxDist) {

                            builder.append(textManager.getAltColor()).append(p.getName()).append(textManager.getMainColor()).append(" (").append(textManager.getAltColor()).append(format.format(distance)).append(" blocks").append(textManager.getMainColor()).append("), ");

                        }

                    }

                }

                if (builder.length() > 0) {

                    sender.sendMessage(textManager.getMainColor() + String.format("Nearby players: %s", builder.substring(0, builder.length() - 2)));

                } else {

                    sender.sendMessage(textManager.getMainColor() + "There are no nearby players.");

                }

            } else {

                if (sender.hasPermission("kxessentials.command.near.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player target = Bukkit.getPlayerExact(args[0]);

            if (target == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                StringBuilder builder = new StringBuilder();

                for (Player p : Bukkit.getOnlinePlayers()) {

                    if (sender instanceof Player && !p.equals((Player) sender) && ((Player) sender).getWorld().equals(p.getWorld())) {

                        double distance = p.getLocation().distance(((Player) sender).getLocation());

                        if (distance <= maxDist) {

                            builder.append(textManager.getAltColor()).append(p.getName()).append(textManager.getMainColor()).append(" (").append(textManager.getAltColor()).append(format.format(distance)).append(" blocks").append(textManager.getMainColor()).append("), ");

                        }

                    }

                }

                if (builder.length() > 0) {

                    sender.sendMessage(textManager.getMainColor() + String.format("Players near " + textManager.getAltColor() + target.getName() + textManager.getMainColor() + ": %s", builder.substring(0, builder.length() - 2)));

                } else {

                    sender.sendMessage(textManager.getMainColor() + "There are no players near " + textManager.getAltColor() + target.getName() + textManager.getMainColor() + ".");

                }

            }

        }

        return true;

    }

}
