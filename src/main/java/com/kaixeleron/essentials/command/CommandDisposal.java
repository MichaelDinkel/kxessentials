package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandDisposal implements CommandExecutor {

    private final TextManager textManager;

    public CommandDisposal(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            ((Player) sender).openInventory(Bukkit.createInventory(null, 54, "Disposal"));

        } else sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        return true;

    }

}
