package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandHat implements CommandExecutor {

    private final TextManager textManager;

    public CommandHat(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            ItemStack hat = ((Player) sender).getInventory().getHelmet();

            ((Player) sender).getInventory().setHelmet(((Player) sender).getInventory().getItemInMainHand());

            if (!(hat == null || hat.getType().equals(Material.AIR))) {

                ((Player) sender).getInventory().setItemInMainHand(hat);

            } else {

                ((Player) sender).getInventory().setItemInMainHand(null);

            }

            sender.sendMessage(textManager.getMainColor() + "Enjoy your new hat.");

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
