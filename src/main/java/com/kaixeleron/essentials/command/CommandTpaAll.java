package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpaAll implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    public CommandTpaAll(TextManager textManager, TeleportManager teleportManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            for (Player p : Bukkit.getOnlinePlayers()) {

                if (!teleportManager.isDisabled(p) && !p.equals(sender)) {

                    if (teleportManager.isAcceptingAll(p)) {

                        teleportManager.teleport(p, ((Player) sender).getLocation());

                        sender.sendMessage(textManager.getAltColor() + p.getName() + textManager.getMainColor() + " has accepted your teleport request.");
                        p.sendMessage(textManager.getMainColor() + "Accepted teleport request from " + textManager.getAltColor() + sender.getName() + textManager.getMainColor() + ".");

                    } else {

                        teleportManager.addTeleportHereRequest((Player) sender, p);

                        p.sendMessage(textManager.getAltColor() + sender.getName() + textManager.getMainColor() + " has sent you a request to teleport to them. Type " + textManager.getAltColor() + "/tpaccept" + textManager.getMainColor() + " to accept or " + textManager.getAltColor() + "/tpdeny" + textManager.getMainColor() + " to deny.");

                    }

                }

            }

            sender.sendMessage(textManager.getMainColor() + "Teleport here request sent to all players.");

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
