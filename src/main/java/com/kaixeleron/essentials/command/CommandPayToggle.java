package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandPayToggle implements CommandExecutor {

    private final TextManager textManager;

    private final PlayerDataManager playerDataManager;

    public CommandPayToggle(TextManager textManager, PlayerDataManager playerDataManager) {

        this.textManager = textManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            boolean disabled = !playerDataManager.isPayDisabled((Player) sender);

            if (label.equalsIgnoreCase("payon")) {

                disabled = false;

            } else if (label.equalsIgnoreCase("payoff")) {

                disabled = true;

            }

            playerDataManager.setPayDisabled((Player) sender, disabled);

            if (disabled) {

                sender.sendMessage(textManager.getMainColor() + "Receipt of payments " + textManager.getAltColor() + "disabled" + textManager.getMainColor() + ".");

            } else {

                sender.sendMessage(textManager.getMainColor() + "Receipt of payments " + textManager.getAltColor() + "enabled" + textManager.getMainColor() + ".");

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
