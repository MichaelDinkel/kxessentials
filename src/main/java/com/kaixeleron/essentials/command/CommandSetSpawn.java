package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSetSpawn implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    public CommandSetSpawn(TextManager textManager, TeleportManager teleportManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            teleportManager.setSpawn(((Player) sender).getLocation());
            ((Player) sender).getWorld().setSpawnLocation(((Player) sender).getLocation());

            sender.sendMessage(textManager.getMainColor() + "Spawnpoint set to your current location.");

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
