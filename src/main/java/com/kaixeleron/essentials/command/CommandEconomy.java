package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.OfflinePlayerManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import java.math.BigDecimal;

public class CommandEconomy implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final OfflinePlayerManager offlinePlayerManager;

    private final PlayerDataManager playerDataManager;

    public CommandEconomy(EssentialsMain m, TextManager textManager, OfflinePlayerManager offlinePlayerManager, PlayerDataManager playerDataManager) {

        this.m = m;

        this.textManager = textManager;

        this.offlinePlayerManager = offlinePlayerManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, final String[] args) {

        if (args.length < 2) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else {

            switch (args[0].toLowerCase()) {

                case "give":

                    if (args.length > 2) {

                        double amount = -1.0D;

                        try {

                            amount = Double.parseDouble(args[2]);

                        } catch (NumberFormatException ignored) {}

                        if (amount >= 0) {

                            final double subclassAmount = amount;

                            new BukkitRunnable() {

                                @Override
                                public void run() {

                                    final GameProfile profile = offlinePlayerManager.getOfflineProfile(args[1]);

                                    new BukkitRunnable() {

                                        @Override
                                        public void run() {

                                            if (profile == null) {

                                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                                            } else {

                                                OfflinePlayer p = Bukkit.getOfflinePlayer(profile.getId());

                                                playerDataManager.setBalance(p, playerDataManager.getBalance(p) + subclassAmount);

                                                sender.sendMessage("Added " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(subclassAmount).stripTrailingZeros().toPlainString() + textManager.getMainColor() + " to " + textManager.getAltColor() + (profile.getName() == null ? args[1] : profile.getName()) + textManager.getMainColor() + "'s balance.");

                                            }

                                        }

                                    }.runTask(m);

                                }

                            }.runTaskAsynchronously(m);

                        } else {

                            sender.sendMessage(textManager.getErrorColor() + "Invalid amount. Must be a positive decimal.");

                        }

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "You must specify an amount to give.");

                    }

                    break;

                case "take":

                    if (args.length > 2) {

                        double amount = -1.0D;

                        try {

                            amount = Double.parseDouble(args[2]);

                        } catch (NumberFormatException ignored) {}

                        if (amount >= 0) {

                            final double subclassAmount = amount;

                            new BukkitRunnable() {

                                @Override
                                public void run() {

                                    final GameProfile profile = offlinePlayerManager.getOfflineProfile(args[1]);

                                    new BukkitRunnable() {

                                        @Override
                                        public void run() {

                                            if (profile == null) {

                                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                                            } else {

                                                OfflinePlayer p = Bukkit.getOfflinePlayer(profile.getId());

                                                playerDataManager.setBalance(p, playerDataManager.getBalance(p) - subclassAmount);

                                                sender.sendMessage("Took " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(subclassAmount).stripTrailingZeros().toPlainString() + textManager.getMainColor() + " from " + textManager.getAltColor() + (profile.getName() == null ? args[1] : profile.getName()) + textManager.getMainColor() + "'s balance.");

                                            }

                                        }

                                    }.runTask(m);

                                }

                            }.runTaskAsynchronously(m);

                        } else {

                            sender.sendMessage(textManager.getErrorColor() + "Invalid amount. Must be a positive decimal.");

                        }

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "You must specify an amount to take.");

                    }

                    break;

                case "set":

                    if (args.length > 2) {

                        double amount = -1.0D;

                        try {

                            amount = Double.parseDouble(args[2]);

                        } catch (NumberFormatException ignored) {}

                        if (amount >= 0) {

                            final double subclassAmount = amount;

                            new BukkitRunnable() {

                                @Override
                                public void run() {

                                    final GameProfile profile = offlinePlayerManager.getOfflineProfile(args[1]);

                                    new BukkitRunnable() {

                                        @Override
                                        public void run() {

                                            if (profile == null) {

                                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                                            } else {

                                                OfflinePlayer p = Bukkit.getOfflinePlayer(profile.getId());

                                                playerDataManager.setBalance(p, subclassAmount);

                                                sender.sendMessage(textManager.getMainColor() + "Set " + textManager.getAltColor() + (profile.getName() == null ? args[1] : profile.getName()) + textManager.getMainColor() + "'s balance to " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(subclassAmount).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                                            }

                                        }

                                    }.runTask(m);

                                }

                            }.runTaskAsynchronously(m);

                        } else {

                            sender.sendMessage(textManager.getErrorColor() + "Invalid amount. Must be a positive decimal.");

                        }

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "You must specify an amount to set.");

                    }

                    break;

                case "reset":

                    new BukkitRunnable() {

                        @Override
                        public void run() {

                            final GameProfile profile = offlinePlayerManager.getOfflineProfile(args[1]);

                            new BukkitRunnable() {

                                @Override
                                public void run() {

                                    if (profile == null) {

                                        sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                                    } else {

                                        OfflinePlayer p = Bukkit.getOfflinePlayer(profile.getId());

                                        playerDataManager.resetBalance(p);

                                        sender.sendMessage(textManager.getMainColor() + "Reset " + textManager.getAltColor() + (profile.getName() == null ? args[1] : profile.getName()) + textManager.getMainColor() + "'s balance.");

                                    }

                                }

                            }.runTask(m);

                        }

                    }.runTaskAsynchronously(m);

                    break;

                default:

                    sender.sendMessage(textManager.getErrorColor() + "Invalid operation. Must be " + textManager.getMainColor() + "give" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "take" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "set" + textManager.getErrorColor() + ", or " + textManager.getMainColor() + "reset" + textManager.getErrorColor() + ".");

                    break;

            }

        }

        return true;

    }

}
