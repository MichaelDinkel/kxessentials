package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigDecimal;

public class CommandMaxHealth implements CommandExecutor {

    private final TextManager textManager;

    public CommandMaxHealth(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            if (!(sender instanceof Player) && !sender.hasPermission("kxessentials.command.maxhealth.other")) {

                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

            } else {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            }

        } else {

            Player p = null;

            if (args.length > 1 && sender.hasPermission("kxessentials.command.maxhealth.other")) {

                p = Bukkit.getPlayerExact(args[1]);

                if (p == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                }

            } else {

                if (sender instanceof Player) {

                    p = (Player) sender;

                } else {

                    if (sender.hasPermission("kxessentials.command.maxhealth.other")) {

                        sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                        sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                    } else {

                        sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                    }

                }

            }

            if (p != null) {

                Double maxHealth;

                try {

                    maxHealth = Double.parseDouble(args[0]);

                } catch (NumberFormatException e) {

                    maxHealth = -1.0D;

                }

                if (maxHealth < 0.0D) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid max health. Must be a positive decimal number.");

                } else if (maxHealth > 2048.0D) {

                    maxHealth = 2048.0D;

                }

                p.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(maxHealth);

                sender.sendMessage(textManager.getMainColor() + "Set max health on " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " to " + textManager.getAltColor() + new BigDecimal(maxHealth).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

            }

        }

        return true;

    }

}
