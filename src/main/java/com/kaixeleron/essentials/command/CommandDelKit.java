package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.KitManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

public class CommandDelKit implements CommandExecutor {

    private final TextManager textManager;

    private final KitManager kitManager;

    public CommandDelKit(TextManager textManager, KitManager kitManager) {

        this.textManager = textManager;

        this.kitManager = kitManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                if (kitManager.deleteKit(args[0])) {

                    Permission perm = Bukkit.getPluginManager().getPermission(String.format("kxessentials.kit.%s", args[0]));

                    if (perm != null) Bukkit.getPluginManager().removePermission(perm);

                    sender.sendMessage(textManager.getMainColor() + "Deleted kit " + textManager.getAltColor() + args[0] + textManager.getMainColor() + ".");

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "Kit " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + " does not exist.");

                }

            }

        } else sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        return true;

    }

}
