package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

public class CommandRepair implements CommandExecutor {

    private final TextManager textManager;

    public CommandRepair(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0 || !sender.hasPermission("kxessentials.command.repair.all")) {

                ItemStack item = ((Player) sender).getInventory().getItemInMainHand();

                ItemMeta meta = item.getItemMeta();

                if (meta instanceof Damageable && ((Damageable) meta).getDamage() > 0) {

                    ((Damageable) meta).setDamage(0);
                    item.setItemMeta(meta);

                    sender.sendMessage(textManager.getMainColor() + "Item repaired.");

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "This item cannot be repaired.");

                }

            } else {

                if (args[0].equalsIgnoreCase("all")) {

                    int repaired = 0;

                    for (ItemStack i : ((Player) sender).getInventory().getContents()) {

                        if (i != null) {

                            ItemMeta meta = i.getItemMeta();

                            if (meta instanceof Damageable) {

                                if (((Damageable) meta).getDamage() > 0) {

                                    ((Damageable) meta).setDamage(0);
                                    i.setItemMeta(meta);
                                    repaired++;

                                }

                            }

                        }

                    }

                    if (repaired == 0) {

                        sender.sendMessage(textManager.getErrorColor() + "You have no items to repair.");

                    } else {

                        sender.sendMessage(textManager.getMainColor() + "Repaired " + textManager.getAltColor() + repaired + textManager.getMainColor() + " items.");

                    }

                } else {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
