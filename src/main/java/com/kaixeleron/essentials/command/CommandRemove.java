package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;

public class CommandRemove implements CommandExecutor {

    private TextManager textManager;

    public CommandRemove(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else if (args.length == 1) {

            if (sender instanceof Player) {

                int amount = removeEntities(args[0], ((Player) sender).getWorld(), -1L, null);

                if (amount == 0) {

                    sender.sendMessage(textManager.getErrorColor() + "No entities found.");

                } else {

                    sender.sendMessage(textManager.getMainColor() + "Removed " + textManager.getAltColor() + amount + textManager.getMainColor() + " entit" + (amount == 1 ? "y." : "ies."));

                }

            } else {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            }

        } else if (args.length == 2 || !(sender instanceof Player)) {

            World w = Bukkit.getWorld(args[1]);

            if (w == null) {

                sender.sendMessage(textManager.getErrorColor() + "World not found.");

            } else {

                int amount = removeEntities(args[0], w, -1L, null);

                if (amount == 0) {

                    sender.sendMessage(textManager.getErrorColor() + "No entities found.");

                } else {

                    sender.sendMessage(textManager.getMainColor() + "Removed " + textManager.getAltColor() + amount + textManager.getMainColor() + " entit" + (amount == 1 ? "y." : "ies."));

                }

            }

        } else {

            World w = Bukkit.getWorld(args[1]);

            Long r = -1L;

            try {

                r = Long.parseLong(args[2]);

            } catch (NumberFormatException ignored) {}

            if (w == null) {

                sender.sendMessage(textManager.getErrorColor() + "World not found.");

            } else if (r < 0L) {

                sender.sendMessage(textManager.getErrorColor() + "Invalid radius. Must be a positive integer.");

            } else {

                int amount = removeEntities(args[0], w, r, ((Player) sender).getLocation());

                if (amount == 0) {

                    sender.sendMessage(textManager.getErrorColor() + "No entities found.");

                } else {

                    sender.sendMessage(textManager.getMainColor() + "Removed " + textManager.getAltColor() + amount + textManager.getMainColor() + " entities.");

                }

            }

        }

        return true;

    }

    private int removeEntities(String name, World world, long radius, Location center) {

        int out = 0;

        switch (name) {

            case "all":

                for (Entity e : world.getEntities()) {

                    if (!e.getType().equals(EntityType.PLAYER)) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "tamed":

                for (Entity e : world.getEntities()) {

                    if (e instanceof Tameable && ((Tameable) e).isTamed()) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "named":

                for (Entity e : world.getEntities()) {

                    if (e.getCustomName() != null) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "drops":

                for (Entity e : world.getEntities()) {

                    if (e.getType().equals(EntityType.DROPPED_ITEM)) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "arrows":

                for (Entity e : world.getEntities()) {

                    if (e.getType().equals(EntityType.ARROW) || e.getType().equals(EntityType.SPECTRAL_ARROW)) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "boats":

                for (Entity e : world.getEntities()) {

                    if (e.getType().equals(EntityType.BOAT)) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "minecarts":

                for (Entity e : world.getEntities()) {

                    if (e.getType().equals(EntityType.MINECART) || e.getType().equals(EntityType.MINECART_CHEST) || e.getType().equals(EntityType.MINECART_COMMAND) || e.getType().equals(EntityType.MINECART_FURNACE) || e.getType().equals(EntityType.MINECART_HOPPER) || e.getType().equals(EntityType.MINECART_MOB_SPAWNER) || e.getType().equals(EntityType.MINECART_TNT)) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "xp":

                for (Entity e : world.getEntities()) {

                    if (e.getType().equals(EntityType.EXPERIENCE_ORB)) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "paintings":

                for (Entity e : world.getEntities()) {

                    if (e.getType().equals(EntityType.PAINTING)) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "itemframes":

                for (Entity e : world.getEntities()) {

                    if (e.getType().equals(EntityType.ITEM_FRAME)) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "endercrystals":

                for (Entity e : world.getEntities()) {

                    if (e.getType().equals(EntityType.ENDER_CRYSTAL)) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "monsters":

                for (Entity e : world.getEntities()) {

                    if (e instanceof Monster) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "animals":

                for (Entity e : world.getEntities()) {

                    if (e instanceof Animals) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "ambient":

                for (Entity e : world.getEntities()) {

                    if (e instanceof Ambient) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

            case "mobs":

                for (Entity e : world.getEntities()) {

                    if (e instanceof Ambient || e instanceof Animals || e instanceof Monster) {

                        if (center != null) {

                            if (e.getLocation().distance(center) < radius) {

                                e.remove();
                                out++;

                            }

                        } else {

                            e.remove();
                            out++;

                        }

                    }

                }

                break;

        }

        return out;

    }

}
