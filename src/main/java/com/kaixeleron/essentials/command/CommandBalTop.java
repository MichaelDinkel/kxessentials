package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.database.data.BalTopEntry;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.math.BigDecimal;
import java.util.List;

public class CommandBalTop implements CommandExecutor {

    private final TextManager textManager;

    private final PlayerDataManager playerDataManager;

    public CommandBalTop(TextManager textManager, PlayerDataManager playerDataManager) {

        this.textManager = textManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (playerDataManager.isBalTopReady()) {

            int page = 1;

            if (args.length > 0) {

                try {

                    page = Integer.parseInt(args[0]);

                    if (page < 1) page = 1;
                    if (page > playerDataManager.getBalTopPages()) page = playerDataManager.getBalTopPages();

                } catch (NumberFormatException e) {

                    page = 1;

                }

            }

            List<BalTopEntry> entries = playerDataManager.getBalTop(page);

            sender.sendMessage(textManager.getAltColor() + "=====" + textManager.getMainColor() + "Top balances" + textManager.getAltColor() + "=====");
            sender.sendMessage(textManager.getMainColor() + "Server total: " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(playerDataManager.getBalanceTotal()).stripTrailingZeros().toPlainString());

            for (int i = 0; i < entries.size(); i++) {

                sender.sendMessage(textManager.getAltColor() + String.format("%d. ", i + 1) + textManager.getMainColor() + entries.get(i).getName() + textManager.getAltColor() + ": " + textManager.getMainColor() + textManager.getCurrencySymbol() + new BigDecimal(entries.get(i).getAmount()).stripTrailingZeros().toPlainString());

            }

        } else {

            sender.sendMessage(textManager.getErrorColor() + "Top balance data is still loading. Please wait.");

        }

        return true;

    }

}
