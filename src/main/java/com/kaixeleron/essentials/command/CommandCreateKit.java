package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.KitManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

public class CommandCreateKit implements CommandExecutor {

    private final TextManager textManager;

    private final KitManager kitManager;

    public CommandCreateKit(TextManager textManager, KitManager kitManager) {

        this.textManager = textManager;

        this.kitManager = kitManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                int cooldown = 0;

                if (args.length > 1) {

                    try {

                        cooldown = Integer.parseInt(args[1]);

                        if (cooldown < 0) cooldown = 0;

                    } catch (NumberFormatException ignored) {}

                }

                if (args[0].matches("[a-zA-Z0-9]+")) {

                    if (kitManager.createKit(args[0], ((Player) sender).getInventory().getContents(), cooldown * 1000L)) {

                        Permission star = Bukkit.getPluginManager().getPermission("kxessentials.kit.*");

                        if (star == null) {

                            star = new Permission("kxessentials.kit.*", PermissionDefault.FALSE);
                            Bukkit.getPluginManager().addPermission(star);

                        }

                        Permission perm = new Permission(String.format("kxessentials.kit.%s", args[0]), PermissionDefault.OP);
                        perm.addParent(star, true);

                        Bukkit.getPluginManager().addPermission(perm);

                        sender.sendMessage(textManager.getMainColor() + "Created kit " + textManager.getAltColor() + args[0] + textManager.getMainColor() + " with your current inventory" + (cooldown > 0 ? " with a cooldown of " + textManager.getAltColor() + cooldown + textManager.getMainColor() + " seconds." : " with no cooldown."));

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "Kit " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + " already exists.");

                    }

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid kit name. Must be alphanumeric.");

                }

            }

        } else sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        return true;

    }

}
