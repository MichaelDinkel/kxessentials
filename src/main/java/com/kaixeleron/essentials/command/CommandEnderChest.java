package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandEnderChest implements CommandExecutor {

    private final TextManager textManager;

    public CommandEnderChest(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0 || !sender.hasPermission("kxessentials.command.enderchest.other")) {

                ((Player) sender).openInventory(((Player) sender).getEnderChest());

            } else {

                Player p = Bukkit.getPlayerExact(args[0]);

                if (p == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                } else {

                    ((Player) sender).openInventory(p.getEnderChest());

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
