package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandBroadcast implements CommandExecutor {

    private final TextManager textManager;

    public CommandBroadcast(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String args[]) {

        if (args.length == 0) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else {

            StringBuilder message = new StringBuilder();

            for (String s : args) {

                message.append(s).append(" ");

            }

            Bukkit.broadcastMessage(textManager.getMainColor() + "[" + textManager.getAltColor() + "Broadcast" + textManager.getMainColor() + "] " + textManager.getMessageColor() + ChatColor.translateAlternateColorCodes('&', message.substring(0, message.length() - 1)));

        }

        return true;

    }

}
