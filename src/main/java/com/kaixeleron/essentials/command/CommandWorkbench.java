package com.kaixeleron.essentials.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

public class CommandWorkbench implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            ((Player) sender).openInventory(Bukkit.createInventory(null, InventoryType.WORKBENCH));

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

}
