package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.GodManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandGod implements CommandExecutor {

    private final TextManager textManager;

    private final GodManager godManager;

    public CommandGod(TextManager textManager, GodManager godManager) {

        this.textManager = textManager;
        this.godManager = godManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.god.other")) {

            if (sender instanceof Player) {

                boolean god = godManager.isGod((Player) sender);

                if (god) {

                    godManager.removeGod((Player) sender);

                } else {

                    godManager.addGod((Player) sender);

                }

                sender.sendMessage(textManager.getMainColor() + "God mode " + textManager.getAltColor() + (!god ? "enabled" : "disabled") + textManager.getMainColor() + ".");

            } else {

                if (sender.hasPermission("kxessentials.command.god.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                boolean god = godManager.isGod(p);

                if (god) {

                    godManager.removeGod(p);

                } else {

                    godManager.addGod(p);

                }

                sender.sendMessage(textManager.getMainColor() + "God mode " + textManager.getAltColor() + (!god ? "enabled" : "disabled") + textManager.getMainColor() + " on " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

            }

        }

        return true;

    }

}
