package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpAuto implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    private final PlayerDataManager playerDataManager;

    public CommandTpAuto(TextManager textManager, TeleportManager teleportManager, PlayerDataManager playerDataManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            boolean allowed = teleportManager.isAcceptingAll((Player) sender);

            playerDataManager.setAcceptingAllTeleports((Player) sender, !allowed);

            if (allowed) {

                teleportManager.removeAcceptingAll((Player) sender);

                sender.sendMessage(textManager.getMainColor() + "No longer accepting all teleport requests.");

            } else {

                teleportManager.addAcceptingAll((Player) sender);

                sender.sendMessage(textManager.getMainColor() + "Now accepting all teleport requests.");

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

}
