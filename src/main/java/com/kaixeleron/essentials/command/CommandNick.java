package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.NicknameManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandNick implements CommandExecutor {

    private final TextManager textManager;

    private final NicknameManager nicknameManager;

    private final PlayerDataManager playerDataManager;

    private int maxLength, minLength;

    public CommandNick(TextManager textManager, NicknameManager nicknameManager, PlayerDataManager playerDataManager, int maxLength, int minLength) {

        this.textManager = textManager;

        this.nicknameManager = nicknameManager;

        this.playerDataManager = playerDataManager;

        this.maxLength = maxLength;

        this.minLength = minLength;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            if (sender instanceof Player || sender.hasPermission("kxessentials.command.nick.other")) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

            }

        } else if (args.length == 1 || !sender.hasPermission("kxessentials.command.nick.other")) {

            if (sender instanceof Player) {

                if (args[0].equalsIgnoreCase("off")) {

                    sender.sendMessage(textManager.getMainColor() + "Nickname removed.");

                    nicknameManager.removeNickname((Player) sender);
                    playerDataManager.setNickname((Player) sender,null);

                } else {

                    if (sender.hasPermission("kxessentials.command.nick.nolimit") || (args[0].length() >= minLength && args[0].length() <= maxLength)) {

                        if (args[0].matches("[a-zA-Z0-9]+")) {

                            sender.sendMessage(textManager.getMainColor() + "Nickname set to " + textManager.getAltColor() + nicknameManager.getPrefix() + args[0] + textManager.getMainColor() + ".");

                            nicknameManager.setNickname((Player) sender, args[0]);
                            playerDataManager.setNickname((Player) sender, args[0]);

                        } else {

                            sender.sendMessage(textManager.getErrorColor() + "Invalid nickname. Nicknames must be alphanumeric.");

                        }

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "Invalid nickname length. Must be between " + textManager.getMainColor() + minLength + textManager.getErrorColor() + " and " + textManager.getMainColor() + maxLength + textManager.getErrorColor() + " characters long.");

                    }

                }

            } else {

                if (sender.hasPermission("kxessentials.command.nick.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[1]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                if (args[0].equalsIgnoreCase("off")) {

                    sender.sendMessage(textManager.getMainColor() + "Nickname removed from " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

                    nicknameManager.removeNickname(p);
                    playerDataManager.setNickname(p,null);

                } else {

                    if (sender.hasPermission("kxessentials.command.nick.nolimit") || (args[0].length() >= minLength && args[0].length() <= maxLength)) {

                        if (args[0].matches("[a-zA-Z0-9]+")) {

                            sender.sendMessage(textManager.getMainColor() + "Nickname set to " + textManager.getAltColor() + nicknameManager.getPrefix() + args[0] + textManager.getMainColor() + " on " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

                            nicknameManager.setNickname(p, args[0]);
                            playerDataManager.setNickname(p, args[0]);

                        } else {

                            sender.sendMessage(textManager.getErrorColor() + "Invalid nickname. Nicknames must be alphanumeric.");

                        }

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "Invalid nickname length. Must be between " + textManager.getMainColor() + minLength + textManager.getErrorColor() + " and " + textManager.getMainColor() + maxLength + textManager.getErrorColor() + " characters long.");

                    }

                }

            }

        }

        return true;

    }

}
