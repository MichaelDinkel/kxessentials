package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.AliasManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandItem implements CommandExecutor {

    private final TextManager textManager;

    private final AliasManager aliasManager;

    public CommandItem(TextManager textManager, AliasManager aliasManager) {

        this.textManager = textManager;

        this.aliasManager = aliasManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else if (args.length == 1) {

                Material type = null;

                for (Material m : Material.values()) {

                    if (m.toString().replace("_", "").equalsIgnoreCase(args[0])) {

                        type = m;
                        break;

                    }

                }

                if (type == null) {

                    type = aliasManager.getByAlias(args[0].toLowerCase());

                }

                if (type == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid item type " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + ".");

                } else {

                    ((Player) sender).getInventory().addItem(new ItemStack(type));

                    sender.sendMessage(textManager.getMainColor() + "Spawned " + textManager.getAltColor() + "1 "  + textManager.getAltColor() + type.toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + ".");

                }

            } else {

                int amount = -1;

                try {

                    amount = Integer.parseInt(args[1]);

                } catch (NumberFormatException ignored) {}

                if (amount != -1) {

                    Material type = null;

                    for (Material m : Material.values()) {

                        if (m.toString().replace("_", "").equalsIgnoreCase(args[0])) {

                            type = m;
                            break;

                        }

                    }

                    if (type == null) {

                        sender.sendMessage(textManager.getErrorColor() + "Invalid item type " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + ".");

                    } else {

                        ((Player) sender).getInventory().addItem(new ItemStack(type, amount));

                        sender.sendMessage(textManager.getMainColor() + "Spawned " + textManager.getAltColor() + amount + " " + textManager.getAltColor() + type.toString().toLowerCase().replace('_', ' ') + (amount == 1 ? "" : "s") + textManager.getMainColor() + ".");

                    }

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid item quantity " + textManager.getMainColor() + args[1] + textManager.getErrorColor() + ". Must be a positive integer.");

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
