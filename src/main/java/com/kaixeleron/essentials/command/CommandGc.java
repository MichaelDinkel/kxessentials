package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.TimeUnit;

public class CommandGc implements CommandExecutor {

    private final TextManager textManager;

    private Object minecraftServer;

    private Field getRecentTps;

    public CommandGc(TextManager textManager) {

        this.textManager = textManager;

        String version = Bukkit.getServer().getClass().getPackage().getName();
        version = version.substring(version.lastIndexOf('.') + 1);

        try {

            Class<?> minecraftServerClass = Class.forName(String.format("net.minecraft.server.%s.MinecraftServer", version));

            minecraftServer = minecraftServerClass.getMethod("getServer").invoke(null);

            getRecentTps = minecraftServerClass.getField("recentTps");

        } catch (ClassNotFoundException | InvocationTargetException | IllegalAccessException | NoSuchMethodException | NoSuchFieldException e) {

            minecraftServer = null;
            getRecentTps = null;

            System.err.println("Could not reflect to Minecraft server class.");
            e.printStackTrace();

        }

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();

        long uptime = rb.getUptime();
        StringBuilder tps = new StringBuilder("TPS from last 1m, 5m, 15m: ");

        double[] minecraftTps = new double[0];

        try {

            minecraftTps = (double[]) getRecentTps.get(minecraftServer);

            for (double d : minecraftTps) {

                d = Math.round(d * 100.0D) / 100.0D;

                tps.append(getTpsColor(d)).append(d).append(textManager.getMainColor()).append(", ");

            }

        } catch (IllegalAccessException e) {

            System.err.println("Could not get TPS from Minecraft server class.");

            e.printStackTrace();

        }

        sender.sendMessage(textManager.getMainColor() + "Uptime: " + getUptimeString(uptime));

        if (minecraftTps.length > 0) {

            sender.sendMessage(textManager.getMainColor() + tps.substring(0, tps.length() - 4));

        }

        sender.sendMessage(textManager.getMainColor() + "Max memory: " + textManager.getAltColor() + String.format("%d MB", Runtime.getRuntime().maxMemory() / 1048576));
        sender.sendMessage(textManager.getMainColor() + "Allocated memory: " + textManager.getAltColor() + String.format("%d MB", Runtime.getRuntime().totalMemory() / 1048576));
        sender.sendMessage(textManager.getMainColor() + "Free memory: " + textManager.getAltColor() + String.format("%d MB", Runtime.getRuntime().freeMemory() / 1048576));

        for (World w : Bukkit.getWorlds()) {

            StringBuilder message = new StringBuilder();

            message.append(textManager.getMainColor());

            switch (w.getEnvironment()) {

                case NORMAL:

                    message.append("Overworld ");

                    break;

                case NETHER:

                    message.append("Nether ");

                    break;

                case THE_END:

                    message.append("End ");

                    break;

                default:

                    String environment = w.getEnvironment().toString().toLowerCase().replace('_', ' ');
                    environment = environment.substring(0, 1).toUpperCase() + environment.substring(1).toLowerCase();

                    message.append(environment).append(" ");

                    break;


            }

            message.append(textManager.getAltColor()).append(w.getName()).append(textManager.getMainColor()).append(": ").append(textManager.getAltColor()).append(w.getLoadedChunks().length).append(textManager.getMainColor()).append(" chunks, ").append(textManager.getAltColor()).append(w.getEntities().size()).append(textManager.getMainColor()).append(" entities");

            sender.sendMessage(message.toString());

        }

        return true;

    }

    private String getUptimeString(long uptime) {

        long days = TimeUnit.MILLISECONDS.toDays(uptime);
        long hours = TimeUnit.MILLISECONDS.toHours(uptime) - (days * 24);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(uptime) - (TimeUnit.MILLISECONDS.toHours(uptime) * 60);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(uptime) - (TimeUnit.MILLISECONDS.toMinutes(uptime) * 60);

        StringBuilder message = new StringBuilder();

        if (days > 0) {

            message.append(textManager.getAltColor()).append(days).append(" days").append(textManager.getMainColor()).append(", ");

        }

        if (hours > 0) {

            message.append(textManager.getAltColor()).append(hours).append(" hours").append(textManager.getMainColor()).append(", ");

        }

        if (minutes > 0) {

            message.append(textManager.getAltColor()).append(minutes).append(" minutes").append(textManager.getMainColor()).append(", ");

        }

        if (seconds > 0) {

            message.append(textManager.getAltColor()).append(seconds).append(" seconds").append(textManager.getMainColor()).append(", ");

        }

        if (message.length() > 0) {

            return message.substring(0, message.length() - 4);

        } else {

            return textManager.getAltColor() + "0 seconds";

        }

    }

    private ChatColor getTpsColor(double tps) {

        if (tps > 18.0D) {

            return ChatColor.GREEN;

        } else if (tps > 16.0D) {

            return ChatColor.YELLOW;

        } else {

            return ChatColor.RED;

        }

    }

}
