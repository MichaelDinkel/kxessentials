package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpaCancel implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    public CommandTpaCancel(TextManager textManager, TeleportManager teleportManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            teleportManager.removeAllRequests((Player) sender);

            sender.sendMessage(textManager.getMainColor() + "Cancelled all teleport requests.");

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
