package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.AliasManager;
import com.kaixeleron.essentials.manager.PriceManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.math.BigDecimal;

public class CommandWorth implements CommandExecutor {

    private final TextManager textManager;

    private final PriceManager priceManager;

    private final AliasManager aliasManager;

    public CommandWorth(TextManager textManager, PriceManager priceManager, AliasManager aliasManager) {

        this.textManager = textManager;

        this.priceManager = priceManager;

        this.aliasManager = aliasManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                switch (args[0].toLowerCase()) {

                    case "hand": {

                        ItemStack i = ((Player) sender).getInventory().getItemInMainHand();

                        Double price = priceManager.getPrice(i.getType());

                        if (price == null) {

                            sender.sendMessage(textManager.getAltColor() + i.getType().toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + " has no price.");

                        } else {

                            sender.sendMessage(textManager.getMainColor() + "Price of " + textManager.getAltColor() + i.getType().toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + ": " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(price).stripTrailingZeros().toPlainString());

                        }

                        break;

                    }

                    case "all": {

                        double total = 0.0D;

                        for (ItemStack i : ((Player) sender).getInventory().getContents()) {

                            if (i != null) {

                                Double price = priceManager.getPrice(i.getType());

                                if (price != null) {

                                    total = total + price;

                                }

                            }

                        }

                        if (total == 0.0D) {

                            sender.sendMessage(textManager.getMainColor() + "No items in your inventory have a price.");

                        } else {

                            sender.sendMessage(textManager.getMainColor() + "Price of your inventory: " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(total).stripTrailingZeros().toPlainString());

                        }

                        break;

                    }

                    default: {

                        Material type = null;

                        for (Material m : Material.values()) {

                            if (m.toString().replace("_", "").equalsIgnoreCase(args[0])) {

                                type = m;
                                break;

                            }

                        }

                        if (type == null) {

                            type = aliasManager.getByAlias(args[0].toLowerCase());

                        }

                        if (type == null) {

                            sender.sendMessage(textManager.getErrorColor() + "Item type not found.");

                        } else {

                            Double price = priceManager.getPrice(type);

                            if (price == null) {

                                sender.sendMessage(textManager.getAltColor() + type.toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + " has no price.");

                            } else {

                                sender.sendMessage(textManager.getMainColor() + "Price of " + textManager.getAltColor() + type.toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + ": " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(price).stripTrailingZeros().toPlainString());

                            }

                        }

                        break;

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
