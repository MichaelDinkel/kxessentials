package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.TextManager;
import com.kaixeleron.essentials.manager.WarpManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;

public class CommandDelWarp implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final WarpManager warpManager;

    private final Map<CommandSender, String> deleting;

    public CommandDelWarp(EssentialsMain m, TextManager textManager, WarpManager warpManager) {

        this.m = m;

        this.textManager = textManager;

        this.warpManager = warpManager;

        deleting = new HashMap<>();

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else {

            if (warpManager.listWarps().contains(args[0])) {

                if (deleting.containsKey(sender) && deleting.get(sender).equals(args[0])) {

                    Bukkit.getPluginManager().removePermission(String.format("kxessentials.warp.%s", args[0]));

                    warpManager.deleteWarp(args[0]);

                    deleting.remove(sender);

                    sender.sendMessage(textManager.getMainColor() + "Warp " + textManager.getAltColor() + args[0] + textManager.getMainColor() + " deleted.");

                } else {

                    deleting.put(sender, args[0]);

                    sender.sendMessage(textManager.getMainColor() + "Are you sure you want to delete the warp " + textManager.getAltColor() + args[0] + textManager.getMainColor() + "? Type " + textManager.getAltColor() + "/" + label + " " + args[0] + textManager.getMainColor() + " again to confirm.");

                    new BukkitRunnable() {

                        @Override
                        public void run() {

                            deleting.remove(sender);

                        }

                    }.runTaskLater(m, 200L);

                }

            } else {

                sender.sendMessage(textManager.getErrorColor() + "Warp " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + " does not exist.");

            }

        }

        return true;

    }

}
