package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSpawn implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    private final PlayerDataManager playerDataManager;

    public CommandSpawn(TextManager textManager, TeleportManager teleportManager, PlayerDataManager playerDataManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.spawn.other")) {

            if (sender instanceof Player) {

                Location spawn = teleportManager.getSpawn();

                if (spawn == null) spawn = Bukkit.getWorlds().get(0).getSpawnLocation();

                spawn = spawn.clone().add(0.5D, 0.0D, 0.5D);

                sender.sendMessage(textManager.getMainColor() + "Teleporting to spawn...");
                teleportManager.teleport((Player) sender, spawn);

            } else {

                if (sender.hasPermission("kxessentials.command.spawn.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            if (p.isOnline()) {

                Location spawn = teleportManager.getSpawn();

                if (spawn == null) spawn = Bukkit.getWorlds().get(0).getSpawnLocation();

                spawn = spawn.clone().add(0.5D, 0.0D, 0.5D);

                p.getPlayer().teleport(spawn);

                sender.sendMessage(textManager.getMainColor() + "Teleporting " + textManager.getAltColor() + p.getPlayer().getName() + textManager.getMainColor() + " to spawn...");

            } else {

                boolean queued = !playerDataManager.isRespawnOnLoginSet(p.getUniqueId());

                playerDataManager.setLoginRespawn(p.getUniqueId(), queued);

                if (queued) {

                    sender.sendMessage(textManager.getMainColor() + "Teleporting " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " to spawn the next time they log in.");

                } else {

                    sender.sendMessage(textManager.getMainColor() + "No longer teleporting " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " to spawn the next time they log in.");

                }

            }

        }

        return true;

    }

}
