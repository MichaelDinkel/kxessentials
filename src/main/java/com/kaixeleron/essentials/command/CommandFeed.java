package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandFeed implements CommandExecutor {

    private final TextManager textManager;

    public CommandFeed(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.feed.other")) {

            if (sender instanceof Player) {

                ((Player) sender).setFoodLevel(20);
                sender.sendMessage(textManager.getMainColor() + "You have been fed.");

            } else {

                if (sender.hasPermission("kxessentials.command.feed.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                p.setFoodLevel(20);

                p.sendMessage(textManager.getMainColor() + "You have been fed.");
                if (!(sender instanceof Player) || !p.equals((Player) sender)) sender.sendMessage(textManager.getMainColor() + "Fed " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

            }

        }

        return true;

    }

}
