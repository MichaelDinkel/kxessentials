package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandWorld implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    public CommandWorld(TextManager textManager, TeleportManager teleportManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());
                sender.sendMessage(generateWorldsList());

            } else {

                World w = Bukkit.getWorld(args[0]);

                if (w == null) {

                    sender.sendMessage(textManager.getErrorColor() + "World not found.");

                } else if (((Player) sender).getWorld().equals(w)) {

                    sender.sendMessage(textManager.getErrorColor() + "You are already in world " + textManager.getMainColor() + w.getName() + textManager.getErrorColor() + ".");

                } else {

                    sender.sendMessage(textManager.getMainColor() + "Teleporting to world " + textManager.getAltColor() + w.getName() + textManager.getMainColor() + "...");

                    teleportManager.teleport((Player) sender, w.getSpawnLocation());

                }

            }

        } else {

            sender.sendMessage(generateWorldsList());

        }

        return true;

    }

    private String generateWorldsList() {

        StringBuilder worlds = new StringBuilder();
        worlds.append(textManager.getMainColor()).append("Active worlds: ");

        for (World w : Bukkit.getWorlds()) {

            worlds.append(textManager.getAltColor());

            switch (w.getEnvironment()) {

                case NORMAL:

                    worlds.append("Overworld ");

                    break;

                case NETHER:

                    worlds.append("Nether ");

                    break;

                case THE_END:

                    worlds.append("End ");

                    break;

                default:

                    String environment = w.getEnvironment().toString().toLowerCase().replace('_', ' ');
                    environment = environment.substring(0, 1).toUpperCase() + environment.substring(1).toLowerCase();

                    worlds.append(environment).append(" ");

                    break;

            }

            worlds.append(w.getName()).append(textManager.getMainColor()).append(", ");

        }

        return worlds.substring(0, worlds.length() - 4);

    }

}
