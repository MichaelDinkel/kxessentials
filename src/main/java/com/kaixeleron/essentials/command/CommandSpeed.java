package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CommandSpeed implements CommandExecutor {

    private final TextManager textManager;

    public CommandSpeed(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            if (sender instanceof Player || sender.hasPermission("kxessentials.command.speed.other")) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

            }

        } else if (args.length == 1) {

            if (sender instanceof Player) {

                float speed = -11.0F;

                try {

                    speed = Float.parseFloat(args[0]);

                    if (speed > 10.0F) {

                        speed = 10.0F;

                    } else if (speed < -10.0F) {

                        speed = -10.0F;

                    }

                } catch (NumberFormatException ignored) {}

                if (speed == -11.0F) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid speed. Must be an integer.");

                } else {

                    if ((((Player) sender).isFlying() || label.equalsIgnoreCase("flyspeed")) && !label.equalsIgnoreCase("walkspeed")) {

                        ((Player) sender).setFlySpeed(speed / 10.0F);
                        sender.sendMessage(textManager.getMainColor() + "Fly speed set to " + textManager.getAltColor() + new BigDecimal(speed).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                    } else {

                        ((Player) sender).setWalkSpeed(speed / 10.0F);
                        sender.sendMessage(textManager.getMainColor() + "Walk speed set to " + textManager.getAltColor() + new BigDecimal(speed).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                    }

                }

            } else {

                if (sender.hasPermission("kxessentials.command.speed.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else if (args.length == 2 || !sender.hasPermission("kxessentials.command.speed.other")) {

            if (sender instanceof Player) {

                float speed = -11.0F;

                try {

                    speed = Float.parseFloat(args[1]);

                    if (speed > 10.0F) {

                        speed = 10.0F;

                    } else if (speed < -10.0F) {

                        speed = -10.0F;

                    }

                } catch (NumberFormatException ignored) {}

                if (speed == -11.0F) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid speed. Must be an integer.");

                } else {

                    if ((((Player) sender).isFlying() || label.equalsIgnoreCase("flyspeed") || args[0].equalsIgnoreCase("fly")) && !label.equalsIgnoreCase("walkspeed") && !args[0].equalsIgnoreCase("walk")) {

                        ((Player) sender).setFlySpeed(speed / 10.0F);
                        sender.sendMessage(textManager.getMainColor() + "Fly speed set to " + textManager.getAltColor() + new BigDecimal(speed).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                    } else if (!((Player) sender).isFlying() || label.equalsIgnoreCase("walkspeed") || args[0].equalsIgnoreCase("walk")) {

                        ((Player) sender).setWalkSpeed(speed / 10.0F);
                        sender.sendMessage(textManager.getMainColor() + "Walk speed set to " + textManager.getAltColor() + new BigDecimal(speed).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "Invalid speed type. Must be " + textManager.getMainColor() + "fly" + textManager.getErrorColor() + " or " + textManager.getMainColor() + "walk" + textManager.getErrorColor() + ".");

                    }

                }

            } else {

                if (sender.hasPermission("kxessentials.command.speed.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[2]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                float speed = -11.0F;

                try {

                    speed = Float.parseFloat(args[1]);

                    if (speed > 10.0F) {

                        speed = 10.0F;

                    } else if (speed < -10) {

                        speed = -10.0F;

                    }

                } catch (NumberFormatException ignored) {}

                if (speed == -11.0F) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid speed. Must be an integer.");

                } else {

                    if ((p.isFlying() || label.equalsIgnoreCase("flyspeed") || args[0].equalsIgnoreCase("fly")) && !label.equalsIgnoreCase("walkspeed") && !args[0].equalsIgnoreCase("walk")) {

                        p.setFlySpeed(speed / 10.0F);
                        sender.sendMessage(textManager.getMainColor() + "Fly speed set to " + textManager.getAltColor() + new BigDecimal(speed).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString() + textManager.getMainColor() + " on " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

                    } else if (!p.isFlying() || label.equalsIgnoreCase("walkspeed") || args[0].equalsIgnoreCase("walk")) {

                        p.setWalkSpeed(speed / 10.0F);
                        sender.sendMessage(textManager.getMainColor() + "Walk speed set to " + textManager.getAltColor() + new BigDecimal(speed).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString() + textManager.getMainColor() + " on " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "Invalid speed type. Must be " + textManager.getMainColor() + "fly" + textManager.getErrorColor() + " or " + textManager.getMainColor() + "walk" + textManager.getErrorColor() + ".");

                    }

                }

            }

        }

        return true;

    }

}
