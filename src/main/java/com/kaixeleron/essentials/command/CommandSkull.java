package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.OfflinePlayerManager;
import com.kaixeleron.essentials.manager.TextManager;
import com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class CommandSkull implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final OfflinePlayerManager offlinePlayerManager;

    public CommandSkull(EssentialsMain m, TextManager textManager, OfflinePlayerManager offlinePlayerManager) {

        this.m = m;

        this.textManager = textManager;

        this.offlinePlayerManager = offlinePlayerManager;

    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(final CommandSender sender, Command cmd, String label, final String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                new BukkitRunnable() {

                    @Override
                    public void run() {

                        final GameProfile profile = offlinePlayerManager.getOfflineProfile(args[0]);

                        new BukkitRunnable() {

                            @Override
                            public void run() {

                                if (profile == null) {

                                    sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                                } else {

                                    ItemStack head = new ItemStack(Material.PLAYER_HEAD);
                                    ItemMeta meta = head.getItemMeta();
                                    ((SkullMeta) meta).setOwningPlayer(Bukkit.getOfflinePlayer(profile.getId()));
                                    head.setItemMeta(meta);

                                    ((Player) sender).getInventory().addItem(head);

                                    sender.sendMessage(textManager.getMainColor() + "Spawned " + textManager.getAltColor() + profile.getName() + textManager.getMainColor() + "'s head.");

                                }

                            }

                        }.runTask(m);

                    }

                }.runTaskAsynchronously(m);

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
