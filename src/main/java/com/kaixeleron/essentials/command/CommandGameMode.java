package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandGameMode implements CommandExecutor {

    private final TextManager textManager;

    public CommandGameMode(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            if (label.equalsIgnoreCase("gamemode") || label.equalsIgnoreCase("gm") || !(sender instanceof Player)) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                switch (label.toLowerCase()) {

                    case "gms":
                    case "gm0":

                        if (sender.hasPermission("kxessentials.gamemode.survival")) {

                            ((Player) sender).setGameMode(GameMode.SURVIVAL);
                            sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "survival" + textManager.getMainColor() + " mode.");

                        }

                        break;

                    case "gmc":
                    case "gm1":

                        if (sender.hasPermission("kxessentials.gamemode.creative")) {

                            ((Player) sender).setGameMode(GameMode.CREATIVE);
                            sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "creative" + textManager.getMainColor() + " mode.");

                        } else {

                            sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                        }

                        break;

                    case "gma":
                    case "gm2":

                        if (sender.hasPermission("kxessentials.gamemode.adventure")) {

                            ((Player) sender).setGameMode(GameMode.ADVENTURE);
                            sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "adventure" + textManager.getMainColor() + " mode.");

                        } else {

                            sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                        }

                        break;

                    case "gmsp":
                    case "gm3":

                        if (sender.hasPermission("kxessentials.gamemode.spectator")) {

                            ((Player) sender).setGameMode(GameMode.SPECTATOR);
                            sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "spectator" + textManager.getMainColor() + " mode.");

                        } else {

                            sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                        }

                        break;

                    default:

                        break;

                }

            }

        } else if (args.length == 1) {

            switch (label.toLowerCase()) {

                case "gamemode":
                case "gm":

                    switch (args[0].toLowerCase()) {

                        case "survival":
                        case "s":
                        case "0":

                            if (sender.hasPermission("kxessentials.gamemode.survival")) {

                                ((Player) sender).setGameMode(GameMode.SURVIVAL);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "survival" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                            }

                            break;

                        case "creative":
                        case "c":
                        case "1":

                            if (sender.hasPermission("kxessentials.gamemode.creative")) {

                                ((Player) sender).setGameMode(GameMode.CREATIVE);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "creative" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                            }

                            break;

                        case "adventure":
                        case "a":
                        case "2":

                            if (sender.hasPermission("kxessentials.gamemode.adventure")) {

                                ((Player) sender).setGameMode(GameMode.ADVENTURE);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "adventure" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                            }

                            break;

                        case "spectator":
                        case "sp":
                        case "3":

                            if (sender.hasPermission("kxessentials.gamemode.spectator")) {

                                ((Player) sender).setGameMode(GameMode.SPECTATOR);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "spectator" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                            }

                            break;

                        default:

                            break;

                    }

                    break;

                case "gms":
                case "gm0":

                    if (sender.hasPermission("kxessentials.gamemode.survival")) {

                        if (sender.hasPermission("kxessentials.command.gamemode.other")) {

                            Player p = Bukkit.getPlayerExact(args[0]);

                            if (p == null) {

                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                            } else {

                                p.setGameMode(GameMode.SURVIVAL);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "survival" + textManager.getMainColor() + " mode.");

                            }

                        } else {

                            if (sender instanceof Player) {

                                ((Player) sender).setGameMode(GameMode.SURVIVAL);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "survival" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                            }

                        }

                    } else {

                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                    }

                    break;

                case "gmc":
                case "gm1":

                    if (sender.hasPermission("kxessentials.gamemode.creative")) {

                        if (sender.hasPermission("kxessentials.command.gamemode.other")) {

                            Player p = Bukkit.getPlayerExact(args[0]);

                            if (p == null) {

                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                            } else {

                                p.setGameMode(GameMode.CREATIVE);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "creative" + textManager.getMainColor() + " mode.");

                            }

                        } else {

                            if (sender instanceof Player) {

                                ((Player) sender).setGameMode(GameMode.CREATIVE);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "creative" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                            }

                        }

                    } else {

                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                    }

                    break;

                case "gma":
                case "gm2":

                    if (sender.hasPermission("kxessentials.gamemode.adventure")) {

                        if (sender.hasPermission("kxessentials.command.gamemode.other")) {

                            Player p = Bukkit.getPlayerExact(args[0]);

                            if (p == null) {

                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                            } else {

                                p.setGameMode(GameMode.ADVENTURE);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "adventure" + textManager.getMainColor() + " mode.");

                            }

                        } else {

                            if (sender instanceof Player) {

                                ((Player) sender).setGameMode(GameMode.ADVENTURE);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "adventure" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                            }

                        }

                    } else {

                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                    }

                    break;

                case "gmsp":
                case "gm3":

                    if (sender.hasPermission("kxessentials.gamemode.spectator")) {

                        if (sender.hasPermission("kxessentials.command.gamemode.other")) {

                            Player p = Bukkit.getPlayerExact(args[0]);

                            if (p == null) {

                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                            } else {

                                p.setGameMode(GameMode.SPECTATOR);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "spectator" + textManager.getMainColor() + " mode.");

                            }

                        } else {

                            if (sender instanceof Player) {

                                ((Player) sender).setGameMode(GameMode.SPECTATOR);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "spectator" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                            }

                        }

                    } else {

                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                    }

                    break;

                default:

                    break;

            }

        } else {

            switch (label.toLowerCase()) {

                case "gamemode":
                case "gm":

                    if (sender.hasPermission("kxessentials.command.gamemode.other")) {

                        Player p = Bukkit.getPlayerExact(args[1]);

                        if (p == null) {

                            sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                        } else {

                            switch (args[0].toLowerCase()) {

                                case "survival":
                                case "s":
                                case "0":

                                    if (sender.hasPermission("kxessentials.gamemode.survival")) {

                                        p.setGameMode(GameMode.SURVIVAL);
                                        sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "survival" + textManager.getMainColor() + " mode.");

                                    } else {

                                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                                    }

                                    break;

                                case "creative":
                                case "c":
                                case "1":

                                    if (sender.hasPermission("kxessentials.gamemode.creative")) {

                                        p.setGameMode(GameMode.CREATIVE);
                                        sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "creative" + textManager.getMainColor() + " mode.");

                                    } else {

                                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                                    }

                                    break;

                                case "adventure":
                                case "a":
                                case "2":

                                    if (sender.hasPermission("kxessentials.gamemode.adventure")) {

                                        p.setGameMode(GameMode.ADVENTURE);
                                        sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "adventure" + textManager.getMainColor() + " mode.");

                                    } else {

                                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                                    }

                                    break;

                                case "spectator":
                                case "sp":
                                case "3":

                                    if (sender.hasPermission("kxessentials.gamemode.spectator")) {

                                        p.setGameMode(GameMode.SPECTATOR);
                                        sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "spectator" + textManager.getMainColor() + " mode.");

                                    } else {

                                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                                    }

                                    break;

                                default:

                                    break;

                            }

                        }

                    } else {

                        if (sender instanceof Player) {

                            switch (args[0].toLowerCase()) {

                                case "survival":
                                case "s":
                                case "0":

                                    if (sender.hasPermission("kxessentials.gamemode.survival")) {

                                        ((Player) sender).setGameMode(GameMode.SURVIVAL);
                                        sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "survival" + textManager.getMainColor() + " mode.");

                                    } else {

                                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                                    }

                                    break;

                                case "creative":
                                case "c":
                                case "1":

                                    if (sender.hasPermission("kxessentials.gamemode.creative")) {

                                        ((Player) sender).setGameMode(GameMode.CREATIVE);
                                        sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "creative" + textManager.getMainColor() + " mode.");

                                    } else {

                                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                                    }

                                    break;

                                case "adventure":
                                case "a":
                                case "2":

                                    if (sender.hasPermission("kxessentials.gamemode.adventure")) {

                                        ((Player) sender).setGameMode(GameMode.ADVENTURE);
                                        sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "adventure" + textManager.getMainColor() + " mode.");

                                    } else {

                                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                                    }

                                    break;

                                case "spectator":
                                case "sp":
                                case "3":

                                    if (sender.hasPermission("kxessentials.gamemode.spectator")) {

                                        ((Player) sender).setGameMode(GameMode.SPECTATOR);
                                        sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "spectator" + textManager.getMainColor() + " mode.");

                                    } else {

                                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                                    }

                                    break;

                                default:

                                    break;

                            }

                        } else {

                            sender.sendMessage("This command can only be used by a player.");

                        }

                    }

                    break;

                case "gms":
                case "gm0":

                    if (sender.hasPermission("kxessentials.gamemode.survival")) {

                        if (sender.hasPermission("kxessentials.command.gamemode.other")) {

                            Player p = Bukkit.getPlayerExact(args[0]);

                            if (p == null) {

                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                            } else {

                                p.setGameMode(GameMode.SURVIVAL);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "survival" + textManager.getMainColor() + " mode.");

                            }

                        } else {

                            if (sender instanceof Player) {

                                ((Player) sender).setGameMode(GameMode.SURVIVAL);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "survival" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                            }

                        }

                    } else {

                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                    }

                    break;

                case "gmc":
                case "gm1":

                    if (sender.hasPermission("kxessentials.gamemode.creative")) {

                        if (sender.hasPermission("kxessentials.command.gamemode.other")) {

                            Player p = Bukkit.getPlayerExact(args[0]);

                            if (p == null) {

                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                            } else {

                                p.setGameMode(GameMode.CREATIVE);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "creative" + textManager.getMainColor() + " mode.");

                            }

                        } else {

                            if (sender instanceof Player) {

                                ((Player) sender).setGameMode(GameMode.CREATIVE);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "creative" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                            }

                        }

                    } else {

                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                    }

                    break;

                case "gma":
                case "gm2":

                    if (sender.hasPermission("kxessentials.gamemode.adventure")) {

                        if (sender.hasPermission("kxessentials.command.gamemode.other")) {

                            Player p = Bukkit.getPlayerExact(args[0]);

                            if (p == null) {

                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                            } else {

                                p.setGameMode(GameMode.ADVENTURE);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "adventure" + textManager.getMainColor() + " mode.");

                            }

                        } else {

                            if (sender instanceof Player) {

                                ((Player) sender).setGameMode(GameMode.ADVENTURE);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "adventure" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                            }

                        }

                    } else {

                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                    }

                    break;

                case "gmsp":
                case "gm3":

                    if (sender.hasPermission("kxessentials.gamemode.spectator")) {

                        if (sender.hasPermission("kxessentials.command.gamemode.other")) {

                            Player p = Bukkit.getPlayerExact(args[0]);

                            if (p == null) {

                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                            } else {

                                p.setGameMode(GameMode.SPECTATOR);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode of " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " set to " + textManager.getAltColor() + "spectator" + textManager.getMainColor() + " mode.");

                            }

                        } else {

                            if (sender instanceof Player) {

                                ((Player) sender).setGameMode(GameMode.SPECTATOR);
                                sender.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "spectator" + textManager.getMainColor() + " mode.");

                            } else {

                                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                            }

                        }

                    } else {

                        sender.sendMessage(ChatColor.RED + "You do not have access to that gamemode.");

                    }

                    break;

                default:

                    break;

            }

        }

        return true;

    }

}
