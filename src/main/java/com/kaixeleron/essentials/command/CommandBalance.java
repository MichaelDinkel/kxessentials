package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.OfflinePlayerManager;
import com.kaixeleron.essentials.manager.TextManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.math.BigDecimal;

public class CommandBalance implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final PlayerDataManager playerDataManager;

    private final OfflinePlayerManager offlinePlayerManager;

    public CommandBalance(EssentialsMain m, TextManager textManager, PlayerDataManager playerDataManager, OfflinePlayerManager offlinePlayerManager) {

        this.m = m;

        this.textManager = textManager;

        this.playerDataManager = playerDataManager;

        this.offlinePlayerManager = offlinePlayerManager;

    }

    @Override
    public boolean onCommand(final CommandSender sender, Command cmd, String label, final String[] args) {

        if (args.length == 0) {

            if (sender instanceof Player) {

                sender.sendMessage(textManager.getMainColor() + "Balance: " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(playerDataManager.getBalance((Player) sender)).stripTrailingZeros().toPlainString());

            } else {

                if (sender.hasPermission("kxessentials.command.balance.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else sender.sendMessage("This command can only be used by a player.");

            }

        } else {

            if (sender.hasPermission("kxessentials.command.balance.other")) {

                new BukkitRunnable() {

                    @Override
                    public void run() {

                        final GameProfile profile = offlinePlayerManager.getOfflineProfile(args[0]);

                        new BukkitRunnable() {

                            @Override
                            public void run() {

                                if (profile == null) {

                                    sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                                } else {

                                    double balance = playerDataManager.getBalance(Bukkit.getOfflinePlayer(profile.getId()));

                                    if (balance < 0.0D) {

                                        sender.sendMessage(textManager.getAltColor() + profile.getName() + textManager.getMainColor() + " has no balance.");

                                    } else {

                                        sender.sendMessage(textManager.getMainColor() + "Balance of " + profile.getName() + ": " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(balance).stripTrailingZeros().toPlainString());

                                    }

                                }

                            }

                        }.runTask(m);

                    }

                }.runTaskAsynchronously(m);

            } else {

                if (sender instanceof Player) {

                    sender.sendMessage(textManager.getMainColor() + "Balance: " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(playerDataManager.getBalance((Player) sender)).stripTrailingZeros().toPlainString());

                } else sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

            }

        }

        return true;

    }

}
