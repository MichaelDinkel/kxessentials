package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.AliasManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.PriceManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.math.BigDecimal;

public class CommandBuy implements CommandExecutor {

    private final TextManager textManager;

    private final PriceManager priceManager;

    private final PlayerDataManager playerDataManager;

    private final AliasManager aliasManager;

    public CommandBuy(TextManager textManager, PriceManager priceManager, PlayerDataManager playerDataManager, AliasManager aliasManager) {

        this.textManager = textManager;

        this.priceManager = priceManager;

        this.playerDataManager = playerDataManager;

        this.aliasManager = aliasManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length < 1) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                int amount = 1;

                if (args.length > 1) {

                    try {

                        amount = Integer.parseInt(args[1]);

                    } catch (NumberFormatException ignored) {}

                }

                if (amount < 1) amount = 1;

                Material type = null;

                for (Material m : Material.values()) {

                    if (m.toString().replace("_", "").equalsIgnoreCase(args[0])) {

                        type = m;

                        break;

                    }

                }

                if (type == null) {

                    type = aliasManager.getByAlias(args[0].toLowerCase());

                }

                if (type == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid item " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + ".");

                } else {

                    Double price = priceManager.getPrice(type);

                    if (price == null) {

                        sender.sendMessage(textManager.getErrorColor() + "That item is not priced.");

                    } else {

                        price = price * (double) amount;

                        double balance = playerDataManager.getBalance((Player) sender);

                        if (balance < price) {

                            sender.sendMessage(textManager.getErrorColor() + "You do not have enough money for this purchase.");

                        } else {

                            playerDataManager.setBalance((Player) sender, balance - price);

                            ((Player) sender).getInventory().addItem(new ItemStack(type, amount));

                            sender.sendMessage(textManager.getMainColor() + "Purchased " + textManager.getAltColor() + amount + " " + type.toString().toLowerCase().replace('_', ' ') + (amount == 1 ? "" : "s") + textManager.getMainColor() + " for " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(price).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                        }

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
