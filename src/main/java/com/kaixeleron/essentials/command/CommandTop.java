package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTop implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    public CommandTop(TextManager textManager, TeleportManager teleportManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Location l = ((Player) sender).getLocation().clone();

            Location found = null;

            for (int y = 255; y > 0; y--) {

                l.setY(y);

                if (!l.getBlock().isEmpty()) {

                    found = l.add(0.0D, 1.0D, 0.0D);

                    break;

                }

            }

            if (found == null || found.getBlockY() <= ((Player) sender).getLocation().getBlockY()) {

                sender.sendMessage(textManager.getErrorColor() + "There are no solid blocks above you.");

            } else {

                teleportManager.teleport((Player) sender, found);

                sender.sendMessage(textManager.getMainColor() + "Teleporting to top...");

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
