package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandPTime implements CommandExecutor {

    private final TextManager textManager;

    public CommandPTime(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            if (sender instanceof Player || sender.hasPermission("kxessentials.command.ptime.other")) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

            }

        } else if (args.length == 1 || !sender.hasPermission("kxessentials.command.ptime.other")) {

            if (sender instanceof Player) {

                long time = processTime(args[0]);

                if (time == -1L) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid time. Must be a positive integer, " + textManager.getMainColor() + "day" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "night" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "noon" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "midnight" + textManager.getErrorColor() + ", or " + textManager.getMainColor() + "reset" + textManager.getErrorColor() + ".");

                } else if (time == -2L) {

                    sender.sendMessage(textManager.getMainColor() + "Player time reset.");
                    ((Player) sender).resetPlayerTime();

                } else {

                    ((Player) sender).setPlayerTime(time, false);
                    sender.sendMessage(textManager.getMainColor() + "Player time set to " + textManager.getAltColor() + time + textManager.getMainColor() + ".");

                }

            } else {

                if (sender.hasPermission("kxessentials.command.ptime.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[1]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                long time = processTime(args[0]);

                if (time == -1L) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid time. Must be a positive integer, " + textManager.getMainColor() + "day" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "night" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "noon" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "midnight" + textManager.getErrorColor() + ", or " + textManager.getMainColor() + "reset" + textManager.getErrorColor() + ".");

                } else if (time == -2L) {

                    sender.sendMessage(textManager.getMainColor() + "Player time reset for " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");
                    p.resetPlayerTime();

                } else {

                    p.setPlayerTime(time, false);
                    sender.sendMessage(textManager.getMainColor() + "Player time set to " + textManager.getAltColor() + time + textManager.getMainColor() + " for " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

                }

            }

        }

        return true;

    }

    private long processTime(String timeString) {

        long time = -1L;

        switch (timeString.toLowerCase()) {

            case "day":

                time = 1000L;

                break;

            case "night":

                time = 13000L;

                break;

            case "noon":

                time = 6000L;

                break;

            case "midnight":

                time = 18000L;

                break;

            case "reset":

                time = -2L;

                break;

            default:

                try {

                    time = Long.parseLong(timeString);

                } catch (NumberFormatException ignored) {}

                break;

        }

        return time;

    }

}
