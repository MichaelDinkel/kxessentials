package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.MessageManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandMsgToggle implements CommandExecutor {

    private final TextManager textManager;

    private final MessageManager messageManager;

    private final PlayerDataManager playerDataManager;

    public CommandMsgToggle(TextManager textManager, MessageManager messageManager, PlayerDataManager playerDataManager) {

        this.textManager = textManager;

        this.messageManager = messageManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            boolean disabled = messageManager.isDisabled((Player) sender);

            messageManager.setDisabled((Player) sender, !disabled);
            playerDataManager.setMessagingDisabled((Player) sender, !disabled);

            sender.sendMessage(textManager.getMainColor() + "Messaging " + textManager.getAltColor() + (disabled ? "enabled" : "disabled") + textManager.getMainColor() + ".");

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
