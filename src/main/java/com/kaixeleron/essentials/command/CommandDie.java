package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class CommandDie implements CommandExecutor {

    private final TextManager textManager;

    public CommandDie(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            ((Player) sender).setLastDamageCause(new EntityDamageEvent((Player) sender, EntityDamageEvent.DamageCause.SUICIDE, ((Player) sender).getHealth()));
            ((Player) sender).setHealth(0.0D);

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
