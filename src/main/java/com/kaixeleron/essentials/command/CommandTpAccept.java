package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpAccept implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    public CommandTpAccept(TextManager textManager, TeleportManager teleportManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Player request = teleportManager.getTeleportRequest((Player) sender), hereRequest = teleportManager.getTeleportHereRequest((Player) sender);

            if (request != null) {

                teleportManager.teleport(request, ((Player) sender).getLocation());
                teleportManager.removeTeleportRequest((Player) sender);

                sender.sendMessage(textManager.getMainColor() + "Accepted " + textManager.getAltColor() + request.getName() + textManager.getMainColor() + "'s teleport request.");
                request.sendMessage(textManager.getAltColor() + sender.getName() + textManager.getMainColor() + " has accepted your teleport request.");

            } else if (hereRequest != null) {

                teleportManager.teleport((Player) sender, hereRequest.getLocation());
                teleportManager.removeTeleportHereRequest((Player) sender);

                sender.sendMessage(textManager.getMainColor() + "Accepted " + textManager.getAltColor() + hereRequest.getName() + textManager.getMainColor() + "'s teleport request.");
                hereRequest.sendMessage(textManager.getAltColor() + sender.getName() + textManager.getMainColor() + " has accepted your teleport request.");

            } else {

                sender.sendMessage(textManager.getErrorColor() + "You have no pending teleport request.");

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
