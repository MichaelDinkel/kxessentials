package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.MessageManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandMsg implements CommandExecutor {

    private final TextManager textManager;

    private final MessageManager messageManager;

    public CommandMsg(TextManager textManager, MessageManager messageManager) {

        this.textManager = textManager;
        this.messageManager = messageManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                if (messageManager.isDisabled(p)) {

                    sender.sendMessage(textManager.getAltColor() + p.getName() + textManager.getMainColor() + " has messaging disabled.");

                } else {

                    StringBuilder message = new StringBuilder();

                    for (int i = 1; i < args.length; i++) {

                        message.append(args[i]).append(" ");

                    }

                    sender.sendMessage(textManager.getAltColor() + "[Me -> " + p.getName() + "] " + textManager.getMainColor() + message.toString());

                    if (sender.hasPermission("kxessentials.messaging.bypassignore") || !(sender instanceof Player) || !messageManager.isIgnored(p, (Player) sender)) {

                        p.sendMessage(textManager.getAltColor() + "[" + sender.getName() + " -> Me] " + textManager.getMainColor() + message.toString());

                        if (sender instanceof Player) {

                            messageManager.dispatchSocialSpyMessage((Player) sender, p, message.toString());
                            messageManager.setReply((Player) sender, p);

                        }

                    }

                }

            }

        }

        return true;

    }

}
