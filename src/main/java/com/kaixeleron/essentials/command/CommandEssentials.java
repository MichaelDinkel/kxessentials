package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;

public class CommandEssentials implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final PlayerDataManager playerDataManager;

    private final Map<CommandSender, String> confirmingExport, confirmingImport;

    public CommandEssentials(EssentialsMain m, TextManager textManager, PlayerDataManager playerDataManager) {

        this.m = m;

        this.textManager = textManager;

        this.playerDataManager = playerDataManager;

        confirmingExport = new HashMap<>();
        confirmingImport = new HashMap<>();

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length > 0) {

            switch (args[0].toLowerCase()) {

                case "export":

                    exportDatabase(sender, args.length > 1 ? args[1] : null);

                    break;

                case "import":

                    importDatabase(sender, args.length > 1 ? args[1] : null);

                    break;

                case "reload":

                    reload(sender);

                    break;

                default:

                    help(sender);

                    break;

            }

        } else {

            help(sender);

        }

        return true;

    }

    private void exportDatabase(CommandSender sender, String target) {

        if (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender || sender.hasPermission("kxessentials.command.essentials.export")) {

            if (confirmingExport.containsKey(sender) && confirmingExport.get(sender).equalsIgnoreCase(target)) {

                confirmingExport.remove(sender);

                if (playerDataManager.exportToDatabase(target)) {

                    sender.sendMessage(textManager.getMainColor() + "Starting database export. " + (sender instanceof Player ? " Check the console for more details." : ""));

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "Could not start database export as the target database is invalid.");
                    sender.sendMessage(textManager.getErrorColor() + "Valid database types: " + textManager.getMainColor() + "yaml" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "sql" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "essentialsx" + textManager.getErrorColor() + ", or " + textManager.getMainColor() + "a custom database accessed by class path and name" + textManager.getErrorColor() + ".");

                }

            } else {

                confirmingExport.put(sender, target);

                sender.sendMessage(textManager.getMainColor() + "Are you sure you want to " + ChatColor.YELLOW + ChatColor.BOLD + "EXPORT" + textManager.getMainColor() + " all kxEssentials player data to database type " + textManager.getAltColor() + target + textManager.getMainColor() + "? Type " + textManager.getAltColor() + "/kxessentials export " + target + textManager.getMainColor() + " again to confirm.");
                sender.sendMessage(textManager.getMainColor() + "" + ChatColor.BOLD + "This process will overwrite all applicable data on the target database type.");
                sender.sendMessage(textManager.getMainColor() + "This process may take a very long time. The server must remain up until it finishes in order to have a complete export.");
                sender.sendMessage(textManager.getMainColor() + "This process should be done with no players online so all data is up to date once exported.");

                new BukkitRunnable() {

                    @Override
                    public void run() {

                        confirmingExport.remove(sender);

                    }

                }.runTaskLater(m, 200L);

            }

        } else {

            sender.sendMessage(textManager.getErrorColor() + "You do not have permission to export the database.");

        }

    }

    private void importDatabase(CommandSender sender, String target) {

        if (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender || sender.hasPermission("kxessentials.command.essentials.import")) {

            if (confirmingImport.containsKey(sender) && confirmingImport.get(sender).equalsIgnoreCase(target)) {

                confirmingImport.remove(sender);

                if (playerDataManager.importDatabase(target)) {

                    sender.sendMessage(textManager.getMainColor() + "Starting database import." + (sender instanceof Player ? " Check the console for more details." : ""));

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "Could not start database import as the target database is invalid.");
                    sender.sendMessage(textManager.getErrorColor() + "Valid database types: " + textManager.getMainColor() + "yaml" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "sql" + textManager.getErrorColor() + ", " + textManager.getMainColor() + "essentialsx" + textManager.getErrorColor() + ", or " + textManager.getMainColor() + "a custom database accessed by class path and name" + textManager.getErrorColor() + ".");

                }

            } else {

                confirmingImport.put(sender, target);

                sender.sendMessage(textManager.getMainColor() + "Are you sure you want to " + ChatColor.DARK_RED + ChatColor.BOLD + "IMPORT" + textManager.getMainColor() + " all kxEssentials player data from database type " + textManager.getAltColor() + target + textManager.getMainColor() + "? Type " + textManager.getAltColor() + "/kxessentials export " + target + textManager.getMainColor() + " again to confirm.");
                sender.sendMessage(textManager.getMainColor() + "" + ChatColor.BOLD + "This process will overwrite all applicable data on the target database type.");
                sender.sendMessage(textManager.getMainColor() + "This process may take a very long time. The server must remain up until it finishes in order to have a complete import.");
                sender.sendMessage(textManager.getMainColor() + "This process should be done with no players online so all data is up to date once imported.");

                new BukkitRunnable() {

                    @Override
                    public void run() {

                        confirmingImport.remove(sender);

                    }

                }.runTaskLater(m, 200L);

            }

        } else {

            sender.sendMessage(textManager.getErrorColor() + "You do not have permission to import a database.");

        }

    }

    private void reload(CommandSender sender) {

        m.reload();

        sender.sendMessage(textManager.getMainColor() + "kxEssentials reloaded.");

    }

    private void help(CommandSender sender) {

        sender.sendMessage(textManager.getAltColor() + m.getDescription().getName() + textManager.getMainColor() + " version " + textManager.getAltColor() + m.getDescription().getVersion() + textManager.getMainColor() + " by " + textManager.getAltColor() + m.getDescription().getAuthors().get(0));

        if (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender || sender.hasPermission("kxessentials.command.essentials.export")) sender.sendMessage(textManager.getAltColor() + "/essentials export <DatabaseType>" + textManager.getMainColor() + " - Export the kxEssentials database to another database type.");
        if (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender || sender.hasPermission("kxessentials.command.essentials.import")) sender.sendMessage(textManager.getAltColor() + "/essentials import <DatabaseType>" + textManager.getMainColor() + " - Import data from another database type.");
        if (sender.hasPermission("kxessentials.command.essentials.reload")) sender.sendMessage(textManager.getAltColor() + "/essentials reload" + textManager.getMainColor() + " - Reload kxEssentials.");

    }

}
