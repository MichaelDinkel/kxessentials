package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.OfflinePlayerManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Set;

public class CommandHome implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final OfflinePlayerManager offlinePlayerManager;

    private final PlayerDataManager playerDataManager;

    private final TeleportManager teleportManager;

    public CommandHome(EssentialsMain m, TextManager textManager, OfflinePlayerManager offlinePlayerManager, PlayerDataManager playerDataManager, TeleportManager teleportManager) {

        this.m = m;

        this.textManager = textManager;

        this.offlinePlayerManager = offlinePlayerManager;

        this.playerDataManager = playerDataManager;

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                StringBuilder homes = new StringBuilder();

                Set<String> keys = playerDataManager.listHomes((Player) sender);

                if (keys.size() > 0) {

                    homes.append(textManager.getMainColor()).append("Your homes: ");

                    for (String s : keys) {

                        homes.append(textManager.getAltColor()).append(s).append(textManager.getMainColor()).append(", ");

                    }

                }

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());
                if (homes.length() > 0) sender.sendMessage(homes.substring(0, homes.length() - 4));

            } else {

                if (args[0].contains(":")) {

                    if (sender.hasPermission("kxessentials.command.home.other")) {

                        final String[] split = args[0].split(":");

                        if (split.length == 1) {

                            new BukkitRunnable() {

                                @Override
                                public void run() {

                                    final GameProfile profile = offlinePlayerManager.getOfflineProfile(split[0]);

                                    new BukkitRunnable() {

                                        @Override
                                        public void run() {

                                            if (profile == null) {

                                                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                                            } else {

                                                StringBuilder homes = new StringBuilder();

                                                Set<String> keys = playerDataManager.listHomes(Bukkit.getOfflinePlayer(profile.getId()));

                                                if (keys.size() > 0) {

                                                    homes.append(textManager.getMainColor()).append("Homes of ").append(textManager.getAltColor()).append(profile.getName() == null ? split[0] : profile.getName()).append(textManager.getMainColor()).append(": ");

                                                    for (String s : keys) {

                                                        homes.append(textManager.getAltColor()).append(s).append(textManager.getMainColor()).append(", ");

                                                    }

                                                }

                                                if (homes.length() == 0) {

                                                    sender.sendMessage(textManager.getAltColor() + (profile.getName() == null ? split[0] : profile.getName()) + textManager.getMainColor() + " has no homes.");

                                                } else {

                                                    sender.sendMessage(homes.substring(0, homes.length() - 4));

                                                }

                                            }

                                        }

                                    }.runTask(m);

                                }

                            }.runTaskAsynchronously(m);

                        } else if (split.length > 1) {

                            new BukkitRunnable() {

                                @Override
                                public void run() {

                                    final GameProfile profile = offlinePlayerManager.getOfflineProfile(split[0]);

                                    new BukkitRunnable() {

                                        @Override
                                        public void run() {

                                            Location home = playerDataManager.getHome(Bukkit.getOfflinePlayer(profile.getId()), split[1]);

                                            if (home == null) {

                                                sender.sendMessage(textManager.getErrorColor() + "Home " + textManager.getMainColor() + split[1] + textManager.getErrorColor() + " does not exist.");

                                            } else {

                                                sender.sendMessage(textManager.getMainColor() + "Teleporting to " + textManager.getAltColor() + (profile.getName() == null ? split[0] : profile.getName()) + textManager.getMainColor() + "'s home: " + textManager.getAltColor() + split[1] + textManager.getMainColor() + "...");
                                                teleportManager.teleport((Player) sender, home);

                                            }

                                        }

                                    }.runTask(m);

                                }

                            }.runTaskAsynchronously(m);

                        } else {

                            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                        }

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "You do not have permission to access homes of other players.");

                    }

                } else {

                    Location home = playerDataManager.getHome((Player) sender, args[0]);

                    if (home == null) {

                        sender.sendMessage(textManager.getErrorColor() + "Home " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + " does not exist.");

                    } else {

                        sender.sendMessage(textManager.getMainColor() + "Teleporting to home: " + textManager.getAltColor() + args[0] + textManager.getMainColor() + "...");
                        teleportManager.teleport((Player) sender, home);

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}