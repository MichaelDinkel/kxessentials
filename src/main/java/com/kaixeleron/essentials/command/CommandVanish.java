package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import com.kaixeleron.essentials.manager.VanishManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandVanish implements CommandExecutor {

    private final TextManager textManager;

    private final VanishManager vanishManager;

    public CommandVanish(TextManager textManager, VanishManager vanishManager) {

        this.textManager = textManager;

        this.vanishManager = vanishManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.vanish.other")) {

            if (sender instanceof Player) {

                if (vanishManager.isVanished((Player) sender)) {

                    vanishManager.visible((Player) sender);

                    sender.sendMessage(textManager.getMainColor() + "You have become visible.");

                    for (Player p : Bukkit.getOnlinePlayers()) {

                        if (!p.equals(sender) && p.hasPermission("kxessentials.vanish.see")) {

                            p.sendMessage(textManager.getAltColor() + sender.getName() + textManager.getMainColor() + " has become visible.");

                        }

                    }

                } else {

                    vanishManager.vanish((Player) sender);

                    sender.sendMessage(textManager.getMainColor() + "You have vanished.");

                    for (Player p : Bukkit.getOnlinePlayers()) {

                        if (!p.equals(sender) && p.hasPermission("kxessentials.vanish.see")) {

                            p.sendMessage(textManager.getAltColor() + sender.getName() + textManager.getMainColor() + " has vanished.");

                        }

                    }

                }

            } else {

                if (sender.hasPermission("kxessentials.command.vanish.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                if (vanishManager.isVanished(p)) {

                    vanishManager.visible(p);

                    sender.sendMessage(textManager.getMainColor() + "You have made " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " visible.");

                    for (Player player : Bukkit.getOnlinePlayers()) {

                        if (sender instanceof Player && !player.equals((Player) sender)) {

                            if (p.hasPermission("kxessentials.vanish.see")) {

                                player.sendMessage(textManager.getAltColor() + p.getName() + textManager.getMainColor() + " has become visible.");

                            }

                        }

                    }

                } else {

                    vanishManager.vanish(p);

                    sender.sendMessage(textManager.getMainColor() + "You have vanished " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

                    for (Player player : Bukkit.getOnlinePlayers()) {

                        if (sender instanceof Player && !player.equals((Player) sender)) {

                            if (p.hasPermission("kxessentials.vanish.see")) {

                                player.sendMessage(textManager.getAltColor() + p.getName() + textManager.getMainColor() + " has vanished.");

                            }

                        }

                    }

                }

            }

        }

        return true;

    }

}
