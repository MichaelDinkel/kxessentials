package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import com.kaixeleron.essentials.manager.WarpManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandWarp implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    private final WarpManager warpManager;

    public CommandWarp(TextManager textManager, TeleportManager teleportManager, WarpManager warpManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

        this.warpManager = warpManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                StringBuilder warps = new StringBuilder();

                warps.append(textManager.getMainColor()).append("Available warps: ");

                for (String s : warpManager.listWarps()) {

                    if (sender.hasPermission(String.format("kxessentials.warp.%s", s))) {

                        warps.append(textManager.getAltColor()).append(s).append(textManager.getMainColor()).append(", ");

                    }

                }

                if (warps.length() == 19) {

                    sender.sendMessage(textManager.getErrorColor() + "You do not have access to any warps.");

                } else {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());
                    sender.sendMessage(warps.substring(0, warps.length() - 4));

                }

            } else {

                if (warpManager.listWarps().contains(args[0])) {

                    if (sender.hasPermission(String.format("kxessentials.warp.%s", args[0]))) {

                        sender.sendMessage(textManager.getMainColor() + "Teleporting to warp " + textManager.getAltColor() + args[0] + textManager.getMainColor() + "...");

                        teleportManager.teleport((Player) sender, warpManager.getWarp(args[0]));

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "You do not have access to that warp.");

                    }

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "Warp " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + " not found.");

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
