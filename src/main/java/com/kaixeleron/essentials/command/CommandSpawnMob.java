package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.util.Vector;

import java.util.List;

public class CommandSpawnMob implements CommandExecutor {

    private final TextManager textManager;

    private int maxDist, maxAmount;

    private final EntityType[] validTypes;

    public CommandSpawnMob(TextManager textManager, int maxDist, int maxAmount, List<String> blacklist) {

        this.textManager = textManager;

        this.maxDist = maxDist;
        this.maxAmount = maxAmount;

        EntityType[] types = EntityType.values();

        validTypes = new EntityType[types.length - blacklist.size()];

        int current = 0;

        Permission grandStar = Bukkit.getPluginManager().getPermission("kxessentials.*");

        Permission star = new Permission("kxessentials.spawnmob.*", PermissionDefault.FALSE);

        for (EntityType e : types) {

            if (!blacklist.contains(e.toString())) {

                validTypes[current] = e;

                Permission p = new Permission(String.format("kxessentials.spawnmob.%s", e.toString().toLowerCase().replace("_", "")), PermissionDefault.OP);
                p.addParent(star, true);

                Bukkit.getPluginManager().addPermission(p);

                current++;

            }

        }

        Bukkit.getPluginManager().addPermission(star);

        star.addParent(grandStar, true);

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                boolean any = false;

                StringBuilder list = new StringBuilder();
                list.append(textManager.getMainColor()).append("Available types: ");

                for (EntityType e : validTypes) {

                    String formatted = e.toString().toLowerCase().replace("_", "");

                    if (sender.hasPermission(String.format("kxessentials.spawnmob.%s", formatted))) {

                        list.append(textManager.getAltColor()).append(formatted).append(textManager.getMainColor()).append(", ");

                        any = true;

                    }

                }

                if (any) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());
                    sender.sendMessage(list.substring(0, list.length() - 4));

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "You do not have permission to spawn any entities.");

                }

            } else {

                EntityType type = null;

                String formatted = "";

                for (EntityType e : validTypes) {

                    formatted = e.toString().replace("_", "");

                    if (formatted.equalsIgnoreCase(args[0])) {

                        type = e;
                        break;

                    }

                }

                if (type == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid entity type " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + ".");

                } else if (!sender.hasPermission(String.format("kxessentials.spawnmob.%s", formatted.toLowerCase()))) {

                    sender.sendMessage(textManager.getErrorColor() + "You do not have permission to spawn that entity.");

                } else {

                    int amount = 1;

                    if (args.length > 1) {

                        try {

                            amount = Integer.parseInt(args[1]);

                            if (amount > maxAmount && !sender.hasPermission("kxessentials.command.spawnmob.unlimited")) {

                                amount = maxAmount;

                            } else if (amount < 1) {

                                amount = 1;

                            }

                        } catch (NumberFormatException ignored) {}

                    }

                    if (args.length > 2) {

                        Player p = Bukkit.getPlayerExact(args[2]);

                        if (p == null) {

                            sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                        } else {

                            Location l = p.getLocation();

                            for (int i = 0; i < amount; i++) {

                                l.getWorld().spawnEntity(l, type);

                            }

                            sender.sendMessage(textManager.getMainColor() + "Spawned " + textManager.getAltColor() + amount + " " + type.toString().toLowerCase().replace("_", "") + (amount != 1 ? "s" : "") + textManager.getMainColor() + " at " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + "'s location.");

                        }

                    } else {

                        Block target = null;

                        Location eyeLoc = ((Player) sender).getEyeLocation();

                        Vector direction = eyeLoc.getDirection();
                        direction.normalize();

                        for (int i = 0; i < maxDist; i++) {

                            eyeLoc.add(direction);

                            Block b = eyeLoc.getBlock();

                            target = b;

                            if (!b.getType().equals(Material.AIR)) {

                                break;

                            }

                        }

                        if (target == null) {

                            sender.sendMessage(textManager.getErrorColor() + "No block found in your line of sight.");

                        } else {

                            Location spawnpoint = target.getLocation().clone().add(0.0D, 1.0D, 0.0D);

                            for (int i = 0; i < amount; i++) {

                                spawnpoint.getWorld().spawnEntity(spawnpoint, type);

                            }

                            sender.sendMessage(textManager.getMainColor() + "Spawned " + textManager.getAltColor() + amount + " " + type.toString().toLowerCase().replace("_", "") + (amount != 1 ? "s" : "") + textManager.getMainColor() + ".");

                        }

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
