package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class CommandGetPos implements CommandExecutor {

    private final TextManager textManager;

    private final DecimalFormat format;

    public CommandGetPos(TextManager textManager) {

        this.textManager = textManager;

        format = new DecimalFormat("0.000");

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.getpos.other")) {

            if (sender instanceof Player) {

                StringBuilder message = new StringBuilder();

                message.append(textManager.getMainColor()).append("Currenet position: ");

                Location l = ((Player) sender).getLocation();

                message.append("x: ").append(textManager.getAltColor()).append(format.format(l.getX()));
                message.append(textManager.getMainColor()).append(" y: ").append(textManager.getAltColor()).append(format.format(l.getY()));
                message.append(textManager.getMainColor()).append(" z: ").append(textManager.getAltColor()).append(format.format(l.getZ()));
                message.append(textManager.getMainColor()).append(" yaw: ").append(textManager.getAltColor()).append(format.format(l.getYaw()));
                message.append(textManager.getMainColor()).append(" pitch: ").append(textManager.getAltColor()).append(format.format(l.getPitch()));

                sender.sendMessage(message.toString());

            } else {

                if (sender.hasPermission("kxessentials.command.getpos.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                StringBuilder message = new StringBuilder();

                message.append(textManager.getMainColor()).append("Currenet position of ").append(textManager.getAltColor()).append(p.getName()).append(textManager.getMainColor()).append(": ");

                Location l = p.getLocation();

                message.append("x: ").append(textManager.getAltColor()).append(format.format(l.getX()));
                message.append(textManager.getMainColor()).append(" y: ").append(textManager.getAltColor()).append(format.format(l.getY()));
                message.append(textManager.getMainColor()).append(" z: ").append(textManager.getAltColor()).append(format.format(l.getZ()));
                message.append(textManager.getMainColor()).append(" yaw: ").append(textManager.getAltColor()).append(format.format(l.getYaw()));
                message.append(textManager.getMainColor()).append(" pitch: ").append(textManager.getAltColor()).append(format.format(l.getPitch()));

                sender.sendMessage(message.toString());

            }

        }

        return true;

    }

}
