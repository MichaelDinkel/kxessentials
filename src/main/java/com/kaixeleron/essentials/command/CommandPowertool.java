package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandPowertool implements CommandExecutor {

    private final TextManager textManager;

    private final PlayerDataManager playerDataManager;

    public CommandPowertool(TextManager textManager, PlayerDataManager playerDataManager) {

        this.textManager = textManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                if (args[0].equalsIgnoreCase("clear")) {

                    Material m = ((Player) sender).getInventory().getItemInMainHand().getType();

                    playerDataManager.removePowertool((Player) sender, m);

                    sender.sendMessage(textManager.getMainColor() + "Powertool on " + textManager.getAltColor() + m.toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + " removed.");

                } else {

                    StringBuilder command = new StringBuilder();

                    for (String s : args) {

                        command.append(s).append(" ");

                    }

                    Material m = ((Player) sender).getInventory().getItemInMainHand().getType();

                    playerDataManager.setPowertool((Player) sender, m, command.substring(0, command.length() - 1));

                    sender.sendMessage(textManager.getMainColor() + "Powertool on " + textManager.getAltColor() + m.toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + " set to command: " + textManager.getAltColor() + command.toString());

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
