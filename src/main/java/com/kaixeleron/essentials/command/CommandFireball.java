package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkull;

public class CommandFireball implements CommandExecutor {

    private final TextManager textManager;

    public CommandFireball(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            String type = "fireball";

            if (args.length > 0) {

                if (args[0].equalsIgnoreCase("skull")) {

                    type = "skull";

                } else if (args[0].equalsIgnoreCase("blueskull")) {

                    type = "blueskull";

                }

            }

            switch (type) {

                case "fireball":

                    ((Player) sender).launchProjectile(Fireball.class);
                    ((Player) sender).getWorld().playSound(((Player) sender).getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1.0F, 1.0F);

                    break;

                case "skull":

                    ((Player) sender).launchProjectile(WitherSkull.class);
                    ((Player) sender).getWorld().playSound(((Player) sender).getLocation(), Sound.ENTITY_WITHER_SHOOT, 1.0F, 1.0F);

                    break;

                case "blueskull":

                    WitherSkull skull = ((Player) sender).launchProjectile(WitherSkull.class);
                    skull.setCharged(true);
                    ((Player) sender).getWorld().playSound(((Player) sender).getLocation(), Sound.ENTITY_WITHER_SHOOT, 1.0F, 1.0F);

                    break;

                default:

                    break;

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
