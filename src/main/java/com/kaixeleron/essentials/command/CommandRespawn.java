package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandRespawn implements CommandExecutor {

    private final TextManager textManager;

    public CommandRespawn(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                if (p.isDead()) {

                    p.spigot().respawn();

                    sender.sendMessage(textManager.getMainColor() + "Respawned " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

                } else {

                    sender.sendMessage(textManager.getMainColor() + p.getName() + textManager.getErrorColor() + " is not dead.");

                }

            }

        }

        return true;

    }

}
