package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.MessageManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandReply implements CommandExecutor {

    private final TextManager textManager;

    private final MessageManager messageManager;

    public CommandReply(TextManager textManager, MessageManager messageManager) {

        this.textManager = textManager;
        this.messageManager = messageManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length < 1) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                Player p = messageManager.getReply((Player) sender);

                if (p == null) {

                    sender.sendMessage(textManager.getErrorColor() + "You have nobody to reply to.");

                } else {

                    if (messageManager.isDisabled(p)) {

                        sender.sendMessage(textManager.getAltColor() + p.getName() + textManager.getMainColor() + " has messaging disabled.");

                    } else {

                        StringBuilder message = new StringBuilder();

                        for (String s : args) {

                            message.append(s).append(" ");

                        }

                        sender.sendMessage(textManager.getAltColor() + "[Me -> " + p.getName() + "] " + textManager.getMainColor() + message.toString());

                        if (sender.hasPermission("kxessentials.messaging.bypassignore") || !messageManager.isIgnored(p, (Player) sender)) {

                            p.sendMessage(textManager.getAltColor() + "[" + sender.getName() + " -> Me] " + textManager.getMainColor() + message.toString());

                            messageManager.dispatchSocialSpyMessage((Player) sender, p, message.toString());
                            messageManager.setReply((Player) sender, p);

                        }

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
