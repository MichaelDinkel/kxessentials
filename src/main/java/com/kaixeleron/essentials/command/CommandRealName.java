package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.NicknameManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandRealName implements CommandExecutor {

    private final TextManager textManager;

    private final NicknameManager nicknameManager;

    public CommandRealName(TextManager textManager, NicknameManager nicknameManager) {

        this.textManager = textManager;

        this.nicknameManager = nicknameManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else {

            String realname = nicknameManager.getRealName(args[0]);

            if (realname == null) {

                sender.sendMessage(textManager.getErrorColor() + "Nobody with that nickname is online.");

            } else {

                sender.sendMessage(textManager.getAltColor() + nicknameManager.getPrefix() + args[0] + textManager.getMainColor() + "'s real name is " + textManager.getAltColor() + realname + textManager.getMainColor() + ".");

            }

        }

        return true;

    }

}
