package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandChatAs implements CommandExecutor {

    private final TextManager textManager;

    public CommandChatAs(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                if (p.hasPermission("kxessentials.command.chatas.exempt")) {

                    sender.sendMessage(textManager.getErrorColor() + "You may not chat as that player.");

                } else {

                    StringBuilder message = new StringBuilder();

                    for (int i = 1; i < args.length; i++) {

                        message.append(args[i]).append(" ");

                    }

                    p.chat(message.substring(0, message.length() - 1));
                    sender.sendMessage(textManager.getMainColor() + "Message sent as " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

                }

            }

        }

        return true;

    }

}
