package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.AfkManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandAfk implements CommandExecutor {

    private final AfkManager manager;

    private final TextManager textManager;

    public CommandAfk(AfkManager manager, TextManager textManager) {

        this.manager = manager;

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Player p = (Player) sender;

            if (manager.isAfk(p)) {

                manager.setAfk(p, false);

            } else {

                manager.setAfk(p, true);

            }

        } else sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        return true;

    }

}
