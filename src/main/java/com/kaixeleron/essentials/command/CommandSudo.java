package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSudo implements CommandExecutor {

    private final TextManager textManager;

    public CommandSudo(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                if (p.hasPermission("kxessentials.command.sudo.exempt")) {

                    sender.sendMessage(textManager.getErrorColor() + "You may not sudo that player.");

                } else {

                    StringBuilder command = new StringBuilder();

                    for (int i = 1; i < args.length; i++) {

                        command.append(args[i]).append(" ");

                    }

                    String trimmed = command.substring(0, command.length() - 1);

                    Bukkit.dispatchCommand(p, trimmed);

                    sender.sendMessage(textManager.getMainColor() + "Sudoed " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " with command " + textManager.getAltColor() + trimmed + textManager.getMainColor() + ".");

                }

            }

        }

        return true;

    }

}
