package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.AliasManager;
import com.kaixeleron.essentials.manager.PriceManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigDecimal;

public class CommandSetWorth implements CommandExecutor {

    private final TextManager textManager;

    private final PriceManager priceManager;

    private final AliasManager aliasManager;

    public CommandSetWorth(TextManager textManager, PriceManager priceManager, AliasManager aliasManager) {

        this.textManager = textManager;

        this.priceManager = priceManager;

        this.aliasManager = aliasManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else if (args.length == 1) {

            if (sender instanceof Player) {

                double amount = -1.0D;

                try {

                    amount = Double.parseDouble(args[0]);

                } catch (NumberFormatException ignored) {}

                if (amount == -1.0D) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid price. Must be a positive decimal.");

                } else {

                    Material m = ((Player) sender).getInventory().getItemInMainHand().getType();

                    if (amount == 0.0D) {

                        priceManager.removePrice(m);

                        sender.sendMessage(textManager.getMainColor() + "Price of " + textManager.getAltColor() + m.toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + " removed.");

                    } else {

                        priceManager.setPrice(m, amount);

                        sender.sendMessage(textManager.getMainColor() + "Price of " + textManager.getAltColor() + m.toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + " set to " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(amount).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                    }

                }

            } else {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            }

        } else {

            double amount = -1.0D;

            try {

                amount = Double.parseDouble(args[1]);

            } catch (NumberFormatException ignored) {}

            if (amount == -1.0D) {

                sender.sendMessage(textManager.getErrorColor() + "Invalid price. Must be a positive decimal.");

            } else {

                Material m = null;

                for (Material mat : Material.values()) {

                    if (mat.toString().replace("_", "").equalsIgnoreCase(args[0])) {

                        m = mat;
                        break;

                    }

                }

                if (m == null) {

                    m = aliasManager.getByAlias(args[0].toLowerCase());

                }

                if (m == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Invalid material " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + ".");

                } else {

                    if (amount == 0.0D) {

                        priceManager.removePrice(m);

                        sender.sendMessage(textManager.getMainColor() + "Price of " + textManager.getAltColor() + m.toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + " removed.");

                    } else {

                        priceManager.setPrice(m, amount);

                        sender.sendMessage(textManager.getMainColor() + "Price of " + textManager.getAltColor() + m.toString().toLowerCase().replace('_', ' ') + textManager.getMainColor() + " set to " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(amount).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                    }

                }

            }

        }

        return true;

    }
}
