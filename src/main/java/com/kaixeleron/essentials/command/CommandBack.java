package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.BackManager;
import com.kaixeleron.essentials.manager.TextManager;
import com.kaixeleron.essentials.manager.TeleportManager;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandBack implements CommandExecutor {

    private final BackManager manager;

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    public CommandBack(BackManager manager, TextManager textManager, TeleportManager teleportManager) {

        this.manager = manager;

        this.textManager = textManager;

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Location l = manager.getBackLocation((Player) sender);

            if (l == null) {

                sender.sendMessage(textManager.getMainColor() + "You have no back location.");

            } else {

                sender.sendMessage(textManager.getMainColor() + "Teleporting to previous location...");
                teleportManager.teleport((Player) sender, l);

            }

        } else sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        return true;

    }

}
