package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.MessageManager;
import com.kaixeleron.essentials.manager.OfflinePlayerManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class CommandIgnore implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final MessageManager messageManager;

    private final PlayerDataManager playerDataManager;

    private final OfflinePlayerManager offlinePlayerManager;

    public CommandIgnore(EssentialsMain m, TextManager textManager, MessageManager messageManager, PlayerDataManager playerDataManager, OfflinePlayerManager offlinePlayerManager) {

        this.m = m;

        this.textManager = textManager;

        this.messageManager = messageManager;

        this.playerDataManager = playerDataManager;

        this.offlinePlayerManager = offlinePlayerManager;

    }

    @Override
    public boolean onCommand(final CommandSender sender, Command cmd, String label, final String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                if (args[0].equalsIgnoreCase(sender.getName())) {

                    sender.sendMessage(textManager.getMainColor() + "You cannot ignore yourself.");

                } else {

                    new BukkitRunnable() {

                        @Override
                        public void run() {

                            final GameProfile profile = offlinePlayerManager.getOfflineProfile(args[0]);

                            new BukkitRunnable() {

                                @Override
                                public void run() {

                                    if (profile == null) {

                                        sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                                    } else {

                                        OfflinePlayer target = Bukkit.getOfflinePlayer(profile.getId());

                                        if (playerDataManager.isIgnored((Player) sender, target)) {

                                            if (target.isOnline()) messageManager.removeIgnored((Player) sender, target.getPlayer());
                                            playerDataManager.removeIgnored((Player) sender, target);

                                            sender.sendMessage(textManager.getMainColor() + "Stopped ignoring " + textManager.getAltColor() + profile.getName() + textManager.getMainColor() + ".");

                                        } else {

                                            if (target.isOnline()) messageManager.addIgnored((Player) sender, target.getPlayer());
                                            playerDataManager.addIgnored((Player) sender, target);

                                            sender.sendMessage(textManager.getMainColor() + "Now ignoring " + textManager.getAltColor() + profile.getName() + textManager.getMainColor() + ".");

                                        }

                                    }

                                }

                            }.runTask(m);

                        }

                    }.runTaskAsynchronously(m);

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
