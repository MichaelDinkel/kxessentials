package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpaHere implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    public CommandTpaHere(TextManager textManager, TeleportManager teleportManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                Player p = Bukkit.getPlayerExact(args[0]);

                if (p == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                } else if (p.equals(sender)) {

                    sender.sendMessage(textManager.getErrorColor() + "You cannot request to teleport to yourself.");

                } else {

                    if (teleportManager.isDisabled(p)) {

                        sender.sendMessage(textManager.getAltColor() + p.getName() + textManager.getMainColor() + " has teleportation disabled.");

                    } else if (teleportManager.isAcceptingAll(p)) {

                        teleportManager.teleport(p, ((Player) sender).getLocation());

                        sender.sendMessage(textManager.getAltColor() + p.getName() + textManager.getMainColor() + " has accepted your teleport request.");
                        p.sendMessage(textManager.getMainColor() + "Accepted teleport request from " + textManager.getAltColor() + sender.getName() + textManager.getMainColor() + ".");

                    } else {

                        teleportManager.addTeleportHereRequest((Player) sender, p);

                        sender.sendMessage(textManager.getMainColor() + "Teleport request sent to " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");
                        p.sendMessage(textManager.getAltColor() + sender.getName() + textManager.getMainColor() + " has sent you a request to teleport to them. Type " + textManager.getAltColor() + "/tpaccept" + textManager.getMainColor() + " to accept or " + textManager.getAltColor() + "/tpdeny" + textManager.getMainColor() + " to deny.");

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
