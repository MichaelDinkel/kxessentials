package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandBurn implements CommandExecutor {

    private final TextManager textManager;

    public CommandBurn(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
            sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                int seconds = 10;

                if (args.length > 1) {

                    try {

                        seconds = Integer.parseInt(args[1]);

                        if (seconds < 0) seconds = 0;

                    } catch (NumberFormatException ignored) {}

                }

                p.setFireTicks(seconds * 20);

                sender.sendMessage(textManager.getMainColor() + "Set " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + " on fire for " + textManager.getAltColor() + seconds + textManager.getMainColor() + " seconds.");

            }

        }

        return true;

    }

}
