package com.kaixeleron.essentials.command;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.math.BigDecimal;

public class CommandPay implements CommandExecutor {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final PlayerDataManager playerDataManager;

    private final Table<Player, Player, Double> confirming;

    private boolean allowDecimals;

    public CommandPay(EssentialsMain m, TextManager textManager, PlayerDataManager playerDataManager, boolean allowDecimals) {

        this.m = m;

        this.textManager = textManager;

        this.playerDataManager = playerDataManager;

        confirming = HashBasedTable.create();

        this.allowDecimals = allowDecimals;

    }

    @Override
    public boolean onCommand(final CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length < 2) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                final Player p = Bukkit.getPlayerExact(args[0]);

                if (p == null) {

                    sender.sendMessage(textManager.getErrorColor() + "Player not found.");

                } else {

                    if (playerDataManager.isPayDisabled(p)) {

                        sender.sendMessage(textManager.getAltColor() + p.getName() + textManager.getMainColor() + " has payments disabled.");

                    } else {

                        double amount = -1.0D;

                        try {

                            amount = Double.parseDouble(args[1]);

                        } catch (NumberFormatException ignored) {}

                        if (amount <= 0.0D || (!allowDecimals && amount != (int) amount)) {

                            sender.sendMessage(textManager.getErrorColor() + "Invalid amount. Must be a positive " + (allowDecimals ? "decimal." : "integer."));

                        } else {

                            double changedBalance = playerDataManager.getBalance((Player) sender) - amount;

                            if (changedBalance > 0.0D) {

                                String amountStr = new BigDecimal(amount).stripTrailingZeros().toPlainString();

                                if (confirming.contains(sender, p) && confirming.get(sender, p) == amount) {

                                    confirming.remove(sender, p);

                                    playerDataManager.setBalance((Player) sender, changedBalance);
                                    playerDataManager.setBalance(p, playerDataManager.getBalance(p) + amount);

                                    sender.sendMessage(textManager.getMainColor() + "Sent " + textManager.getAltColor() + textManager.getCurrencySymbol() + amountStr + textManager.getMainColor() + " to " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");
                                    p.sendMessage(textManager.getMainColor() + "Received " + textManager.getAltColor() + textManager.getCurrencySymbol() + amountStr + textManager.getMainColor() + " from " + textManager.getAltColor() + sender.getName() + textManager.getMainColor() + ".");

                                } else {

                                    sender.sendMessage(textManager.getMainColor() + "Are you sure you want to send " + textManager.getAltColor() + textManager.getCurrencySymbol() + amountStr + textManager.getMainColor() + " to " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + "? Type " + textManager.getAltColor() + "/" + label + " " + p.getName() + " " + amountStr + textManager.getMainColor() + " again to confirm.");

                                    confirming.put((Player) sender, p, amount);

                                    new BukkitRunnable() {

                                        @Override
                                        public void run() {

                                            confirming.remove(sender, p);

                                        }

                                    }.runTaskLater(m, 200L);

                                }

                            } else {

                                sender.sendMessage(textManager.getErrorColor() + "You do not have sufficient funds to make that payment.");

                            }

                        }

                    }

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }
}
