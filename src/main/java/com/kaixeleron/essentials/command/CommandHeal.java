package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandHeal implements CommandExecutor {

    private final TextManager textManager;

    public CommandHeal(TextManager textManager) {

        this.textManager = textManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.heal.other")) {

            if (sender instanceof Player) {

                ((Player) sender).setHealth(((Player) sender).getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
                ((Player) sender).setFoodLevel(20);
                sender.sendMessage(textManager.getMainColor() + "You have been healed.");

            } else {

                if (sender.hasPermission("kxessentials.command.heal.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                p.setHealth(p.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
                p.setFoodLevel(20);

                p.sendMessage(textManager.getMainColor() + "You have been healed.");
                if (!(sender instanceof Player) || !p.equals(sender)) sender.sendMessage(textManager.getMainColor() + "Healed " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

            }

        }

        return true;

    }

}
