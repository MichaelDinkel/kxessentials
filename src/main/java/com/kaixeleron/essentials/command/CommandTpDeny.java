package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.TeleportManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTpDeny implements CommandExecutor {

    private final TextManager textManager;

    private final TeleportManager teleportManager;

    public CommandTpDeny(TextManager textManager, TeleportManager teleportManager) {

        this.textManager = textManager;

        this.teleportManager = teleportManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Player request = teleportManager.getTeleportRequest((Player) sender), hereRequest = teleportManager.getTeleportHereRequest((Player) sender);

            if (request != null) {

                teleportManager.removeTeleportRequest((Player) sender);

                sender.sendMessage(textManager.getMainColor() + "Denied " + textManager.getAltColor() + request.getName() + textManager.getMainColor() + "'s teleport request.");
                request.sendMessage(textManager.getAltColor() + sender.getName() + textManager.getMainColor() + " has denied your teleport request.");

            } else if (hereRequest != null) {

                teleportManager.removeTeleportHereRequest((Player) sender);

                sender.sendMessage(textManager.getMainColor() + "Denied " + textManager.getAltColor() + hereRequest.getName() + textManager.getMainColor() + "'s teleport request.");
                hereRequest.sendMessage(textManager.getAltColor() + sender.getName() + textManager.getMainColor() + " has denied your teleport request.");

            } else {

                sender.sendMessage(textManager.getErrorColor() + "You have no pending teleport request.");

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
