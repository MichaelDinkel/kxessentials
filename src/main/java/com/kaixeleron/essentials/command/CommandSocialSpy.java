package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.MessageManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSocialSpy implements CommandExecutor {

    private final TextManager textManager;

    private final MessageManager messageManager;

    private final PlayerDataManager playerDataManager;

    public CommandSocialSpy(TextManager textManager, MessageManager messageManager, PlayerDataManager playerDataManager) {

        this.textManager = textManager;

        this.messageManager = messageManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0 || !sender.hasPermission("kxessentials.command.socialspy.other")) {

            if (sender instanceof Player) {

                boolean spying = messageManager.isSpying((Player) sender);

                if (spying) {

                    messageManager.removeSocialSpy((Player) sender);
                    playerDataManager.setSocialSpy((Player) sender, false);

                    sender.sendMessage(textManager.getMainColor() + "SocialSpy " + textManager.getAltColor() + "disabled" + textManager.getMainColor() + ".");

                } else {

                    messageManager.addSocialSpy((Player) sender);
                    playerDataManager.setSocialSpy((Player) sender, true);

                    sender.sendMessage(textManager.getMainColor() + "SocialSpy " + textManager.getAltColor() + "enabled" + textManager.getMainColor() + ".");

                }

            } else {

                if (sender.hasPermission("kxessentials.command.socialspy.other")) {

                    sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                    sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

                } else {

                    sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

                }

            }

        } else {

            Player p = Bukkit.getPlayerExact(args[0]);

            if (p == null) {

                sender.sendMessage(textManager.getErrorColor() + "Player not found.");

            } else {

                if (p.hasPermission("kxessentials.command.socialspy")) {

                    boolean spying = messageManager.isSpying(p);

                    if (spying) {

                        messageManager.removeSocialSpy(p);
                        playerDataManager.setSocialSpy(p, false);

                        sender.sendMessage(textManager.getMainColor() + "SocialSpy " + textManager.getAltColor() + "disabled" + textManager.getMainColor() + " for " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

                    } else {

                        messageManager.addSocialSpy(p);
                        playerDataManager.setSocialSpy(p, true);

                        sender.sendMessage(textManager.getMainColor() + "SocialSpy " + textManager.getAltColor() + "enabled" + textManager.getMainColor() + " for " + textManager.getAltColor() + p.getName() + textManager.getMainColor() + ".");

                    }

                } else {

                    sender.sendMessage(textManager.getMainColor() + p.getName() + textManager.getErrorColor() + " does not have access to SocialSpy.");

                }

            }

        }

        return true;

    }

}
