package com.kaixeleron.essentials.command;

import com.kaixeleron.essentials.manager.KitManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandKit implements CommandExecutor {

    private final TextManager textManager;

    private final KitManager kitManager;

    private final PlayerDataManager playerDataManager;

    public CommandKit(TextManager textManager, KitManager kitManager, PlayerDataManager playerDataManager) {

        this.textManager = textManager;

        this.kitManager = kitManager;

        this.playerDataManager = playerDataManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length == 0) {

                sender.sendMessage(textManager.getMainColor() + cmd.getDescription());
                sender.sendMessage(textManager.getUsageColor() + "Usage: " + textManager.getMainColor() + cmd.getUsage());

            } else {

                if (kitManager.kitExists(args[0])) {

                    if (sender.hasPermission(String.format("kxessentials.kit.%s", args[0]))) {

                        long cooldown = playerDataManager.getKitCooldown((Player) sender, args[0]);

                        if (cooldown <= System.currentTimeMillis() || sender.hasPermission("kxessentials.command.kit.nocooldown")) {

                            ((Player) sender).getInventory().addItem(kitManager.getKit(args[0]));

                            long kitCooldown = kitManager.getKitCooldown(args[0]);

                            if (kitCooldown > 0L && !sender.hasPermission("kxessentials.command.kit.nocooldown")) {

                                playerDataManager.setKitCooldown((Player) sender, args[0], kitCooldown + System.currentTimeMillis());

                            }

                            sender.sendMessage(textManager.getMainColor() + "Spawned kit " + textManager.getAltColor() + args[0] + textManager.getMainColor() + ".");

                        } else {

                            sender.sendMessage(kitManager.generateCooldownString(cooldown - System.currentTimeMillis()));

                        }

                    } else {

                        sender.sendMessage(textManager.getErrorColor() + "You do not have permission to spawn that kit.");

                    }

                } else {

                    sender.sendMessage(textManager.getErrorColor() + "Kit " + textManager.getMainColor() + args[0] + textManager.getErrorColor() + " does not exist.");

                }

            }

        } else {

            sender.sendMessage(textManager.getMainColor() + "This command can only be used by a player.");

        }

        return true;

    }

}
