package com.kaixeleron.essentials;

import com.kaixeleron.essentials.command.*;
import com.kaixeleron.essentials.command.tabcomplete.EmptyTabComplete;
import com.kaixeleron.essentials.command.tabcomplete.TabCompleteBalance;
import com.kaixeleron.essentials.command.tabcomplete.TabCompleteBroadcast;
import com.kaixeleron.essentials.command.tabcomplete.TabCompleteBurn;
import com.kaixeleron.essentials.database.DatabaseException;
import com.kaixeleron.essentials.database.EssentialsDatabase;
import com.kaixeleron.essentials.database.SQLDatabase;
import com.kaixeleron.essentials.database.YAMLDatabase;
import com.kaixeleron.essentials.listener.*;
import com.kaixeleron.essentials.manager.*;
import com.kaixeleron.essentials.manager.SpawnerManager;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.InvocationTargetException;

public class EssentialsMain extends JavaPlugin {

    private EssentialsDatabase database = null;

    private AfkManager afkManager;
    private AliasManager aliasManager;
    private BackManager backManager;
    private GodManager godManager;
    private KitManager kitManager;
    private MessageManager messageManager;
    private NicknameManager nicknameManager;
    private OfflinePlayerManager offlinePlayerManager;
    private PlayerDataManager playerDataManager;
    private PriceManager priceManager;
    private SpawnerManager spawnerManager;
    private TeleportManager teleportManager;
    private TextManager textManager;
    private VanishManager vanishManager;
    private WarpManager warpManager;

    @Override
    public void onLoad() {

        saveDefaultConfig();

        textManager = new TextManager();

        loadTextColors();

        afkManager = new AfkManager(textManager);
        aliasManager = new AliasManager(getDataFolder());
        backManager = new BackManager(this);
        godManager = new GodManager();
        kitManager = new KitManager(getDataFolder(), textManager);
        nicknameManager = new NicknameManager(getConfig().getString("nickname.prefix"));
        offlinePlayerManager = new OfflinePlayerManager();
        priceManager = new PriceManager(getDataFolder());
        spawnerManager = new SpawnerManager(getConfig().getStringList("spawner.blacklisted"));
        teleportManager = new TeleportManager(this, getConfig().getInt("teleportation.delay"), backManager, textManager);
        vanishManager = new VanishManager(this, textManager);
        warpManager = new WarpManager(getDataFolder());

        messageManager = new MessageManager(textManager);

        playerDataManager = new PlayerDataManager(this, messageManager, nicknameManager, teleportManager, getConfig().getDouble("economy.defaultCash"), getConfig().getString("database.sql.hostname"), getConfig().getInt("database.sql.port"), getConfig().getString("database.sql.database"), getConfig().getString("database.sql.username"), getConfig().getString("database.sql.password"));

        loadDatabase();

        playerDataManager.setDatabase(database);

        messageManager.setPlayerDataManager(playerDataManager);

        spawnerManager.registerPermissions();

        //Reloaded
        for (Player p : getServer().getOnlinePlayers()) {

            playerDataManager.loadData(p);
            offlinePlayerManager.addCache(p.getName(), p.getUniqueId());

        }

    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onEnable() {

        kitManager.reload();
        priceManager.reload();
        warpManager.reload();
        warpManager.registerPerms();

        EmptyTabComplete emptyTabComplete = new EmptyTabComplete();

        getCommand("afk").setExecutor(new CommandAfk(afkManager, textManager));
        getCommand("afk").setTabCompleter(emptyTabComplete);
        getCommand("back").setExecutor(new CommandBack(backManager, textManager, teleportManager));
        getCommand("back").setTabCompleter(emptyTabComplete);
        getCommand("balance").setExecutor(new CommandBalance(this, textManager, playerDataManager, offlinePlayerManager));
        getCommand("balance").setTabCompleter(new TabCompleteBalance());
        getCommand("baltop").setExecutor(new CommandBalTop(textManager, playerDataManager));
        getCommand("baltop").setTabCompleter(emptyTabComplete);
        getCommand("broadcast").setExecutor(new CommandBroadcast(textManager));
        getCommand("broadcast").setTabCompleter(new TabCompleteBroadcast());
        getCommand("burn").setExecutor(new CommandBurn(textManager));
        getCommand("burn").setTabCompleter(new TabCompleteBurn());
        getCommand("buy").setExecutor(new CommandBuy(textManager, priceManager, playerDataManager, aliasManager));
        getCommand("buy").setTabCompleter(emptyTabComplete);
        getCommand("chatas").setExecutor(new CommandChatAs(textManager));
        getCommand("chatas").setTabCompleter(emptyTabComplete);
        getCommand("clearinventory").setExecutor(new CommandClearInventory(this, textManager));
        getCommand("clearinventory").setTabCompleter(emptyTabComplete);
        getCommand("createkit").setExecutor(new CommandCreateKit(textManager, kitManager));
        getCommand("createkit").setTabCompleter(emptyTabComplete);
        getCommand("delhome").setExecutor(new CommandDelHome(this, textManager, offlinePlayerManager, playerDataManager));
        getCommand("delhome").setTabCompleter(emptyTabComplete);
        getCommand("delkit").setExecutor(new CommandDelKit(textManager, kitManager));
        getCommand("delkit").setTabCompleter(emptyTabComplete);
        getCommand("delwarp").setExecutor(new CommandDelWarp(this, textManager, warpManager));
        getCommand("delwarp").setTabCompleter(emptyTabComplete);
        getCommand("die").setExecutor(new CommandDie(textManager));
        getCommand("die").setTabCompleter(emptyTabComplete);
        getCommand("disposal").setExecutor(new CommandDisposal(textManager));
        getCommand("disposal").setTabCompleter(emptyTabComplete);
        getCommand("economy").setExecutor(new CommandEconomy(this, textManager, offlinePlayerManager, playerDataManager));
        getCommand("economy").setTabCompleter(emptyTabComplete);
        getCommand("enchant").setExecutor(new CommandEnchant(textManager));
        getCommand("enchant").setTabCompleter(emptyTabComplete);
        getCommand("enderchest").setExecutor(new CommandEnderChest(textManager));
        getCommand("enderchest").setTabCompleter(emptyTabComplete);
        getCommand("essentials").setExecutor(new CommandEssentials(this, textManager, playerDataManager));
        getCommand("essentials").setTabCompleter(emptyTabComplete);
        getCommand("extinguish").setExecutor(new CommandExtinguish(textManager));
        getCommand("extinguish").setTabCompleter(emptyTabComplete);
        getCommand("feed").setExecutor(new CommandFeed(textManager));
        getCommand("feed").setTabCompleter(emptyTabComplete);
        getCommand("fly").setExecutor(new CommandFly(textManager));
        getCommand("fly").setTabCompleter(emptyTabComplete);
        getCommand("fireball").setExecutor(new CommandFireball(textManager));
        getCommand("fireball").setTabCompleter(emptyTabComplete);
        getCommand("gamemode").setExecutor(new CommandGameMode(textManager));
        getCommand("gamemode").setTabCompleter(emptyTabComplete);
        getCommand("gc").setExecutor(new CommandGc(textManager));
        getCommand("gc").setTabCompleter(emptyTabComplete);
        getCommand("getpos").setExecutor(new CommandGetPos(textManager));
        getCommand("getpos").setTabCompleter(emptyTabComplete);
        getCommand("god").setExecutor(new CommandGod(textManager, godManager));
        getCommand("god").setTabCompleter(emptyTabComplete);
        getCommand("hat").setExecutor(new CommandHat(textManager));
        getCommand("hat").setTabCompleter(emptyTabComplete);
        getCommand("heal").setExecutor(new CommandHeal(textManager));
        getCommand("heal").setTabCompleter(emptyTabComplete);
        getCommand("home").setExecutor(new CommandHome(this, textManager, offlinePlayerManager, playerDataManager, teleportManager));
        getCommand("home").setTabCompleter(emptyTabComplete);
        getCommand("ignore").setExecutor(new CommandIgnore(this, textManager, messageManager, playerDataManager, offlinePlayerManager));
        getCommand("ignore").setTabCompleter(emptyTabComplete);
        getCommand("invsee").setExecutor(new CommandInvSee(textManager));
        getCommand("invsee").setTabCompleter(emptyTabComplete);
        getCommand("item").setExecutor(new CommandItem(textManager, aliasManager));
        getCommand("item").setTabCompleter(emptyTabComplete);
        getCommand("jump").setExecutor(new CommandJump(textManager, teleportManager, getConfig().getInt("teleportation.jumpMaxDistance")));
        getCommand("jump").setTabCompleter(emptyTabComplete);
        getCommand("kickall").setExecutor(new CommandKickAll(textManager));
        getCommand("kickall").setTabCompleter(emptyTabComplete);
        getCommand("kit").setExecutor(new CommandKit(textManager, kitManager, playerDataManager));
        getCommand("kit").setTabCompleter(emptyTabComplete);
        getCommand("kittycannon").setExecutor(new CommandKittyCannon(this, textManager));
        getCommand("kittycannon").setTabCompleter(emptyTabComplete);
        getCommand("lightning").setExecutor(new CommandLightning(textManager, getConfig().getInt("lightning.maxDistance")));
        getCommand("lightning").setTabCompleter(emptyTabComplete);
        getCommand("maxhealth").setExecutor(new CommandMaxHealth(textManager));
        getCommand("maxhealth").setTabCompleter(emptyTabComplete);
        getCommand("msg").setExecutor(new CommandMsg(textManager, messageManager));
        getCommand("msg").setTabCompleter(emptyTabComplete);
        getCommand("msgtoggle").setExecutor(new CommandMsgToggle(textManager, messageManager, playerDataManager));
        getCommand("msgtoggle").setTabCompleter(emptyTabComplete);
        getCommand("near").setExecutor(new CommandNear(textManager, getConfig().getInt("near.maxDistance")));
        getCommand("near").setTabCompleter(emptyTabComplete);
        getCommand("nick").setExecutor(new CommandNick(textManager, nicknameManager, playerDataManager, getConfig().getInt("nickname.maxLength"), getConfig().getInt("nickname.minLength")));
        getCommand("nick").setTabCompleter(emptyTabComplete);
        getCommand("pay").setExecutor(new CommandPay(this, textManager, playerDataManager, getConfig().getBoolean("allowDecimals")));
        getCommand("pay").setTabCompleter(emptyTabComplete);
        getCommand("paytoggle").setExecutor(new CommandPayToggle(textManager, playerDataManager));
        getCommand("paytoggle").setTabCompleter(emptyTabComplete);
        getCommand("powertool").setExecutor(new CommandPowertool(textManager, playerDataManager));
        getCommand("powertool").setTabCompleter(emptyTabComplete);
        getCommand("ptime").setExecutor(new CommandPTime(textManager));
        getCommand("ptime").setTabCompleter(emptyTabComplete);
        getCommand("reply").setExecutor(new CommandReply(textManager, messageManager));
        getCommand("reply").setTabCompleter(emptyTabComplete);
        getCommand("respawn").setExecutor(new CommandRespawn(textManager));
        getCommand("respawn").setTabCompleter(emptyTabComplete);
        getCommand("realname").setExecutor(new CommandRealName(textManager, nicknameManager));
        getCommand("realname").setTabCompleter(emptyTabComplete);
        getCommand("remove").setExecutor(new CommandRemove(textManager));
        getCommand("remove").setTabCompleter(emptyTabComplete);
        getCommand("repair").setExecutor(new CommandRepair(textManager));
        getCommand("repair").setTabCompleter(emptyTabComplete);
        getCommand("sell").setExecutor(new CommandSell(textManager, priceManager, playerDataManager));
        getCommand("sell").setTabCompleter(emptyTabComplete);
        getCommand("sethome").setExecutor(new CommandSetHome(this, textManager, offlinePlayerManager, playerDataManager));
        getCommand("sethome").setTabCompleter(emptyTabComplete);
        getCommand("setwarp").setExecutor(new CommandSetWarp(textManager, warpManager));
        getCommand("setwarp").setTabCompleter(emptyTabComplete);
        getCommand("setspawn").setExecutor(new CommandSetSpawn(textManager, teleportManager));
        getCommand("setspawn").setTabCompleter(emptyTabComplete);
        getCommand("setworth").setExecutor(new CommandSetWorth(textManager, priceManager, aliasManager));
        getCommand("setworth").setTabCompleter(emptyTabComplete);
        getCommand("showkit").setExecutor(new CommandShowKit(textManager, kitManager));
        getCommand("showkit").setTabCompleter(emptyTabComplete);
        getCommand("skull").setExecutor(new CommandSkull(this, textManager, offlinePlayerManager));
        getCommand("skull").setTabCompleter(emptyTabComplete);
        getCommand("socialspy").setExecutor(new CommandSocialSpy(textManager, messageManager, playerDataManager));
        getCommand("socialspy").setTabCompleter(emptyTabComplete);
        getCommand("spawn").setExecutor(new CommandSpawn(textManager, teleportManager, playerDataManager));
        getCommand("spawn").setTabCompleter(emptyTabComplete);
        getCommand("spawner").setExecutor(new CommandSpawner(textManager, spawnerManager, getConfig().getInt("spawner.maxChangeDistance")));
        getCommand("spawner").setTabCompleter(emptyTabComplete);
        getCommand("spawnmob").setExecutor(new CommandSpawnMob(textManager, getConfig().getInt("spawnmob.maxDistance"), getConfig().getInt("spawnmob.maxAmount"), getConfig().getStringList("spawnmob.blacklisted")));
        getCommand("spawnmob").setTabCompleter(emptyTabComplete);
        getCommand("speed").setExecutor(new CommandSpeed(textManager));
        getCommand("speed").setTabCompleter(emptyTabComplete);
        getCommand("sudo").setExecutor(new CommandSudo(textManager));
        getCommand("sudo").setTabCompleter(emptyTabComplete);
        getCommand("top").setExecutor(new CommandTop(textManager, teleportManager));
        getCommand("top").setTabCompleter(emptyTabComplete);
        getCommand("tpa").setExecutor(new CommandTpa(textManager, teleportManager));
        getCommand("tpa").setTabCompleter(emptyTabComplete);
        getCommand("tpahere").setExecutor(new CommandTpaHere(textManager, teleportManager));
        getCommand("tpahere").setTabCompleter(emptyTabComplete);
        getCommand("tpaall").setExecutor(new CommandTpaAll(textManager, teleportManager));
        getCommand("tpaall").setTabCompleter(emptyTabComplete);
        getCommand("tpall").setExecutor(new CommandTpAll(textManager));
        getCommand("tpall").setTabCompleter(emptyTabComplete);
        getCommand("tpauto").setExecutor(new CommandTpAuto(textManager, teleportManager, playerDataManager));
        getCommand("tpauto").setTabCompleter(emptyTabComplete);
        getCommand("tpacancel").setExecutor(new CommandTpaCancel(textManager, teleportManager));
        getCommand("tpacancel").setTabCompleter(emptyTabComplete);
        getCommand("tpaccept").setExecutor(new CommandTpAccept(textManager, teleportManager));
        getCommand("tpaccept").setTabCompleter(emptyTabComplete);
        getCommand("tpdeny").setExecutor(new CommandTpDeny(textManager, teleportManager));
        getCommand("tpdeny").setTabCompleter(emptyTabComplete);
        getCommand("tpo").setExecutor(new CommandTpo(backManager, textManager));
        getCommand("tpo").setTabCompleter(emptyTabComplete);
        getCommand("tpohere").setExecutor(new CommandTpoHere(backManager, textManager));
        getCommand("tpohere").setTabCompleter(emptyTabComplete);
        getCommand("tppos").setExecutor(new CommandTpPos(teleportManager));
        getCommand("tppos").setTabCompleter(emptyTabComplete);
        getCommand("tptoggle").setExecutor(new CommandTpToggle(textManager, teleportManager, playerDataManager));
        getCommand("tptoggle").setTabCompleter(emptyTabComplete);
        getCommand("tree").setExecutor(new CommandTree(textManager, getConfig().getInt("tree.maxDistance")));
        getCommand("tree").setTabCompleter(emptyTabComplete);
        getCommand("vanish").setExecutor(new CommandVanish(textManager, vanishManager));
        getCommand("vanish").setTabCompleter(emptyTabComplete);
        getCommand("workbench").setExecutor(new CommandWorkbench());
        getCommand("workbench").setTabCompleter(emptyTabComplete);
        getCommand("warp").setExecutor(new CommandWarp(textManager, teleportManager, warpManager));
        getCommand("warp").setTabCompleter(emptyTabComplete);
        getCommand("world").setExecutor(new CommandWorld(textManager, teleportManager));
        getCommand("world").setTabCompleter(emptyTabComplete);
        getCommand("worth").setExecutor(new CommandWorth(textManager, priceManager, aliasManager));
        getCommand("worth").setTabCompleter(emptyTabComplete);

        getServer().getPluginManager().registerEvents(new AfkListener(this, afkManager), this);
        getServer().getPluginManager().registerEvents(new BackListener(backManager, textManager), this);
        getServer().getPluginManager().registerEvents(new GodListener(godManager), this);
        getServer().getPluginManager().registerEvents(new KitViewListener(kitManager), this);
        getServer().getPluginManager().registerEvents(new MessageListener(messageManager), this);
        getServer().getPluginManager().registerEvents(new PlayerDataListener(playerDataManager, offlinePlayerManager, teleportManager), this);
        getServer().getPluginManager().registerEvents(new PowertoolListener(playerDataManager), this);
        getServer().getPluginManager().registerEvents(new SignListener(this, textManager, kitManager, playerDataManager, teleportManager, warpManager, aliasManager), this);
        getServer().getPluginManager().registerEvents(new TeleportListener(teleportManager), this);
        getServer().getPluginManager().registerEvents(new VanishListener(textManager, vanishManager), this);

        if (database != null) {

            try {

                database.loadBalTop();

            } catch (DatabaseException e) {

                System.err.println("Could not load top balances.");
                e.printStackTrace();

            }

        }

    }

    @Override
    public void onDisable() {

        afkManager.clearAfk();
        backManager.clearBackLocations();
        godManager.clearGods();
        kitManager.clearKitCache();
        kitManager.clearViewing();
        messageManager.clearAllIgnored();
        messageManager.clearDisabled();
        messageManager.clearSocialSpy();
        nicknameManager.clearNicknames();
        offlinePlayerManager.clearCache();
        playerDataManager.clearCache();
        priceManager.clearCache();
        teleportManager.clearAcceptingAll();
        teleportManager.cancelAllTeleports();
        teleportManager.clearDisabled();
        teleportManager.clearRequests();
        vanishManager.clearVanished();
        warpManager.clearCache();

        if (database != null) {

            try {

                database.close();

            } catch (Exception ignored) {}

        }

    }

    public void reload() {

        if (database != null) {

            try {

                database.close();

            } catch (Exception ignored) {}

        }

        reloadConfig();

        loadTextColors();

        loadDatabase();

        try {

            database.loadBalTop();

        } catch (DatabaseException e) {

            System.err.println("Could not load top balances.");
            e.printStackTrace();

        }

        playerDataManager.clearCache();
        playerDataManager.setDatabase(database);

        for (Player p : getServer().getOnlinePlayers()) {

            playerDataManager.loadData(p);

        }

        kitManager.clearKitCache();
        priceManager.clearCache();
        warpManager.clearCache();

        kitManager.reload();
        priceManager.reload();
        warpManager.reload();
        aliasManager.reload();

    }

    private void loadTextColors() {

        String mainChar = getConfig().getString("commands.colors.main");
        String altChar = getConfig().getString("commands.colors.alt");
        String messageChar = getConfig().getString("commands.colors.message");
        String usageChar = getConfig().getString("commands.colors.usage");
        String errorChar = getConfig().getString("commands.colors.error");
        String socialspyChar = getConfig().getString("commands.colors.socialspy");
        String signChar = getConfig().getString("commands.colors.sign");

        if (mainChar == null) {

            mainChar = "r";

        }

        if (altChar == null) {

            altChar = "7";

        }

        if (messageChar == null) {

            messageChar = "r";

        }

        if (usageChar == null) {

            usageChar = "c";

        }

        if (errorChar == null) {

            errorChar = "c";

        }

        if (socialspyChar == null) {

            socialspyChar = "8";

        }

        if (signChar == null) {

            signChar = "1";

        }

        ChatColor main = ChatColor.getByChar(mainChar);
        ChatColor alt = ChatColor.getByChar(altChar);
        ChatColor message = ChatColor.getByChar(messageChar);
        ChatColor usage = ChatColor.getByChar(usageChar);
        ChatColor error = ChatColor.getByChar(errorChar);
        ChatColor socialspy = ChatColor.getByChar(socialspyChar);
        ChatColor sign = ChatColor.getByChar(signChar);

        String currencySymbol = getConfig().getString("economy.symbol");

        if (currencySymbol == null) {

            currencySymbol = "";

        }

        textManager.setMainColor(main);
        textManager.setAltColor(alt);
        textManager.setMessageColor(message);
        textManager.setUsageColor(usage);
        textManager.setErrorColor(error);
        textManager.setSocialSpyColor(socialspy);
        textManager.setSignColor(sign);

        textManager.setCurrencySymbol(currencySymbol);

    }

    private void loadDatabase() {

        switch (getConfig().getString("database.type", "yaml")) {

            case "yaml":

                database = new YAMLDatabase(this, playerDataManager, getDataFolder());

                break;

            case "sql":

                try {

                    database = new SQLDatabase(this, playerDataManager, getConfig().getString("database.sql.hostname"), getConfig().getInt("database.sql.port"), getConfig().getString("database.sql.database"), getConfig().getString("database.sql.username"), getConfig().getString("database.sql.password"));

                } catch (DatabaseException e) {

                    getLogger().severe("Unable to connect to MySQL database.");
                    e.printStackTrace();

                }

                break;

            default:

                try {

                    Class<?> customClass = Class.forName(getConfig().getString("database.type"));

                    if (EssentialsDatabase.class.isAssignableFrom(customClass)) {

                        database = (EssentialsDatabase) customClass.getMethod("getCustomEssentialsDatabase").invoke(null);

                    } else {

                        getLogger().severe(String.format("Custom database %s does not implement EssentialsDatabase.", getConfig().getString("database.type")));

                    }

                } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {

                    getLogger().severe("Unable to load custom database.");
                    e.printStackTrace();

                }

                break;

        }

    }

}
