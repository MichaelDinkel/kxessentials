package com.kaixeleron.essentials.listener;

import com.kaixeleron.essentials.manager.OfflinePlayerManager;
import com.kaixeleron.essentials.manager.PlayerDataManager;
import com.kaixeleron.essentials.manager.TeleportManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerDataListener implements Listener {

    private final PlayerDataManager manager;

    private final OfflinePlayerManager offlinePlayerManager;

    private final TeleportManager teleportManager;

    public PlayerDataListener(PlayerDataManager manager, OfflinePlayerManager offlinePlayerManager, TeleportManager teleportManager) {

        this.manager = manager;

        this.offlinePlayerManager = offlinePlayerManager;

        this.teleportManager = teleportManager;

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        offlinePlayerManager.addCache(event.getPlayer().getName(), event.getPlayer().getUniqueId());

        manager.loadData(event.getPlayer());

        if (manager.isRespawnOnLoginSet(event.getPlayer().getUniqueId())) {

            manager.setLoginRespawn(event.getPlayer().getUniqueId(), false);

            Location spawn = teleportManager.getSpawn();

            if (spawn == null) spawn = Bukkit.getWorlds().get(0).getSpawnLocation();

            spawn = spawn.clone().add(0.5D, 0.0D, 0.5D);

            event.getPlayer().teleport(spawn);

        }

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        manager.clearCachedData(event.getPlayer());

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        manager.clearCachedData(event.getPlayer());

    }

}
