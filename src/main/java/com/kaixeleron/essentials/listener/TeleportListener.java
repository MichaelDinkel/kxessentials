package com.kaixeleron.essentials.listener;

import com.kaixeleron.essentials.manager.TeleportManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;
import java.util.Map;

public class TeleportListener implements Listener {

    private final TeleportManager manager;

    private final Map<Player, Location> lastLocations;

    public TeleportListener(TeleportManager manager) {

        this.manager = manager;

        lastLocations = new HashMap<>();

    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {

        if (manager.isTeleporting(event.getPlayer())) {

            if (!lastLocations.containsKey(event.getPlayer())) {

                lastLocations.put(event.getPlayer(), event.getFrom());

            } else {

                Location last = lastLocations.get(event.getPlayer()), to = event.getTo();

                if (last.getBlockX() != to.getBlockX() || last.getBlockY() != to.getBlockY() || last.getBlockZ() != to.getBlockZ()) {

                    lastLocations.remove(event.getPlayer());
                    manager.cancelTeleport(event.getPlayer());

                }

            }

        } else if (lastLocations.containsKey(event.getPlayer())) {

            lastLocations.remove(event.getPlayer());

        }

    }

}
