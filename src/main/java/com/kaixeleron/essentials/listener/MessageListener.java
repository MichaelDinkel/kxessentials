package com.kaixeleron.essentials.listener;

import com.kaixeleron.essentials.manager.MessageManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class MessageListener implements Listener {

    private final MessageManager messageManager;

    public MessageListener(MessageManager messageManager) {

        this.messageManager = messageManager;

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        messageManager.clearIgnored(event.getPlayer());
        messageManager.setDisabled(event.getPlayer(), false);
        messageManager.removeReply(event.getPlayer());
        messageManager.removeSocialSpy(event.getPlayer());

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        messageManager.clearIgnored(event.getPlayer());
        messageManager.setDisabled(event.getPlayer(), false);
        messageManager.removeReply(event.getPlayer());
        messageManager.removeSocialSpy(event.getPlayer());

    }

}
