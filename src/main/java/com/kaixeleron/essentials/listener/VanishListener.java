package com.kaixeleron.essentials.listener;

import com.kaixeleron.essentials.manager.TextManager;
import com.kaixeleron.essentials.manager.VanishManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class VanishListener implements Listener {

    private final TextManager textManager;

    private final VanishManager vanishManager;

    public VanishListener(TextManager textManager, VanishManager vanishManager) {

        this.textManager = textManager;

        this.vanishManager = vanishManager;

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        if (event.getPlayer().hasPermission("kxessentials.vanish.silentjoin")) {

            vanishManager.vanish(event.getPlayer());

            event.setJoinMessage("");

            event.getPlayer().sendMessage(textManager.getMainColor() + "You have joined vanished and silently.");

            for (Player p : Bukkit.getOnlinePlayers()) {

                if (!p.equals(event.getPlayer()) && p.hasPermission("kxessentials.vanish.see")) {

                    p.sendMessage(textManager.getAltColor() + event.getPlayer().getName() + textManager.getMainColor() + " has vanished and joined silently.");

                }

            }

        }

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        if (vanishManager.isVanished(event.getPlayer())) {

            vanishManager.visible(event.getPlayer());

            event.setQuitMessage("");

            for (Player p : Bukkit.getOnlinePlayers()) {

                if (!p.equals(event.getPlayer()) && p.hasPermission("kxessentials.vanish.see")) {

                    p.sendMessage(textManager.getAltColor() + event.getPlayer().getName() + textManager.getMainColor() + " has quit silently.");

                }

            }

        }

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        if (vanishManager.isVanished(event.getPlayer())) {

            vanishManager.visible(event.getPlayer());

            event.setLeaveMessage("");

            for (Player p : Bukkit.getOnlinePlayers()) {

                if (!p.equals(event.getPlayer()) && p.hasPermission("kxessentials.vanish.see")) {

                    p.sendMessage(textManager.getAltColor() + event.getPlayer().getName() + textManager.getMainColor() + " has quit silently.");

                }

            }

        }

    }

}
