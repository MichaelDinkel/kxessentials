package com.kaixeleron.essentials.listener;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.*;
import org.bukkit.*;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class SignListener implements Listener {

    private final EssentialsMain m;

    private final TextManager textManager;

    private final KitManager kitManager;

    private final PlayerDataManager playerDataManager;

    private final TeleportManager teleportManager;

    private final WarpManager warpManager;

    private final AliasManager aliasManager;

    private final Set<Player> debounce;

    public SignListener(EssentialsMain m, TextManager textManager, KitManager kitManager, PlayerDataManager playerDataManager, TeleportManager teleportManager, WarpManager warpManager, AliasManager aliasManager) {

        this.m = m;

        this.textManager = textManager;

        this.kitManager = kitManager;

        this.playerDataManager = playerDataManager;

        this.teleportManager = teleportManager;

        this.warpManager = warpManager;

        this.aliasManager = aliasManager;

        debounce = new HashSet<>();

    }

    @EventHandler(ignoreCancelled = true)
    public void onSignChange(SignChangeEvent event) {

        switch (event.getLine(0).toLowerCase()) {

            case "[balance]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.balance")) {

                    event.setLine(0, textManager.getSignColor() + "[Balance]");

                }

                break;

            case "[buy]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.buy")) {

                    event.setLine(0, textManager.getSignColor() + "[Buy]");

                }

                break;

            case "[disposal]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.disposal")) {

                    event.setLine(0, textManager.getSignColor() + "[Disposal]");

                }

                break;

            case "[enchant]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.enchant")) {

                    event.setLine(0, textManager.getSignColor() + "[Enchant]");

                }

                break;

            case "[free]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.free")) {

                    event.setLine(0, textManager.getSignColor() + "[Free]");

                }

                break;

            case "[gamemode]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.gamemode")) {

                    event.setLine(0, textManager.getSignColor() + "[Gamemode]");

                }

                break;

            case "[heal]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.heal")) {

                    event.setLine(0, textManager.getSignColor() + "[Heal]");

                }

                break;

            case "[kit]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.kit")) {

                    event.setLine(0, textManager.getSignColor() + "[Kit]");

                }

                break;

            case "[repair]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.repair")) {

                    event.setLine(0, textManager.getSignColor() + "[Repair]");

                }

                break;

            case "[sell]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.sell")) {

                    event.setLine(0, textManager.getSignColor() + "[Sell]");

                }

                break;

            case "[spawn]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.spawn")) {

                    event.setLine(0, textManager.getSignColor() + "[Spawn]");

                }

                break;

            case "[warp]":

                if (event.getPlayer().hasPermission("kxessentials.sign.create.warp")) {

                    event.setLine(0, textManager.getSignColor() + "[Warp]");

                }

                break;

        }

        if (event.getPlayer().hasPermission("kxessentials.sign.color")) {

            String[] lines = event.getLines();

            for (int i = 0; i < lines.length; i++) {

                event.setLine(i, ChatColor.translateAlternateColorCodes('&', lines[i]));

            }

        }

    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {

        switch (event.getBlock().getType()) {

            case OAK_SIGN:
            case OAK_WALL_SIGN:
            case SPRUCE_SIGN:
            case SPRUCE_WALL_SIGN:
            case BIRCH_SIGN:
            case BIRCH_WALL_SIGN:
            case JUNGLE_SIGN:
            case JUNGLE_WALL_SIGN:
            case ACACIA_SIGN:
            case ACACIA_WALL_SIGN:
            case DARK_OAK_SIGN:
            case DARK_OAK_WALL_SIGN:
            case WARPED_SIGN:
            case WARPED_WALL_SIGN:
            case CRIMSON_SIGN:
            case CRIMSON_WALL_SIGN:

                Sign s = (Sign) event.getBlock().getState();

                if (s.getLine(0).startsWith(textManager.getSignColor() + "")) {

                    switch (ChatColor.stripColor(s.getLine(0).toLowerCase())) {

                        case "[balance]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.balance")) {

                                event.setCancelled(true);

                            }

                            break;

                        case "[buy]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.buy")) {

                                event.setCancelled(true);

                            }

                            break;

                        case "[disposal]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.disposal")) {

                                event.setCancelled(true);

                            }

                            break;

                        case "[enchant]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.enchant")) {

                                event.setCancelled(true);

                            }

                            break;

                        case "[free]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.free")) {

                                event.setCancelled(true);

                            }

                            break;

                        case "[gamemode]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.gamemode")) {

                                event.setCancelled(true);

                            }

                            break;

                        case "[heal]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.heal")) {

                                event.setCancelled(true);

                            }

                            break;

                        case "[kit]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.kit")) {

                                event.setCancelled(true);

                            }

                            break;

                        case "[repair]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.repair")) {

                                event.setCancelled(true);

                            }

                            break;

                        case "[sell]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.sell")) {

                                event.setCancelled(true);

                            }

                            break;


                        case "[spawn]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.spawn")) {

                                event.setCancelled(true);

                            }

                            break;

                        case "[warp]":

                            if (!event.getPlayer().hasPermission("kxessentials.sign.break.warp")) {

                                event.setCancelled(true);

                            }

                            break;

                    }

                }

                break;

        }

    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {

        if (!debounce.contains(event.getPlayer()) && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {

            if (event.getClickedBlock() != null) {

                switch (event.getClickedBlock().getType()) {

                    case OAK_SIGN:
                    case OAK_WALL_SIGN:
                    case SPRUCE_SIGN:
                    case SPRUCE_WALL_SIGN:
                    case BIRCH_SIGN:
                    case BIRCH_WALL_SIGN:
                    case JUNGLE_SIGN:
                    case JUNGLE_WALL_SIGN:
                    case ACACIA_SIGN:
                    case ACACIA_WALL_SIGN:
                    case DARK_OAK_SIGN:
                    case DARK_OAK_WALL_SIGN:

                        Sign s = (Sign) event.getClickedBlock().getState();

                        if (s.getLine(0).startsWith(textManager.getSignColor() + "")) {

                            debounce.add(event.getPlayer());

                            new BukkitRunnable() {

                                @Override
                                public void run() {

                                    debounce.remove(event.getPlayer());

                                }

                            }.runTaskLater(m, 1L);

                            switch (ChatColor.stripColor(s.getLine(0).toLowerCase())) {

                                case "[balance]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.balance")) {

                                        processBalance(event.getPlayer());

                                    }

                                    break;

                                case "[buy]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.buy")) {

                                        if (!processBuy(event.getPlayer(), s.getLine(1), s.getLine(2), s.getLine(3))) {

                                            Location l = event.getClickedBlock().getLocation();

                                            System.err.println(String.format("Buy sign at x: %d y: %d, z: %d has invalid options.", l.getBlockX(), l.getBlockY(), l.getBlockZ()));

                                        }

                                    }

                                    break;

                                case "[disposal]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.disposal")) {

                                        processDisposal(event.getPlayer());

                                    }

                                    break;

                                case "[enchant]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.enchant")) {

                                        if (!processEnchant(event.getPlayer(), s.getLine(1), s.getLine(2))) {

                                            Location l = event.getClickedBlock().getLocation();

                                            System.err.println(String.format("Enchant sign at x: %d y: %d, z: %d has invalid options.", l.getBlockX(), l.getBlockY(), l.getBlockZ()));

                                        }

                                    }

                                    break;

                                case "[free]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.free")) {

                                        if (!processFree(event.getPlayer(), s.getLine(1), s.getLine(2))) {

                                            Location l = event.getClickedBlock().getLocation();

                                            System.err.println(String.format("Free sign at x: %d y: %d, z: %d has invalid options.", l.getBlockX(), l.getBlockY(), l.getBlockZ()));

                                        }

                                    }

                                    break;

                                case "[gamemode]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.gamemode")) {

                                        if (!processGamemode(event.getPlayer(), s.getLine(1))) {

                                            Location l = event.getClickedBlock().getLocation();

                                            System.err.println(String.format("Gamemode sign at x: %d y: %d, z: %d has invalid options.", l.getBlockX(), l.getBlockY(), l.getBlockZ()));

                                        }

                                    }

                                    break;

                                case "[heal]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.heal")) {

                                        processHeal(event.getPlayer());

                                    }

                                    break;

                                case "[kit]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.kit")) {

                                        if (!processKit(event.getPlayer(), s.getLine(1))) {

                                            Location l = event.getClickedBlock().getLocation();

                                            System.err.println(String.format("Kit sign at x: %d y: %d, z: %d has invalid options.", l.getBlockX(), l.getBlockY(), l.getBlockZ()));

                                        }

                                    }

                                    break;

                                case "[repair]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.repair")) {

                                        processRepair(event.getPlayer());

                                    }

                                    break;

                                case "[sell]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.sell")) {

                                        if (!processSell(event.getPlayer(), s.getLine(1), s.getLine(2), s.getLine(3))) {

                                            Location l = event.getClickedBlock().getLocation();

                                            System.err.println(String.format("Sell sign at x: %d y: %d, z: %d has invalid options.", l.getBlockX(), l.getBlockY(), l.getBlockZ()));

                                        }

                                    }

                                    break;

                                case "[spawn]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.spawn")) {

                                        processSpawn(event.getPlayer());

                                    }

                                    break;

                                case "[warp]":

                                    if (event.getPlayer().hasPermission("kxessentials.sign.use.warp")) {

                                        if (!processWarp(event.getPlayer(), s.getLine(1))) {

                                            Location l = event.getClickedBlock().getLocation();

                                            System.err.println(String.format("Sell sign at x: %d y: %d, z: %d has invalid options.", l.getBlockX(), l.getBlockY(), l.getBlockZ()));

                                        }

                                    }

                                    break;

                            }

                        }

                        break;

                }

            }

        }

    }

    private void processBalance(Player p) {

        p.sendMessage(textManager.getMainColor() + "Balance: " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(playerDataManager.getBalance(p)).stripTrailingZeros().toPlainString());

    }

    private boolean processBuy(Player p, String type, String amountStr, String priceStr) {

        int amount = Integer.MIN_VALUE;

        try {

            amount = Integer.parseInt(amountStr);

        } catch (NumberFormatException ignored) {}

        if (amount != Integer.MIN_VALUE) {

            double price = Double.MIN_VALUE;

            try {

                price = Double.parseDouble(priceStr.substring(textManager.getCurrencySymbol().length()));

            } catch (NumberFormatException ignored) {}

            if (price != Double.MIN_VALUE) {

                Material m = null;

                for (Material mat : Material.values()) {

                    if (mat.toString().replace("_", "").equalsIgnoreCase(type) || mat.toString().replace('_', ' ').equalsIgnoreCase(type)) {

                        m = mat;

                        break;

                    }

                }

                if (m == null) {

                    m = aliasManager.getByAlias(type.toLowerCase());

                }

                if (m != null) {

                    double playerBal = playerDataManager.getBalance(p);

                    if (playerBal < price) {

                        p.sendMessage(textManager.getErrorColor() + "You do not have enough money for this purchase.");

                    } else {

                        p.getInventory().addItem(new ItemStack(m, amount));

                        playerDataManager.setBalance(p, playerBal - price);

                        p.sendMessage(textManager.getMainColor() + "Purchased " + textManager.getAltColor() + amount + " " + m.toString().toLowerCase().replace('_', ' ') + (amount == 1 ? "" : "s") + textManager.getMainColor() + " for " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(price).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                    }

                    return true;

                }

            }

        }

        return false;

    }

    private void processDisposal(Player p) {

        p.openInventory(Bukkit.createInventory(null, 54, "Disposal"));

    }

    private boolean processEnchant(Player p, String type, String levelStr) {

        int level = Integer.MIN_VALUE;

        try {

            level = Integer.parseInt(levelStr);

        } catch (NumberFormatException ignored) {}

        if (level != Integer.MIN_VALUE) {

            @SuppressWarnings("deprecation") Enchantment e = Enchantment.getByKey(new NamespacedKey("minecraft", type.toLowerCase()));

            if (e != null) {

                p.getInventory().getItemInMainHand().addUnsafeEnchantment(e, level);

                p.sendMessage(textManager.getMainColor() + "Item enchanted.");

                return true;

            }

        }

        return false;

    }

    private boolean processFree(Player p, String type, String amountStr) {

        int amount = Integer.MIN_VALUE;

        try {

            amount = Integer.parseInt(amountStr);

        } catch (NumberFormatException ignored) {}

        if (amount != Integer.MIN_VALUE) {

            Material m = null;

            for (Material mat : Material.values()) {

                if (mat.toString().replace("_", "").equalsIgnoreCase(type) || mat.toString().replace('_', ' ').equalsIgnoreCase(type)) {

                    m = mat;

                    break;

                }

            }

            if (m != null) {

                p.getInventory().addItem(new ItemStack(m, amount));

                p.sendMessage(textManager.getMainColor() + "Received " + textManager.getAltColor() + amount + textManager.getMainColor() + " free " + textManager.getAltColor() + m.toString().toLowerCase().replace('_', ' ') + (amount == 1 ? "" : "s") + textManager.getMainColor() + ".");


                return true;

            }

        }

        return false;

    }

    private boolean processGamemode(Player p, String gamemode) {

        switch (gamemode.toLowerCase()) {

            case "survival":
            case "s":
            case "0":

                p.setGameMode(GameMode.SURVIVAL);
                p.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "survival" + textManager.getMainColor() + " mode.");

                return true;

            case "creative":
            case "c":
            case "1":

                p.setGameMode(GameMode.CREATIVE);
                p.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "creative" + textManager.getMainColor() + " mode.");

                return true;

            case "adventure":
            case "a":
            case "2":

                p.setGameMode(GameMode.ADVENTURE);
                p.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "adventure" + textManager.getMainColor() + " mode.");

                return true;

            case "spectator":
            case "sp":
            case "3":

                p.setGameMode(GameMode.SPECTATOR);
                p.sendMessage(textManager.getMainColor() + "Gamemode set to " + textManager.getAltColor() + "spectator" + textManager.getMainColor() + " mode.");

                return true;

        }

        return false;

    }

    private void processHeal(Player p) {

        p.setHealth(20.0D);
        p.setFoodLevel(20);
        p.sendMessage(textManager.getMainColor() + "You have been healed.");

    }

    private boolean processKit(Player p, String name) {

        if (kitManager.kitExists(name)) {

            long cooldown = playerDataManager.getKitCooldown(p, name);

            if (cooldown <= System.currentTimeMillis() || p.hasPermission("kxessentials.command.kit.nocooldown")) {

                p.getInventory().addItem(kitManager.getKit(name));

                long kitCooldown = kitManager.getKitCooldown(name);

                if (kitCooldown > 0L && !p.hasPermission("kxessentials.command.kit.nocooldown")) {

                    playerDataManager.setKitCooldown(p, name, kitCooldown + System.currentTimeMillis());

                }

                p.sendMessage(textManager.getMainColor() + "Spawned kit " + textManager.getAltColor() + name + textManager.getMainColor() + ".");

            } else {

                p.sendMessage(kitManager.generateCooldownString(cooldown - System.currentTimeMillis()));

            }

            return true;

        }

        return false;

    }

    private void processRepair(Player p) {

        ItemStack item = p.getInventory().getItemInMainHand();

        ItemMeta meta = item.getItemMeta();

        if (meta instanceof Damageable && ((Damageable) meta).getDamage() > 0) {

            ((Damageable) meta).setDamage(0);
            item.setItemMeta(meta);

            p.sendMessage(textManager.getMainColor() + "Item repaired.");

        } else {

            p.sendMessage(textManager.getErrorColor() + "This item cannot be repaired.");

        }

    }

    private boolean processSell(Player p, String type, String amountStr, String priceStr) {

        int amount = Integer.MIN_VALUE;

        try {

            amount = Integer.parseInt(amountStr);

        } catch (NumberFormatException ignored) {}

        if (amount != Integer.MIN_VALUE) {

            double price = Double.MIN_VALUE;

            try {

                price = Double.parseDouble(priceStr.substring(textManager.getCurrencySymbol().length()));

            } catch (NumberFormatException ignored) {}

            if (price != Double.MIN_VALUE) {

                Material m = null;

                for (Material mat : Material.values()) {

                    if (mat.toString().replace("_", "").equalsIgnoreCase(type) || mat.toString().replace('_', ' ').equalsIgnoreCase(type)) {

                        m = mat;

                        break;

                    }

                }

                if (m == null) {

                    m = aliasManager.getByAlias(type.toLowerCase());

                }

                if (m != null) {

                    for (ItemStack i : p.getInventory().getContents()) {

                        if (i != null) {

                            if (i.getType().equals(m) && i.getAmount() >= amount) {

                                i.setAmount(i.getAmount() - amount);

                                playerDataManager.setBalance(p, playerDataManager.getBalance(p) + price);

                                p.sendMessage(textManager.getMainColor() + "Sold " + textManager.getAltColor() + amount + " " + m.toString().toLowerCase().replace('_', ' ') + (amount == 1 ? "" : "s") + textManager.getMainColor() + " for " + textManager.getAltColor() + textManager.getCurrencySymbol() + new BigDecimal(price).stripTrailingZeros().toPlainString() + textManager.getMainColor() + ".");

                                return true;

                            }

                        }

                    }

                    p.sendMessage(textManager.getErrorColor() + "You do not have enough of this item to sell.");

                    return true;

                }

            }

        }

        return false;

    }

    private void processSpawn(Player p) {

        Location spawn = teleportManager.getSpawn();

        if (spawn == null) spawn = Bukkit.getWorlds().get(0).getSpawnLocation();

        p.sendMessage("Teleporting to spawn...");
        teleportManager.teleport(p, spawn);

    }

    private boolean processWarp(Player p, String warp) {

        if (warpManager.listWarps().contains(warp)) {

            p.sendMessage(textManager.getMainColor() + "Teleporting to warp " + textManager.getAltColor() + warp + textManager.getMainColor() + "...");

            teleportManager.teleport(p, warpManager.getWarp(warp));

            return true;

        }

        return false;

    }

}
