package com.kaixeleron.essentials.listener;

import com.kaixeleron.essentials.EssentialsMain;
import com.kaixeleron.essentials.manager.AfkManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.BukkitRunnable;

public class AfkListener implements Listener {

    private final EssentialsMain m;

    private final AfkManager manager;

    public AfkListener(EssentialsMain m, AfkManager manager) {

        this.m = m;

        this.manager = manager;

    }

    private void disableAfk(Player p) {

        if (manager.isAfk(p)) {

            manager.setAfk(p, false);

        }

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        disableAfk(event.getPlayer());

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        disableAfk(event.getPlayer());

    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {

        disableAfk(event.getPlayer());

    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {

        disableAfk(event.getPlayer());

    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {

        if (!event.getMessage().startsWith("/afk")) {

            disableAfk(event.getPlayer());

        }

    }

    @EventHandler
    public void onChat(final AsyncPlayerChatEvent event) {

        new BukkitRunnable() {

            @Override
            public void run() {

                disableAfk(event.getPlayer());

            }

        }.runTask(m);

    }

}
