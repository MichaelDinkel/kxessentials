package com.kaixeleron.essentials.listener;

import com.kaixeleron.essentials.manager.KitManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class KitViewListener implements Listener {

    private final KitManager manager;

    public KitViewListener(KitManager manager) {

        this.manager = manager;

    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {

        if (manager.isViewing((Player) event.getWhoClicked())) {

            event.setCancelled(true);

        }

    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {

        if (manager.isViewing((Player) event.getPlayer())) {

            manager.removeViewing((Player) event.getPlayer());

        }

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        if (manager.isViewing(event.getPlayer())) {

            manager.removeViewing(event.getPlayer());

        }

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        if (manager.isViewing(event.getPlayer())) {

            manager.removeViewing(event.getPlayer());

        }

    }

}
