package com.kaixeleron.essentials.listener;

import com.kaixeleron.essentials.manager.BackManager;
import com.kaixeleron.essentials.manager.TextManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class BackListener implements Listener {

    private final BackManager manager;

    private final TextManager textManager;

    public BackListener(BackManager manager, TextManager textManager) {

        this.manager = manager;

        this.textManager = textManager;

    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDeath(PlayerDeathEvent event) {

        if (event.getEntity().hasPermission("kxessentials.command.back") && event.getEntity().hasPermission("kxessentials.back.death")) {

            manager.setBackLocation(event.getEntity(), event.getEntity().getLocation());

            event.getEntity().sendMessage(textManager.getMainColor() + "Type " + textManager.getAltColor() + "/back" + textManager.getMainColor() + " to return to your death location.");

        }

    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTeleport(PlayerTeleportEvent event) {

        if (event.getPlayer().hasPermission("kxessentials.command.back")) {

            manager.setBackLocation(event.getPlayer(), event.getFrom());

        }

    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onQuit(PlayerQuitEvent event) {

        manager.removeBackLocation(event.getPlayer());

    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onKick(PlayerKickEvent event) {

        manager.removeBackLocation(event.getPlayer());

    }

}
