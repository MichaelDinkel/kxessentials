package com.kaixeleron.essentials.listener;

import com.kaixeleron.essentials.manager.GodManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class GodListener implements Listener {

    private final GodManager godManager;

    public GodListener(GodManager godManager) {

        this.godManager = godManager;

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        if (godManager.isGod(event.getPlayer())) {

            godManager.removeGod(event.getPlayer());

        }

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        if (godManager.isGod(event.getPlayer())) {

            godManager.removeGod(event.getPlayer());

        }

    }

    @EventHandler(ignoreCancelled = true)
    public void onDamage(EntityDamageEvent event) {

        if (event.getEntity() instanceof Player && !event.getCause().equals(EntityDamageEvent.DamageCause.VOID) && !event.getCause().equals(EntityDamageEvent.DamageCause.CUSTOM)) {

            if (godManager.isGod((Player) event.getEntity())) {

                event.setCancelled(true);

            }

        }

    }

}
