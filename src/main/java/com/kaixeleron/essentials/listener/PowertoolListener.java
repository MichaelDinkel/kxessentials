package com.kaixeleron.essentials.listener;

import com.kaixeleron.essentials.manager.PlayerDataManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PowertoolListener implements Listener {

    private final PlayerDataManager playerDataManager;

    public PowertoolListener(PlayerDataManager playerDataManager) {

        this.playerDataManager = playerDataManager;

    }

    @EventHandler
    public void onLeftClick(PlayerInteractEvent event) {

        if (event.getAction().equals(Action.LEFT_CLICK_BLOCK) || event.getAction().equals(Action.LEFT_CLICK_AIR)) {

            String command = playerDataManager.getPowertool(event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand().getType());

            if (command != null) {

                event.setCancelled(true);

                System.out.println(String.format("%s executed server command: /%s", event.getPlayer().getName(), command));
                Bukkit.dispatchCommand(event.getPlayer(), command);

            }

        }

    }

}
